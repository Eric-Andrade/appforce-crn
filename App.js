import React, { useEffect, Fragment } from 'react';
import {SafeAreaView, StyleSheet, KeyboardAvoidingView, Image, Text, StatusBar, View } from 'react-native';
import {createAppContainer, createSwitchNavigator} from 'react-navigation';
import 'react-native-gesture-handler';
import {createStackNavigator} from 'react-navigation-stack';
import {createBottomTabNavigator} from 'react-navigation-tabs';
import IniciarSesion from './src/screens/IniciarSesion';
import CerrarSesion from './src/screens/CerrarSesion';
import Perfil from './src/screens/Perfil';
import Noticias from './src/screens/Noticias';
import Post from './src/screens/Post';
import SolDatosLaborales from './src/screens/SolDatosLaborales';
import Solicitudes1 from './src/screens/Solicitudes1';
import SolDatosDomicilio from './src/screens/SolDatosDomicilio';
import SolDatosPersonales from './src/screens/SolDatosPersonales';
import SolAutorizacion from './src/screens/SolAutorizacion';
import Seleccion from './src/screens/Seleccion';
import Panel from './src/screens/Panel';
import Soporte from './src/screens/Soporte';
import PinMsg from './src/screens/PinMsg';
import PinVal from './src/screens/PinVal';
import Ionicons from 'react-native-vector-icons/Ionicons';
import Entypo from 'react-native-vector-icons/Entypo';
import {Provider as AuthProvider} from './src/context/AuthContext';
import {setNavigator} from './src/navigationRef';
import ResolveAuthScreen from './src/screens/ResolveAuthScreen';
import SolicitudProcesada from './src/screens/SolicitudProcesada';
import Calculadora from './src/screens/Calculadora';
import nsDatosAdicionales from './src/screens/nsDatosAdicionales';
import nsCargaDocumentos from './src/screens/nsCargaDocumentos';
import nsCargaDocumentosD2 from './src/screens/nsCargaDocumentosD2';
import nsCargaDocumentosD3 from './src/screens/nsCargaDocumentosD3';
import nsCargaDocumentosD4 from './src/screens/nsCargaDocumentosD4';
import nsCargaDocumentosD5 from './src/screens/nsCargaDocumentosD5';
import nsCargaDocumentosD7 from './src/screens/nsCargaDocumentosD7';
import nsCargaDocumentosD8 from './src/screens/nsCargaDocumentosD8';
import nsCargaDocumentosOP1 from './src/screens/nsCargaDocumentosOP1';
import nsCargaDocumentosD11 from './src/screens/nsCargaDocumentosD11';
import SolFinalizada from './src/screens/SolFinalizada';
import Ocr from './src/screens/Ocr';
import SolRechazada from './src/screens/SolRechazada';
import nsPais from './src/screens/nsPais';
import OcrInfo from './src/screens/OcrInfo';
import Selfie from './src/screens/Selfie';
import Solicitudes2 from './src/screens/Solicitudes2';
import Solicitudes3 from './src/screens/Solicitudes3';
import Solicitudes4 from './src/screens/Solicitudes4';
import Irregular from './src/screens/Irregular';
import nsPaisOCR from './src/screens/nsPaisOCR';
import CreditoAprobado from './src/screens/CreditoAprobado';
import Sol1Form from './src/components/Sol1Form';
import Sol2Form from './src/components/Sol2Form';
import IrregularForm from './src/components/IrregularForm';
import CalculadoraCA from './src/screens/CalculadoraCA';
import CalculadoraFormCA from './src/components/CalculadoraFormCA';
import SplashScreen from 'react-native-splash-screen';
import Header from './src/components/Header';
import SolStack from './src/components/SolStack';
import Sandbox from './src/components/sandbox';
import NetInfo from './src/components/NetInfo';
const AppIndex = createAppContainer(SolStack)  

const switchNavigator = createSwitchNavigator({
  ResolveAuth: ResolveAuthScreen,

  login: IniciarSesion,

  mainflow: createBottomTabNavigator(
    {
      Panel: {
        screen: Panel,
        navigationOptions: {
          tabBarLabel: 'Inicio',
          tabBarIcon: ({tintColor}) => (
            <Ionicons name='ios-keypad' size={24} color={tintColor} />
          ),
        },
        defaultNavigationOptions: {
          gestureEnabled: false
        }
      },
      Solicitudes: createStackNavigator(
        {
          AppIndex: {
            screen: AppIndex,
            navigationOptions: {
              style: {
                borderTopColor: 'transparent',
              },
              headerTitleStyle: {
                backgroundColor: 'red'
              },
              headerTitleAlign:'left',
              headerTitle: () => (
                <>
                  <View>
                    <Image
                        style={{
                          height: 70,
                          width: 110,
                          justifyContent: 'flex-start',
                          alignItems: 'flex-start',
                          marginTop: 40,
                          marginBottom: 20,
                          // marginLeft: 10,
                      }}
                        source={require('./assets/Logo-Grande.png')}
                    />
                  </View>
                  {/* <View style={{ flex: 1 }}>
                    <Text style={{ color: 'white', marginTop: 20, fontSize: 22 }}>Solicitudes</Text>
                  </View> */}
                </>
              ),
              title: 'Solicitudes',
              style:{
                borderTopColor: 'transparent',
                backgroundColor: '#000'
              },
              headerShown: true,
              headerStyle: {
                height: 130,
                backgroundColor: '#000',
                // borderBottomLeftRadius: 20,
                // borderBottomRightRadius: 20,
              }
            }
          },
          Solicitudes1: Solicitudes1,
          Solicitudes2: Solicitudes2,
          Solicitudes3: Solicitudes3,
          Solicitudes4: Solicitudes4,
          Irregular: Irregular,
        },
        {
          navigationOptions: {
            tabBarLabel: 'Solicitudes',
            tabBarIcon: ({tintColor}) => (
              <Ionicons name='ios-list-box' size={24} color={tintColor} />
            ),
          },
          defaultNavigationOptions: {
            gestureEnabled: false
          }
        },
      ),

      Nueva: createStackNavigator(
        {
          Seleccion: Seleccion,
          Ocr: Ocr,
          OcrInfo: OcrInfo,
          nsPaisOCR: nsPaisOCR,
          SolDatosPersonales: SolDatosPersonales,
          SolDatosDomicilio: SolDatosDomicilio,
          SolDatosLaborales: SolDatosLaborales,
          SolAutorizacion: SolAutorizacion,
          PinMsg: PinMsg,
          PinVal: PinVal,
          Calculadora: Calculadora,
          CalculadoraCA: CalculadoraCA,
          CreditoAprobado: CreditoAprobado,
          nsPais: nsPais,
          nsDatosAdicionales: nsDatosAdicionales,
          Selfie: Selfie,
          nsCargaDocumentos: nsCargaDocumentos,
          nsCargaDocumentosD2: nsCargaDocumentosD2,
          nsCargaDocumentosD3: nsCargaDocumentosD3,
          nsCargaDocumentosD4: nsCargaDocumentosD4,
          nsCargaDocumentosD5: nsCargaDocumentosD5,
          nsCargaDocumentosD7: nsCargaDocumentosD7,
          nsCargaDocumentosD8: nsCargaDocumentosD8,
          nsCargaDocumentosOP1: nsCargaDocumentosOP1,
          nsCargaDocumentosD11: nsCargaDocumentosD11,
          SolFinalizada: SolFinalizada,
          SolicitudProcesada: SolicitudProcesada,
          SolRechazada: SolRechazada,
          Irregular: Irregular,
          IrregularForm: IrregularForm,
          Sol1Form: Sol1Form,
          Sol2Form: Sol2Form,
          CalculadoraFormCA: CalculadoraFormCA,
        },
        {
          navigationOptions: {
            tabBarLabel: 'Nueva',
            tabBarIcon: ({tintColor}) => (
              <Ionicons
                name='ios-add-circle-outline'
                size={24}
                color={tintColor}
              />
            ),
          },
          defaultNavigationOptions: {
            gestureEnabled: false
          }
        },
      ),

      Perfil: createStackNavigator(
        {
          MiPerfil: Perfil,
          CerrarSesion: CerrarSesion,
        },
        {
          navigationOptions: {
            tabBarLabel: 'Perfil',
            tabBarIcon: ({tintColor}) => (
              <Ionicons name='ios-person' size={24} color={tintColor} />
            ),
          },
          defaultNavigationOptions: {
            gestureEnabled: false
          }
        },
      ),

      Noticias: createStackNavigator(
        {
          PostsList: Noticias,
          Post: {
            screen: Post,
            navigationOptions: {
              headerShown: false
            }
          },
        },
        {
          navigationOptions: {
            tabBarLabel: 'Noticias',
            tabBarIcon: ({tintColor}) => (
              <Entypo name='newsletter' size={24} color={tintColor} />
            ),
          },
          defaultNavigationOptions: {
            gestureEnabled: false
          }
        },
      ),

      Soporte: {
        screen: Soporte,
        navigationOptions: {
          tabBarLabel: 'Soporte',
          tabBarIcon: ({tintColor}) => (
            <Ionicons name='ios-settings' size={24} color={tintColor} />
          ),
        },
        defaultNavigationOptions: {
          gestureEnabled: false
        }
      },
    },
    {
      header: null,
      tabBarPosition: 'bottom',
      swipeEnabled: true,
      animationEnabled: true,
      tabBarOptions: {
        activeTintColor: '#D32345',
        inactiveTintColor: '#808080',
        style: {
          borderTopColor: 'transparent',
          backgroundColor: '#000',
        },
        labelStyle: {
          textAlign: 'center',
        },
      },
    },
  ),
});

const App = createAppContainer(switchNavigator);

export default () => {
  useEffect(() => {
    SplashScreen.hide()
    // Settimeout(()=> {
    //   SplashScreen.hide()
    // }, 2000)
  }, [])

  return (
    <Fragment>
      <StatusBar backgroundColor='black' barStyle='light-content' /> 
      <SafeAreaView style={{ flex: 0, backgroundColor: 'black' }} />
      <NetInfo />
        <SafeAreaView style={{ flex: 1, backgroundColor: 'black' }}>    
          <KeyboardAvoidingView style={{ flex: 1 }} enabled>
            <View style={styles.container}>
              <AuthProvider style={styles.container}>
                <App
                  ref={navigator => {
                    setNavigator(navigator);
                  }}
                />
              </AuthProvider>
            </View>
          </KeyboardAvoidingView>
        </SafeAreaView>
    </Fragment>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});
