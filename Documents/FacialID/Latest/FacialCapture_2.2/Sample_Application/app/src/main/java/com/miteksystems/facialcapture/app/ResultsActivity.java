package com.miteksystems.facialcapture.app;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.miteksystems.facialcapture.science.api.params.FacialCaptureApi;
import com.miteksystems.misnap.params.MiSnapApi;
import com.miteksystems.misnap.utils.Utils;

/**
 * Created by awood on 4/29/16.
 */
public class ResultsActivity extends AppCompatActivity {

    private static final String TAG = ResultsActivity.class.getSimpleName();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_results);

        if(BuildConfig.DEBUG) {
            Utils.unlockAndTurnOnScreen(this);
        }

        ImageView imageCaptured = (ImageView) findViewById(R.id.activity_results_captured_image);
        TextView textMibi = (TextView) findViewById(R.id.activity_results_mibi);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            String miSnapResultCode = extras.getString(MiSnapApi.RESULT_CODE);

            switch (miSnapResultCode) {
                // MiSnap check capture
                case FacialCaptureApi.RESULT_SPOOF_DETECTED:
                    getSupportActionBar().setTitle("SPOOF DETECTED");
                    getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#FFEC0000")));
                    break;
                case FacialCaptureApi.RESULT_UNVERIFIED_STILL:
                    getSupportActionBar().setTitle("UNVERIFIED");
                    getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#FFEC5300")));
                    break;
                case FacialCaptureApi.RESULT_UNVERIFIED:
                    getSupportActionBar().setTitle("WEAK VERIFICATION");
                    getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#FFEC9B00")));
                    break;
                case MiSnapApi.RESULT_SUCCESS_STILL:
                    getSupportActionBar().setTitle("MODERATE VERIFICATION");
                    getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#FFECCA00")));
                    break;
                case MiSnapApi.RESULT_SUCCESS_VIDEO:
                    getSupportActionBar().setTitle("STRONG VERIFICATION");
                    break;
            }
            String mibi = extras.getString(MiSnapApi.RESULT_MIBI_DATA);
            Log.i(TAG, "MIBI: " + mibi);
            byte[] image = getIntent().getByteArrayExtra(MiSnapApi.RESULT_PICTURE_DATA);
            imageCaptured.setImageBitmap(formBitmapImage(image));
            textMibi.setText(mibi);
        }

        Button buttonDone = (Button) findViewById(R.id.activity_results_continue_button);
        buttonDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private Bitmap formBitmapImage(byte[] byteImage) {
        System.gc();
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = false;
        options.inSampleSize = 1;    // downscale by 2 as it's just a review

        Bitmap sourceBmp = BitmapFactory.decodeByteArray(byteImage, 0, byteImage.length, options);
        int height = sourceBmp.getHeight();
        int width = sourceBmp.getWidth();


        Bitmap targetBmp = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);

        Canvas targetCanvas = new Canvas(targetBmp);

        // draw original bitmap into mutable bitmap
        targetCanvas.drawBitmap(sourceBmp, 0, 0, null);

        sourceBmp.recycle();    // no use anymore

        // draw boxes
        Paint paint = new Paint();
        paint.setColor(Color.RED);
        paint.setTextSize(20);
        paint.setFlags(Paint.ANTI_ALIAS_FLAG);
        paint.setStrokeWidth(3);
        paint.setStyle(Paint.Style.STROKE);

        return targetBmp;
    }
}
