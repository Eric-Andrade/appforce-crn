package com.miteksystems.facialcapture.app;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.miteksystems.facialcapture.science.api.params.FacialCaptureApi;
import com.miteksystems.misnap.params.CameraApi;
import com.miteksystems.misnap.utils.Utils;

import java.util.Map;

public class SettingsActivity extends AppCompatActivity {

    ParameterOverrides mOverrides;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        if(BuildConfig.DEBUG) {
            Utils.unlockAndTurnOnScreen(this);
        }

        mOverrides = new ParameterOverrides(this);
        Map<String, Integer> params = mOverrides.load();

        final EditText widgetCaptureMode = (EditText) findViewById(R.id.settings_capture_mode);
        widgetCaptureMode.setText(String.valueOf(params.get(CameraApi.MiSnapCaptureMode)));

        final EditText widgetEyeMinDistance = (EditText) findViewById(R.id.settings_eye_min_distance);
        widgetEyeMinDistance.setText(String.valueOf(params.get(FacialCaptureApi.EyeMinDistance)));

        final EditText widgetEyeMaxDistance = (EditText) findViewById(R.id.settings_eye_max_distance);
        widgetEyeMaxDistance.setText(String.valueOf(params.get(FacialCaptureApi.EyeMaxDistance)));

        final EditText widgetLightingThreshold = (EditText) findViewById(R.id.settings_lighting_min_threshold);
        widgetLightingThreshold.setText(String.valueOf(params.get(FacialCaptureApi.LightingMinThreshold)));

        final EditText widgetSharpnessMinThreshold = (EditText) findViewById(R.id.settings_sharpness_min_threshold);
        widgetSharpnessMinThreshold.setText(String.valueOf(params.get(FacialCaptureApi.SharpnessMinThreshold)));

        final EditText widgetCaptureEyesOpen = (EditText) findViewById(R.id.settings_capture_eyes_open);
        widgetCaptureEyesOpen.setText(String.valueOf(params.get(FacialCaptureApi.CaptureEyesOpen)));

        Button button = (Button) findViewById(R.id.buttonDone);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setPreferenceValue(CameraApi.MiSnapCaptureMode, widgetCaptureMode);
                setPreferenceValue(FacialCaptureApi.EyeMinDistance, widgetEyeMinDistance);
                setPreferenceValue(FacialCaptureApi.EyeMaxDistance, widgetEyeMaxDistance);
                setPreferenceValue(FacialCaptureApi.LightingMinThreshold, widgetLightingThreshold);
                setPreferenceValue(FacialCaptureApi.SharpnessMinThreshold, widgetSharpnessMinThreshold);
                setPreferenceValue(FacialCaptureApi.CaptureEyesOpen, widgetCaptureEyesOpen);
                mOverrides.save();
                finish();
            }
        });
    }

    private void setPreferenceValue(String prefName, EditText editText) {
        int valueToSave;
        try {
            valueToSave = Integer.valueOf(editText.getText().toString());
        } catch (Exception e) {
            valueToSave = 0;
        }

        mOverrides.setParamValue(prefName, valueToSave);
    }
}
