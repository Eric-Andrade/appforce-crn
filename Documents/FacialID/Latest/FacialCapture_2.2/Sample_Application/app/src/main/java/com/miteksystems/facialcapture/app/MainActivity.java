package com.miteksystems.facialcapture.app;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.miteksystems.facialcapture.science.api.params.FacialCaptureApi;
import com.miteksystems.facialcapture.workflow.FacialCaptureWorkflowActivity;
import com.miteksystems.facialcapture.workflow.params.FacialCaptureWorkflowParameters;
import com.miteksystems.misnap.params.CameraApi;
import com.miteksystems.misnap.params.MiSnapApi;
import com.miteksystems.misnap.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = MainActivity.class.getSimpleName();
    private static final long PREVENT_DOUBLE_CLICK_TIME_MS = 1000;
    private long mTime;

    protected static final String LICENSE_KEY = "{\"signature\":\"AAPYSjiv\\/7f9mnHsq4UkrHiZUpaJERywYdwlVpIVPaxKGXagtdX3tVP72wfhKkcDuIIeDFmA3nM9Hx8uGaIR438zQKChtCzjwF6GfMsxpiSci6\\/EAslK2PqeMVjyr842WWgwCD\\/a32obdFdFY16\\/ebOGXZeQvSlI0yKTAA+WQHyIcrbBYEspz9vXjkVhQxRdQvdvpi9pSdcFlSNKEwFg6iN9dBU+rWv\\/TsGkeCZCPCFKs3UeiepHzqdlzlMvwFgeOeRMYFtWzPaX84JgvqwOMMp7HB1C4DiCMfpwb3DiWLdAMhfaYHG+yQQoM+G7+SjWcGOu0MNzqVrIOSdAf8rPbg==\",\"organization\":\"Daon\",\"signed\":{\"features\":[\"ALL\"],\"expiry\":\"2022-07-15 00:00:00\",\"applicationIdentifier\":\"com.miteksystems.*\"},\"version\":\"2.1\"}";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if(BuildConfig.DEBUG) {
            Utils.unlockAndTurnOnScreen(this);
        }

        String versionString =
                "Mobile Verify Facial Capture SDK"
                + "\nVersionName: " + getString(R.string.sdk_versionName)
                + "\nVersionCode: " + getString(R.string.sdk_versionCode)
                + "\nBuildNumber: " + getString(R.string.sdk_buildnumber)
                + "\n\nMiSnap Camera"
                + "\nVersionName: " + getString(R.string.misnap_versionName)
                + "\nVersionCode: " + getString(R.string.misnap_versionCode)
                + "\nBuildNumber: " + getString(R.string.misnap_buildnumber);
        Log.d(TAG, versionString);
        final TextView versionInfo = (TextView) findViewById(R.id.version_info);
        versionInfo.setText(versionString);

        Button buttonStartFacialCapture = (Button) findViewById(R.id.buttonStartFacialCapture);
        buttonStartFacialCapture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startFacialCaptureWorkflow();
            }
        });

        Button settingsButton = (Button) findViewById(R.id.buttonSettings);
        settingsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MainActivity.this.startActivity(new Intent(MainActivity.this, SettingsActivity.class));
            }
        });
    }

    private void startFacialCaptureWorkflow() {
        // Prevent multiple MiSnap instances by preventing multiple button presses
        if (System.currentTimeMillis() - mTime < PREVENT_DOUBLE_CLICK_TIME_MS) {
            // Double-press detected
            return;
        }
        mTime = System.currentTimeMillis();

        // Add in parameter info for MiSnap
        ParameterOverrides overrides = new ParameterOverrides(this);
        Map<String, Integer> paramMap = overrides.load();
        JSONObject jjs = new JSONObject();
        try {
            // MiSnap-specific parameters
            jjs.put(CameraApi.MiSnapAllowScreenshots, 1);

            // Add FacialCapture-specific parameters from the Settings Activity, stored in shared preferences
            // NOTE: If you do not set these, the optimized defaults for this SDK version will be used.
            // NOTE: Do not set these unless you are purposefully overriding the defaults!
            for (Map.Entry<String, Integer> param : paramMap.entrySet()) {
                jjs.put(param.getKey(), param.getValue());
            }
            jjs.put(FacialCaptureApi.FacialCaptureLicenseKey, LICENSE_KEY);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JSONObject jjsWorkflow = new JSONObject();
        try {
            // Optionally add in customizable runtime settings for the FacialCapture workflow.
            // NOTE: These don't go into the JOB_SETTINGS because they are for your app, not for core FacialCapture.
            jjsWorkflow.put(FacialCaptureWorkflowParameters.FACIALCAPTURE_WORKFLOW_MESSAGE_DELAY, 500);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Intent intentFacialCapture = new Intent(this, FacialCaptureWorkflowActivity.class);
        intentFacialCapture.putExtra(MiSnapApi.JOB_SETTINGS, jjs.toString());
        intentFacialCapture.putExtra(FacialCaptureWorkflowParameters.EXTRA_WORKFLOW_PARAMETERS, jjsWorkflow.toString());

        startActivityForResult(intentFacialCapture, MiSnapApi.RESULT_PICTURE_CODE);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (MiSnapApi.RESULT_PICTURE_CODE == requestCode) {
            if (RESULT_OK == resultCode) {
                if (data != null) {
                    Intent intentResults = new Intent(this, ResultsActivity.class);
                    intentResults.putExtras(data);
                    startActivity(intentResults);
                } else {
                    // Image canceled, stop
                    Toast.makeText(this, "MiSnap canceled", Toast.LENGTH_SHORT).show();
                }
            } else if (RESULT_CANCELED == resultCode) {
                // Camera not working or not available, stop
                Toast.makeText(this, "MiSnap aborted", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_about) {
            String versionString = "MiSnap SDK"
                    + "\nVersionName: " + getString(R.string.misnap_versionName)
                    + "\nVersionCode: " + getString(R.string.misnap_versionCode)
                    + "\nBuildNumber: " + getString(R.string.misnap_buildnumber);
            Toast.makeText(this, versionString, Toast.LENGTH_SHORT)
                    .show();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
