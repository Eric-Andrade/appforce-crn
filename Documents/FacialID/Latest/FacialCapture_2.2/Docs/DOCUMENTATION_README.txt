DOCUMENTATION_README

The current version of the docs are included as a PDF for your reference. Any updated documentation for the Facial Capture SDK is available online. This helps us ensure that you always have the latest information. You can also leave feedback on each article to help us continually improve the content.

Documentation for the Facial Capture Android SDK is also available here: https://miteksystems.atlassian.net/servicedesk/customer/portal/4/article/800489659 . You will need credentials for the Mitek Knowledge Base.


