package com.miteksystems.misnap.misnapworkflow.params;

/**
 * Created by awood on 10/9/2015.
 *
 * TODO KW 2018-06-03:  which of these is really needed?  should these be merged with SDKConstants?
 */
public class WorkflowConstants {
    public static final int UI_DO_VIBRATE = 40017;
    public static final int UI_FRAGMENT_SNAP_BUTTON_CLICKED = 40019;
    public static final int UI_FRAGMENT_BRIGHTNESS_CHECK_FAILED = 40004;
    public static final int UI_FRAGMENT_ANGLE_CHECK_FAILED = 40006;
    public static final int UI_FRAGMENT_HORI_FILL_CHECK_FAILED = 40008;
    public static final int UI_FRAGMENT_SHARPNESS_CHECK_FAILED = 40010;
    public static final int UI_FRAGMENT_MAX_BRIGHTNESS_CHECK_FAILED = 40022;
    public static final String TEXT_OVERLAY_CHECK_FRONT = "Front Image";
    public static final String TEXT_OVERLAY_CHECK_BACK = "Back Image";
}
