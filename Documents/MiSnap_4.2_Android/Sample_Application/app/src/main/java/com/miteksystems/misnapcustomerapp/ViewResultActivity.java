package com.miteksystems.misnapcustomerapp;

import android.app.Activity;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;

import com.miteksystems.misnap.analyzer.MiSnapAnalyzerResult;
import com.miteksystems.misnap.params.MiSnapApi;

import java.util.ArrayList;
import java.util.List;

public class ViewResultActivity extends Activity implements OnClickListener {

    private Bitmap imageBitmap;
	private TouchImageView mTouchImageView = null;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// hide the title bar
		requestWindowFeature(Window.FEATURE_NO_TITLE);

		//Suppress keyboard
		getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

		// set content
		setContentView(R.layout.view_result);

		Button menuButton = (Button) findViewById(R.id.menuButton);
		menuButton.setOnClickListener(this);

		mTouchImageView = (TouchImageView) findViewById(R.id.result_screen_touchimageview);
		mTouchImageView.setMaxZoom(4f);
		LoadImage();
		if (!mHasImageBeenLoaded) {
			mTouchImageView.setVisibility(View.GONE);
		}

		EditText resultCode = (EditText) findViewById(R.id.misnap_app_result_code);
		resultCode.setText(getIntent().getStringExtra(MiSnapApi.RESULT_CODE));

		EditText warnings = (EditText) findViewById(R.id.misnap_app_warnings);
		ArrayList<String> warningCodes = getIntent().getStringArrayListExtra(MiSnapApi.RESULT_WARNINGS);
		warnings.setText(getWarningCodeMessage(warningCodes));

		EditText fourCorners = (EditText) findViewById(R.id.misnap_app_four_corners);
		ArrayList<Point> fourCornersResult = getIntent().getParcelableArrayListExtra(MiSnapApi.RESULT_FOUR_CORNERS);
		fourCorners.setText(null == fourCornersResult ? "not present in return Intent" : fourCornersResult.toString());

		EditText mibiData = (EditText) findViewById(R.id.misnap_app_mibi_data);
		mibiData.setText(getIntent().getStringExtra(MiSnapApi.RESULT_MIBI_DATA));
	}


	//@Override
	public void onClick(View view) {
		switch (view.getId()) {
			case R.id.menuButton:
				//startActivity(new Intent(this, HomeActivity.class));
				finish();
				break;
		}
	}
	
	/* METHODS =============================================================*/

	private boolean mHasImageBeenLoaded = false;

	private void LoadImage() {

		if (mHasImageBeenLoaded) return;

		// check the initial result for an image
		byte[] rawImage = this.getIntent().getByteArrayExtra(MiSnapApi.RESULT_PICTURE_DATA);
		if (rawImage == null || rawImage.length == 0) {
			return;
		}

		if (rawImage != null && rawImage.length > 0) {
			imageBitmap = formBitmapImage(rawImage);
			mTouchImageView.setImageBitmap(imageBitmap);
			mHasImageBeenLoaded = true;
		} else {
			//new GetPhoneTransactionTask().execute();
		}
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);

		// Checks the orientation of the screen
		if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
			mTouchImageView.setVisibility(View.GONE);
		} else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) {
			mTouchImageView.setVisibility(View.VISIBLE);
		}
	}

	private Bitmap formBitmapImage(byte[] byteImage) {
		System.gc();
		final BitmapFactory.Options options = new BitmapFactory.Options();
		options.inJustDecodeBounds = false;
		options.inSampleSize = 1;    // downscale by 2 as it's just a review

		Bitmap sourceBmp = BitmapFactory.decodeByteArray(byteImage, 0, byteImage.length, options);
		int height = sourceBmp.getHeight();
		int width = sourceBmp.getWidth();


		Bitmap targetBmp = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);

		Canvas targetCanvas = new Canvas(targetBmp);

		// draw original bitmap into mutable bitmap
		targetCanvas.drawBitmap(sourceBmp, 0, 0, null);

		sourceBmp.recycle();    // no use anymore

		// draw boxes
		Paint paint = new Paint();
		paint.setColor(Color.RED);
		paint.setTextSize(20);
		paint.setFlags(Paint.ANTI_ALIAS_FLAG);
		paint.setStrokeWidth(3);
		paint.setStyle(Paint.Style.STROKE);

		return targetBmp;
	}

	private String getWarningCodeMessage(List<String> warningCodes) {
		StringBuilder result = new StringBuilder();

		for (String warningCode : warningCodes) {
            MiSnapAnalyzerResult.FrameChecks frameCheck = MiSnapAnalyzerResult.FrameChecks.valueOf(warningCode);
			switch (frameCheck) {
				case FOUR_CORNER_CONFIDENCE:
					result.append(this.getResources().getString(R.string.misnap_failure_reason_four_corner_confidence));
					break;
				case HORIZONTAL_MINFILL:
					result.append(this.getResources().getString(R.string.misnap_failure_reason_horizontal_min_fill));
					break;
				case MIN_BRIGHTNESS:
					result.append(this.getResources().getString(R.string.misnap_failure_reason_min_brightness));
					break;
				case MAX_BRIGHTNESS:
					result.append(this.getResources().getString(R.string.misnap_failure_reason_max_brightness));
					break;
				case MAX_SKEW_ANGLE:
					result.append(this.getResources().getString(R.string.misnap_failure_reason_skew_angle));
					break;
				case SHARPNESS:
					result.append(this.getResources().getString(R.string.misnap_failure_reason_sharpness));
					break;
				case MIN_PADDING:
					result.append(this.getResources().getString(R.string.misnap_failure_reason_min_padding));
					break;
				case ROTATION_ANGLE:
					result.append(this.getResources().getString(R.string.misnap_failure_reason_rotation_angle));
					break;
				case LOW_CONTRAST:
					result.append(this.getResources().getString(R.string.misnap_failure_reason_low_contrast));
					break;
				case BUSY_BACKGROUND:
					result.append(this.getResources().getString(R.string.misnap_failure_reason_busy_background));
					break;
				case WRONG_DOCUMENT:
					result.append(this.getResources().getString(R.string.misnap_failure_reason_wrong_doc_generic));
					break;
				case GLARE:
					result.append(this.getResources().getString(R.string.misnap_failure_reason_glare));
					break;
			}
			result.append('\n');
		}
		return result.toString();
	}
}