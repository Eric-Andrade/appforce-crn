package com.miteksystems.misnapcustomerapp;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.miteksystems.misnap.analyzer.MiSnapAnalyzerResult;
import com.miteksystems.misnap.misnapworkflow.MiSnapWorkflowActivity;
import com.miteksystems.misnap.misnapworkflow_UX2.MiSnapWorkflowActivity_UX2;
import com.miteksystems.misnap.misnapworkflow_UX2.params.WorkflowApi;
import com.miteksystems.misnap.params.BarcodeApi;
import com.miteksystems.misnap.params.CameraApi;
import com.miteksystems.misnap.params.CreditCardApi;
import com.miteksystems.misnap.params.MiSnapApi;
import com.miteksystems.misnap.params.ScienceApi;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

// If your app extends AppCompatActivity, then your theme in styles.xml
// must extend Theme.AppCompat.
public class MainActivity extends Activity {
    private static final String TAG = "MainActivity";
    private static final long PREVENT_DOUBLE_CLICK_TIME_MS = 1000;
    private long mTime;
    private static int mUxWorkflow;
    private static int mGeoRegion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        String versionString = "MiSnap SDK"
                + "\nVersionName: " + getString(R.string.misnap_versionName)
                + "\nVersionCode: " + getString(R.string.misnap_versionCode)
                + "\nBuildNumber: " + getString(R.string.misnap_buildnumber);
        Log.d(TAG, versionString);
        final TextView versionInfo = (TextView) findViewById(R.id.version_info);
        versionInfo.setText(versionString);

        // Built-in MiSnap samples
        final Button buttonCheckFront = (Button) findViewById(R.id.buttonMiSnapCheckFront);
        buttonCheckFront.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mGeoRegion = ScienceApi.GEO_REGION_US;
                startMiSnapWorkflow(MiSnapApi.PARAMETER_DOCTYPE_CHECK_FRONT);
            }
        });

        final Button buttonCheckFrontGlobal = (Button) findViewById(R.id.buttonMiSnapCheckFrontGlobal);
        buttonCheckFrontGlobal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mGeoRegion = ScienceApi.GEO_REGION_GLOBAL;
                startMiSnapWorkflow(MiSnapApi.PARAMETER_DOCTYPE_CHECK_FRONT);
            }
        });

        final Button buttonCheckBack = (Button) findViewById(R.id.buttonMiSnapCheckBack);
        buttonCheckBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startMiSnapWorkflow(MiSnapApi.PARAMETER_DOCTYPE_CHECK_BACK);
            }
        });

        final Button buttonDriversLicense = (Button) findViewById(R.id.buttonMiSnapDriversLicense);
        buttonDriversLicense.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startMiSnapWorkflow(MiSnapApi.PARAMETER_DOCTYPE_DRIVER_LICENSE);
            }
        });

        final Button buttonIdCardFront = (Button) findViewById(R.id.buttonMiSnapIdCardFront);
        buttonIdCardFront.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startMiSnapWorkflow(MiSnapApi.PARAMETER_DOCTYPE_ID_CARD_FRONT);
            }
        });

        final Button buttonIdCardBack = (Button) findViewById(R.id.buttonMiSnapIdCardBack);
        buttonIdCardBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startMiSnapWorkflow(MiSnapApi.PARAMETER_DOCTYPE_ID_CARD_BACK);
            }
        });

        final Button buttonBusinessCard = (Button) findViewById(R.id.buttonMiSnapBusinessCard);
        buttonBusinessCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startMiSnapWorkflow(MiSnapApi.PARAMETER_DOCTYPE_BUSINESS_CARD);
            }
        });

        final Button buttonAutoInsurance = (Button) findViewById(R.id.buttonMiSnapAutoInsurance);
        buttonAutoInsurance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startMiSnapWorkflow(MiSnapApi.PARAMETER_DOCTYPE_AUTO_INSURANCE);
            }
        });

        final Button buttonPassport = (Button) findViewById(R.id.buttonMiSnapPassport);
        buttonPassport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startMiSnapWorkflow(MiSnapApi.PARAMETER_DOCTYPE_PASSPORT);
            }
        });

        // Third-party library samples
        final Button buttonCreditCard = (Button) findViewById(R.id.buttonCreditCard);
        buttonCreditCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startMiSnapWorkflow(MiSnapApi.PARAMETER_DOCTYPE_CREDIT_CARD);
            }
        });

        final Button buttonPDF417 = (Button) findViewById(R.id.buttonPDF417);
        buttonPDF417.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startMiSnapWorkflow(MiSnapApi.PARAMETER_DOCTYPE_PDF417);
            }
        });

        final Button buttonBarcodes = (Button) findViewById(R.id.buttonBarcodes);
        buttonBarcodes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startMiSnapWorkflow(MiSnapApi.PARAMETER_DOCTYPE_BARCODES);
            }
        });

        // Allow easy switching between UX1 and UX2 so you can choose the best experience for your app
        final Button buttonUx1 = (Button) findViewById(R.id.UX1);
        final Button buttonUx2 = (Button) findViewById(R.id.UX2);
        buttonUx1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mUxWorkflow = 1;
                buttonUx1.setTextColor(Color.YELLOW);
                buttonUx2.setTextColor(Color.WHITE);
            }
        });

        buttonUx2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mUxWorkflow = 2;
                buttonUx1.setTextColor(Color.WHITE);
                buttonUx2.setTextColor(Color.YELLOW);
            }
        });

        buttonUx2.performClick(); // Default to UX2
    }

    private void startMiSnapWorkflow(String docType) {
        // Prevent multiple MiSnap instances by preventing multiple button presses
        if (System.currentTimeMillis() - mTime < PREVENT_DOUBLE_CLICK_TIME_MS) {
            Log.e("UxStateMachine", "Double-press detected");
            return;
        }

        mTime = System.currentTimeMillis();
        JSONObject misnapParams = new JSONObject();
        try {
            misnapParams.put(MiSnapApi.MiSnapDocumentType, docType);
            if (docType.equals(MiSnapApi.PARAMETER_DOCTYPE_CHECK_FRONT)) {
                misnapParams.put(ScienceApi.MiSnapGeoRegion, mGeoRegion);
            }


            // Example of how to add additional barcode scanning options
            if (docType.equals(MiSnapApi.PARAMETER_DOCTYPE_BARCODES)) {
                // Set everything except Code 128 because it conflicts w/ the PDF417 barcode on the back of most drivers licenses.
                misnapParams.put(BarcodeApi.BarCodeTypes, BarcodeApi.BARCODE_ALL - BarcodeApi.BARCODE_CODE_128);
            }

            // Here you can override optional API parameter defaults
            misnapParams.put(CameraApi.MiSnapAllowScreenshots, 1);
            // e.g. misnapParams.put(MiSnapApi.AppVersion, "1.0");
            // Workflow parameters are now put into the same JSONObject as MiSnap parameters
            misnapParams.put(WorkflowApi.MiSnapTrackGlare, WorkflowApi.TRACK_GLARE_ENABLED);
            //misnapParams.put(CameraApi.MiSnapFocusMode, CameraApi.PARAMETER_FOCUS_MODE_HYBRID_NEW);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Intent intentMiSnap;
        if (mUxWorkflow == 1) {
            intentMiSnap = new Intent(this, MiSnapWorkflowActivity.class);
        } else {
            intentMiSnap = new Intent(this, MiSnapWorkflowActivity_UX2.class);
        }
        intentMiSnap.putExtra(MiSnapApi.JOB_SETTINGS, misnapParams.toString());
        startActivityForResult(intentMiSnap, MiSnapApi.RESULT_PICTURE_CODE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (MiSnapApi.RESULT_PICTURE_CODE == requestCode) {
            if (RESULT_OK == resultCode) {
                if (data != null) {
                    Bundle extras = data.getExtras();
                    String miSnapResultCode = extras.getString(MiSnapApi.RESULT_CODE);
                    if (!miSnapResultCode.equals(MiSnapApi.RESULT_SUCCESS_CREDIT_CARD)) {
                        Intent displayResults = new Intent(this, ViewResultActivity.class);
                        displayResults.putExtras(data);
                        displayResults.putExtra(MiSnapApi.RESULT_CODE, extras.getString(MiSnapApi.RESULT_CODE));
                        startActivity(displayResults);
                    }

                    switch (miSnapResultCode) {
                        // MiSnap check capture
                        case MiSnapApi.RESULT_SUCCESS_VIDEO:
                        case MiSnapApi.RESULT_SUCCESS_STILL:
                            Log.i(TAG, "MIBI: " + extras.getString(MiSnapApi.RESULT_MIBI_DATA));

                            // Image returned successfully
                            byte[] sImage = data.getByteArrayExtra(MiSnapApi.RESULT_PICTURE_DATA);

                            // Now Base64-encode the byte array before sending it to the server.
                            // e.g. byte[] sEncodedImage = Base64.encode(sImage, Base64.DEFAULT);
                            //      sendToServer(sEncodedImage);

                            List<String> warnings = extras.getStringArrayList(MiSnapApi.RESULT_WARNINGS);
                            if (warnings != null && !warnings.isEmpty()) {
                                String message = "WARNINGS:";
                                if ((warnings.contains(MiSnapAnalyzerResult.FrameChecks.WRONG_DOCUMENT.name()))) {
                                    message += "\nWrong document detected";
                                }
                                Toast.makeText(this, message, Toast.LENGTH_LONG).show();
                            }

                            // If document was an ID_CARD_BACK with the MiSnapHybridController, display the barcode if it was extracted
                            displayBarcodeResult(data);

                            break;

                        // Card.io credit card capture
                        case MiSnapApi.RESULT_SUCCESS_CREDIT_CARD:
                            StringBuilder creditCardInfo = new StringBuilder();
                            creditCardInfo.append("Redacted number: " + extras.getString(CreditCardApi.CREDIT_CARD_REDACTED_NUMBER) + "\n");
                            // Never log a raw card number. Avoid displaying it.
                            creditCardInfo.append("Card number: *HIDDEN*\n"); // + extras.getString(MiSnapApi.CREDIT_CARD_NUMBER));
                            creditCardInfo.append("Formatted number: *HIDDEN*\n"); //+ extras.getString(MiSnapApi.CREDIT_CARD_FORMATTED_NUMBER));
                            creditCardInfo.append("Card type: " + extras.getString(CreditCardApi.CREDIT_CARD_TYPE) + "\n");
                            creditCardInfo.append("CVV: " + extras.getInt(CreditCardApi.CREDIT_CARD_CVV) + "\n");
                            creditCardInfo.append("Expiration month: " + extras.getInt(CreditCardApi.CREDIT_CARD_EXPIRY_MONTH) + "\n");
                            creditCardInfo.append("Expiration year: " + extras.getInt(CreditCardApi.CREDIT_CARD_EXPIRY_YEAR) + "\n");
                            new AlertDialog.Builder(this).setTitle("Card.io Data").setMessage(creditCardInfo.toString()).show();
                            Log.i(TAG, creditCardInfo.toString());

                            break;

                        // Barcode capture
                        case MiSnapApi.RESULT_SUCCESS_PDF417:
                            displayBarcodeResult(data);
                            break;
                    }
                } else {
                    // Image canceled, stop
                    Toast.makeText(this, "MiSnap canceled", Toast.LENGTH_SHORT).show();
                }
            } else if (RESULT_CANCELED == resultCode) {
                // Camera not working or not available, stop
                Toast.makeText(this, "Operation canceled!!!", Toast.LENGTH_SHORT).show();
                if (data != null) {
                    Bundle extras = data.getExtras();
                    String miSnapResultCode = extras.getString(MiSnapApi.RESULT_CODE);
                    if (!miSnapResultCode.isEmpty()) {
                        Toast.makeText(this, "Shutdown reason: " + miSnapResultCode, Toast.LENGTH_SHORT).show();
                    }
                }
            }
        }
    }

    private void displayBarcodeResult(Intent returnIntent) {
        String sUnformattedStr = returnIntent.getExtras().getString(BarcodeApi.RESULT_PDF417_DATA);
        Log.i(TAG, "PDF417 data: " + sUnformattedStr);
        if (sUnformattedStr != null) {
            //display the pdf417 data
            new AlertDialog.Builder(this).setTitle("Barcode Data").setMessage(sUnformattedStr).show();
        }
    }
}