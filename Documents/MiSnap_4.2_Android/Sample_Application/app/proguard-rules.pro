# Add project specific ProGuard rules here.
# By default, the flags in this file are appended to flags specified
# in /Users/asingal/Documents/Mitek/Android/android-sdk-macosx/tools/proguard/proguard-android.txt
# You can edit the include path and order by changing the proguardFiles
# directive in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# Add any project specific keep options here:

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}

-keepattributes InnerClasses,EnclosingMethod

# Expose fewer MiSnap classes in the future
-keep class com.miteksystems.misnap.params.SDKConstants
-keepclassmembers class com.miteksystems.misnap.params.SDKConstants { *; }
-keep class com.miteksystems.misnap.params.CameraParamMgr
-keepclassmembers class com.miteksystems.misnap.params.CameraParamMgr { *; }
-keep class com.miteksystems.misnap.params.MiSnapConstants
-keepclassmembers class com.miteksystems.misnap.params.MiSnapConstants { *; }
-keep class com.miteksystems.misnap.storage.CameraInfoCacher
-keepclassmembers class com.miteksystems.misnap.storage.CameraInfoCacher { *; }
-keep class com.miteksystems.misnap.utils.Utils
-keepclassmembers class com.miteksystems.misnap.utils.Utils { *; }


# Atimi changes
-optimizationpasses 5
-dontusemixedcaseclassnames
-dontskipnonpubliclibraryclasses
-optimizations !code/simplification/arithmetic,!field/*,!class/merging/*

# Disable Android logs
-assumenosideeffects class android.util.Log {
    public static boolean isLoggable(java.lang.String, int);
    public static int v(...);
    public static int i(...);
    public static int w(...);
    public static int d(...);
    public static int e(...);
}

# Keep support library
-keep public class * extends android.support.v4.** {*;}

# Keep MiSnap API classes
-keep class com.miteksystems.misnap.analyzer.*
-keepclassmembers class com.miteksystems.misnap.analyzer.* { *; }
-keep class com.miteksystems.misnap.analyzer.MiSnapAnalyzer$MiSnapAnalyzerExtraInfo { *; }

-keep class com.miteksystems.misnap.documents.*
-keepclassmembers class com.miteksystems.misnap.documents.* { *; }
-keep class com.miteksystems.misnap.events.*
-keepclassmembers class com.miteksystems.misnap.events.* { *; }
-keepclassmembers class ** {
    public void onEvent*(**);
}
-keepattributes *Annotation*
-keepclassmembers class ** {
    @org.greenrobot.eventbus.Subscribe <methods>;
}
-keep enum org.greenrobot.eventbus.ThreadMode { *; }

# Only required if you use AsyncExecutor
-keepclassmembers class * extends org.greenrobot.eventbus.util.ThrowableFailureEvent {
    <init>(java.lang.Throwable);
}
-keep class com.miteksystems.misnap.params.*
-keepclassmembers class com.miteksystems.misnap.params.* { *; }

# Keep MiSnap Workflow classes
-keep class com.miteksystems.misnap.misnapworkflow.**
-keepclassmembers class com.miteksystems.misnap.misnapworkflow.** { *; }
-keep class com.miteksystems.misnap.misnapworkflow_UX2.**
-keepclassmembers class com.miteksystems.misnap.misnapworkflow_UX2.** { *; }

# Keep card.io classes
-keep class io.card.**
-keepclassmembers class io.card.** { *; }

# Don't mess with classes with native methods
-keepclasseswithmembers class * {
    native <methods>;
}
-keepclasseswithmembernames class * {
    native <methods>;
}
-keep public class io.card.payment.* {
    public protected *;
}

# Keep card.io wrapper classes
-keep class com.miteksystems.misnap.cardio.**
-keepclassmembers class com.miteksystems.misnap.cardio.** { *; }

# Keep barcode scanning
-keep class com.miteksystems.misnap.barcode.**
-keepclassmembers class com.miteksystems.misnap.barcode.** { *; }
-dontwarn com.miteksystems.misnap.barcode.R*