import React, {useContext, useEffect, useState} from 'react';
import {
  StyleSheet,
  Text,
  View,
  ActivityIndicator,
  TouchableOpacity,
  TextInput,
  KeyboardAvoidingView,
  Keyboard,
  TouchableWithoutFeedback
} from 'react-native';
import {Context} from '../context/AuthContext';
import {useModal} from '../components/useModal';
import InfoModal from '../components/InfoModal';

import Spinner from 'react-native-loading-spinner-overlay';
const PinVal = ({navigation}) => {
  const [itemModalOpen, setItemModalOpen, toggleModal] = useModal();
  const {state, PinCode, PinMsg} = useContext(Context);
  console.log('PinVal state: ', state);
  const [vCode, setvCode] = useState('');
  const [isLoading, setisLoading] = useState(false);
  const [Visible, setVisible] = useState(false);
  const [lockB, setlockB] = useState(false);

  useEffect(() => {
    async function PinMsg() {
      const vSolicitudeId = await AsyncStorage.getItem('SolicitudeId');
      const response = await ForceApi.post(
        '/SendValidationCodeController.php',
        {vSolicitudeId},
      );
      const pay = {
        id: response.data.id ? response.data.id : null,
        message: response.data.message,
        error: response.data.error,
      };
      dispatch({type: 'guardar', payload: pay});
      console.log(response.data.message);
      console.log(response.data.code);
    }
    PinMsg();
  }, []);
  /*
  useEffect(() => {
    if (vCode) {
      PinCode({vCode});
    }
  }, [vCode]);*/
  CheckTextInput = () => {
    setlockB(true);
    setisLoading(false);
    setVisible(true);
    PinCode({vCode});
    setVisible(true);
    toggleModal(true);
  };
  CloseModal = () => {
    setItemModalOpen(false);
    setlockB(false);
  };

  useEffect(() => {
    let isCancelled = false;
    if (Visible && state.errorMessage != '') {
      setVisible(!Visible);
    } else {
      setVisible(false);
    }
    return () => {
      isCancelled = true;
    };
  }, [state]);

  if (Visible) {
    return (
      <Spinner
        size="large"
        visible={Visible}
        color="#D32345"
        textContent={'Un momento por favor'}
        textStyle={styles.spinnerTextStyle}
      />
    );
  }

  return (
      <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()} style={{ flex: 1 }}>
        <View style={styles.container}>
          {/* <Spinner 
            size="large"
            visible={isLoading}
            color ='#D32345'
            textContent={'Cargando Datos'}
            textStyle={styles.spinnerTextStyle}
            /> */}
          {state.error == true ? (
            <InfoModal
              text={`${state.errorMessage}`}
              isActive={itemModalOpen}
              handleClose={this.CloseModal}
            />
          ) : null}
          <View>
            <Text style={styles.textIniciar}>Verificación de PIN</Text>
          </View>
          <View>
            <Text style={styles.Pin}>
              Ingrese el código que hemos enviado al número registrado.
            </Text>
          </View>
          <View style={styles.inputContainer}>
            <TextInput
              style={styles.inputs}
              placeholder="Código"
              placeholderTextColor="white"
              underlineColorAndroid="transparent"
              onChangeText={newvCode => setvCode(newvCode)}
              value={vCode}
              autoCorrect={false}
              keyboardType={'numeric'}
            />
          </View>
          {lockB === false ? 
        ( <TouchableOpacity
            disabled={lockB}
            style={[styles.buttonContainer, styles.loginButton]}
            onPress={this.CheckTextInput}>
            <Text style={styles.loginText}>ENVIAR</Text>
          </TouchableOpacity>) 
          :
          ( <TouchableOpacity
            style={[styles.buttonContainer, styles.loginButton2]}
            disabled={lockB}>
            <ActivityIndicator size="large" color="#fff" />
          </TouchableOpacity>)
        }
          {/* {
                state.errorMessage
                ? <Text>{state.errorMessage}</Text>
                : null
              } */}
          <View style={styles.container2}>
            <Text style={styles.Pin}>
              ¿No se ha recibido el código de verificación?
            </Text>

            <TouchableOpacity onPress={() => PinMsg()}>
              <Text style={styles.textResend}>Reenviar</Text>
            </TouchableOpacity>
          </View>
        </View>
      </TouchableWithoutFeedback>
  );
};

PinVal.navigationOptions = {
  headerShown: false,
};

const styles = StyleSheet.create({
  container2: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  textResend: {
    color: 'white',
    fontWeight: 'bold',
    justifyContent: 'center',
    alignItems: 'center',
    alignContent: 'center',
  },
  spinnerTextStyle: {
    color: '#D32345',
  },
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#333333',
  },
  textIniciar: {
    color: 'white',
    fontSize: 20,
    fontWeight: 'bold',
    marginBottom: 20,
  },
  Pin: {
    color: '#b3b4b5',
    marginLeft: 60,
    marginRight: 60,
    textAlign: 'center',
  },
  inputContainer: {
    backgroundColor: '#5e5e5e',
    borderRadius: 30,
    width: 300,
    height: 45,
    marginTop: 80,
    marginBottom: 60,
    flexDirection: 'row',
    alignItems: 'center',
  },
  inputs: {
    height: 45,
    marginLeft: 16,
    color: 'white',
    flex: 1,
  },
  buttonContainer: {
    height: 45,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 60,
    width: 300,
    borderRadius: 30,
    backgroundColor: 'transparent',
  },
  loginButton: {
    marginTop: 10,
    backgroundColor: '#D32345',
  },
  loginButton2: {
    marginTop: 10,
    backgroundColor: '#911830',
  },
  loginText: {
    color: 'white',
    fontWeight: 'bold',
  },
});

export default PinVal;