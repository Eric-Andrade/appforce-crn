import React, {useContext, useEffect} from 'react';
import {StyleSheet, Text, View, Image, TouchableOpacity, Platform} from 'react-native';
import {Context} from '../context/AuthContext';

const CreditoAprobado = ({navigation}) => {
  const msg = navigation.getParam('msg');
  return (
    <TouchableOpacity
      style={styles.container}
      onPress={() => navigation.navigate('Calculadora')}>
      <Image
        style={styles.manita}
        source={require('../../assets/Thumbs-Up.png')}
      />

      <Text style={styles.textIniciar}>Solicitud Aprobada</Text>
      <Text style={styles.Pin}>
        ¡Felicidades!
      </Text>
      <Text style={styles.Pin}>
        La Solicitud ha sido aprobada
      </Text>
      <View style={styles.Pin2Container}>
        <Text style={styles.Pin2}>{msg}</Text>
      </View>
    </TouchableOpacity>
  );
};

CreditoAprobado.navigationOptions = {
  headerShown: false,
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#333333',
  },
  textIniciar: {
    color: 'white',
    fontSize: 20,
    fontWeight: 'bold',
    marginBottom: 20,
  },
  Pin2: {
    color: 'white',
    fontSize: 16,
    fontWeight: Platform.OS === 'ios' ? '400' :  '900',
    textAlign: 'justify',
    marginLeft: 40,
    marginRight: 40,
  },
  Pin2Container: {
    width: '100%',
    marginTop: 20,
  },
  Pin: {
    color: 'white',
    fontSize: 16,
    fontWeight: '900',
    textAlign: 'justify',
    marginLeft: 50,
    marginRight: 50,
  },
  manita: {
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 50,
  },
  perfilimg: {
    width: 120,
    height: 120,
    resizeMode: 'contain',
    borderRadius: 70,
    marginBottom: 50,
    backgroundColor: '#5e5e5e',
  },
});

export default CreditoAprobado;
