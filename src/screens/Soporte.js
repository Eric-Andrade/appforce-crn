import React, {Component} from 'react';
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableOpacity,
  Image,
  Alert,
  ScrollView,
  Linking,
  StatusBar,
  Platform,
} from 'react-native';
// import { AuthSession } from 'expo';
import Ionicons from 'react-native-vector-icons/Ionicons';

const openLink = url => {
  Linking.openURL(url).catch(err => console.error('An error occurred', err));
};

const Soporte = ({navigation}) => {
  return (
    <View style={styles.container}>
      <View style={styles.head}>
        <StatusBar backgroundColor='black' barStyle={Platform.OS === 'ios' ? 'dark-content' : 'light-content' } />
        <Image
          style={styles.logo}
          source={require('../../assets/Logo-Grande.png')}
        />
        <Text style={styles.versionNumber}>v2.0.9</Text>
      </View>

      <View style={styles.backContainer}>
        <View style={styles.textContainer}>
          <View>
            <Text style={styles.textIniciar}>Soporte</Text>
            <Text style={styles.textInitHead}>Datos de contacto</Text>
            <Text style={styles.textInit2}>Atención a Promotores WhatsApp</Text>
            <Text style={styles.textInit} onPress={() =>
                openLink('https://wa.me/525522760020',)
              }>55 2276 0020</Text>

            <Text style={styles.textInit2}>Soporte TI</Text>
            <Text style={styles.textInit}>ti.soporte@exitus.com</Text>
            <Text style={styles.textInit}>55 4170 9900 Ext. 6382 o 6323</Text>
          </View>
        </View>
      </View>

      <ScrollView>
        <View>
          <Text style={styles.textInit3}>Términos y condiciones</Text>
          <Text style={styles.textInit}>
            Los presentes términos y condiciones (en lo sucesivo “TyC”),
            establecen las bases conforme a las cuales Exitus Credit, S.A.P.I.
            de C.V., SOFOM, E.N.R. (en lo sucesivo “Exitus Credit”), pone a
            disposición de sus Usuarios la gestión electrónica y personalizada
            de su solicitud de crédito, es decir, sin la intervención de un
            intermediario. Dicha solicitud de crédito se realizará a través del
            sitio web de Exitus Credit, permitiendo al usuario agilizar los
            trámites requeridos para la autorización del crédito.” Aviso de
            Privacidad Podrás consultarlo en nuestro sitio web:{' '}
            <Text
              style={styles.textInitLink}
              onPress={() =>
                openLink('https://www.exituscredit.com/#/avisoPrivacidad',)
              }>
              https://www.exituscredit.com/#/avisoPrivacidad.
            </Text>
          </Text>
        </View>
      </ScrollView>
    </View>
  );
};

Soporte.navigationOptions = {
  headerShown: false,
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: '#333333',
  },
  logo: {
    height: 70,
    width: 110,
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    marginTop: 40,
    marginBottom: 20,
    marginLeft: 16,
  },
  backContainer: {
    flexDirection: 'column',
    marginBottom: 20,
  },
  head: {
    backgroundColor: '#000',
    // borderRadius:20
    borderTopLeftRadius: 0,
    borderTopRightRadius: 0,
    // borderBottomLeftRadius: 20,
    // borderBottomRightRadius: 20,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'flex-end',
  },
  versionNumber: {
    color: '#b3b4b5',
    marginBottom: 10,
    fontSize: 12,
    marginHorizontal: 10,
  },
  back: {
    marginTop: 2,
    flexDirection: 'column',
    color: 'white',
  },
  textContainer: {
    flexDirection: 'column',
    marginTop: 30,
    marginBottom: 10,
    marginLeft: 10,
  },
  textIniciar: {
    color: 'white',
    width: 300,
    fontSize: 20,
    fontWeight: 'bold',
    textAlign: 'left',
    marginLeft: 25,
  },
  textInit: {
    color: '#b3b4b5',
    textAlign: 'justify',
    marginBottom: 10,
    marginHorizontal: 40,
  },
  textInitLink: {
    color: '#e6e6e6',
    textAlign: 'justify',
    marginBottom: 25,
    marginHorizontal: 40,
  },
  textInit2: {
    fontSize: 16,
    color: '#e6e6e6',
    textAlign: 'left',
    marginLeft: 25,
    marginBottom: 10,
  },
  textInitHead: {
    fontSize: 15,
    color: '#b3b4b5',
    textAlign: 'left',
    marginLeft: 25,
    marginBottom: 10,
  },
  textInit3: {
    fontSize: 16,
    color: '#b3b4b5',
    textAlign: 'left',
    marginLeft: 25,
    marginBottom: 20,
    fontWeight: 'bold',
  },
  buttonContainer2: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  logout: {
    marginTop: 20,
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 20,
    width: 142,
    borderRadius: 30,
    height: 45,
    backgroundColor: '#911830',
  },
  buttonContainer: {
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  viewData: {
    marginTop: 20,
    marginLeft: 20,
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 20,
    width: 142,
    borderRadius: 30,
    height: 45,
    backgroundColor: '#D32345',
  },
  loginText: {
    color: 'white',
    fontWeight: 'bold',
  },
  inputContainer: {
    backgroundColor: '#5e5e5e',
    borderRadius: 30,
    width: 250,
    height: 45,
    marginBottom: 20,
    flexDirection: 'row',
    alignItems: 'center',
  },

  SinputContainer: {
    backgroundColor: '#5e5e5e',
    borderRadius: 30,
    width: 145,
    height: 45,
    marginBottom: 20,
    flexDirection: 'row',
    alignItems: 'center',
  },

  SinputContainer2: {
    backgroundColor: '#5e5e5e',
    borderRadius: 30,
    width: 145,
    height: 45,
    marginBottom: 20,
    flexDirection: 'row',
    alignItems: 'center',
    marginLeft: 10,
  },
  inputs: {
    height: 45,
    marginLeft: 16,
    color: 'white',
    flex: 1,
  },
});

export default Soporte;
