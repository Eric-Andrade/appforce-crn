import React, {Component} from 'react';
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableOpacity,
  Image,
  Alert,
  ScrollView,
} from 'react-native';
// // import { AuthSession } from 'expo';
import Ionicons from 'react-native-vector-icons/Ionicons';
import Header from '../components/Header';

const SolAutorizacion = ({navigation}) => {
  return (
    <View style={styles.container}>
      <Header />

      <View style={styles.backContainer}>
        <View style={styles.textContainer}>
          <TouchableOpacity onPress={() => navigation.goBack()}>
            <Ionicons style={styles.back} name="ios-arrow-back" size={27} />
          </TouchableOpacity>
          <View>
            <Text style={styles.textIniciar}>Nueva Solicitud</Text>
            <Text style={styles.textInit}>Autorización de consulta a Buro</Text>
          </View>
        </View>
      </View>

      <ScrollView>
        <View>
          <Text style={styles.textInit}>
          Autorizo expresamente a Exitus Credit, S.A.P.I DE C.V., SOFOM, E.N.R, para que lleve a cabo Investigaciones, sobre mi comportamiento Crediticio en SIC que estime conveniente. Conozco la naturaleza y alcance de la información que se solicitará, del uso que se le dará y que se podrá realizar consultas periódicas de mi historial crediticio, consiento que esta autorización tenga vigencia de 3 años contando a partir de hoy, y en su caso mientras mantengamos relación jurídica.
          </Text>
        </View>
      </ScrollView>

      <View style={styles.buttonContainer2}>
        <TouchableOpacity
          style={styles.logout}
          onPress={() => navigation.navigate('Seleccion')}>
          <Text style={styles.loginText}>NO AUTORIZO</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.viewData}
          onPress={() => navigation.navigate('PinMsg')}>
          <Text style={styles.loginText}>AUTORIZO</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

SolAutorizacion.navigationOptions = {
  headerShown: false,
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: '#333333',
  },
  logo: {
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    marginTop: 40,
    marginBottom: 20,
    marginLeft: 10,
  },
  backContainer: {
    flexDirection: 'column',
    marginBottom: 20,
  },
  head: {
    backgroundColor: '#000',
    // borderRadius:20
    borderTopLeftRadius: 0,
    borderTopRightRadius: 0,
    borderBottomLeftRadius: 20,
    borderBottomRightRadius: 20,
  },
  back: {
    marginTop: 2,
    flexDirection: 'column',
    color: 'white',
  },
  textContainer: {
    flexDirection: 'row',
    marginTop: 30,
    marginBottom: 10,
    marginLeft: 10,
  },
  textIniciar: {
    color: 'white',
    width: 300,
    fontSize: 20,
    fontWeight: 'bold',
    textAlign: 'left',
    marginLeft: 25,
  },
  textInit: {
    color: '#b3b4b5',
    textAlign: 'left',
    marginLeft: 25,
  },
  textInit: {
    color: '#b3b4b5',
    textAlign: 'left',
    marginLeft: 25,
  },
  buttonContainer2: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  logout: {
    marginTop: 20,
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 20,
    width: 142,
    borderRadius: 30,
    height: 45,
    backgroundColor: '#911830',
  },
  buttonContainer: {
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  viewData: {
    marginTop: 20,
    marginLeft: 20,
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 20,
    width: 142,
    borderRadius: 30,
    height: 45,
    backgroundColor: '#D32345',
  },
  loginText: {
    color: 'white',
    fontWeight: 'bold',
  },
  inputContainer: {
    backgroundColor: '#5e5e5e',
    borderRadius: 30,
    width: 300,
    height: 45,
    marginBottom: 20,
    flexDirection: 'row',
    alignItems: 'center',
  },

  SinputContainer: {
    backgroundColor: '#5e5e5e',
    borderRadius: 30,
    width: 145,
    height: 45,
    marginBottom: 20,
    flexDirection: 'row',
    alignItems: 'center',
  },

  SinputContainer2: {
    backgroundColor: '#5e5e5e',
    borderRadius: 30,
    width: 145,
    height: 45,
    marginBottom: 20,
    flexDirection: 'row',
    alignItems: 'center',
    marginLeft: 10,
  },
  inputs: {
    height: 45,
    marginLeft: 16,
    color: 'white',
    flex: 1,
  },
});

export default SolAutorizacion;
