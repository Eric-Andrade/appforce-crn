import React, {useContext, useState} from 'react';
import {StyleSheet, View} from 'react-native';

import {Context} from '../context/AuthContext';
import AuthForm from '../components/AuthForm';
import {NavigationEvents} from 'react-navigation';

const IniciarSesion = () => {
  const {state, login, clearErrorMessage} = useContext(Context);
  onPassPress = () => {
    this.setState({
      secureTextEntry: !this.state.secureTextEntry,
    });
  };

  return (
    <View style={styles.container}>
      <NavigationEvents onWillBlur={clearErrorMessage} />
      <AuthForm
        state={state}
        errorMessage={state.errorMessage}
        onSubmit={login}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#333333',
  },
});

/* onPress={() => this.props.navigation.navigate('Nueva')}>
  <TouchableOpacity style={styles.frgt} onPress={() => this.props.navigation.navigate('Solicitudes')}>
              <Text style={styles.textByRegister}>¿Olvidaste tu contraseña?</Text>*/

export default IniciarSesion;
