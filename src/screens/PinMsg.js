import React, {useContext, useEffect} from 'react';
import {StyleSheet, Text, TouchableOpacity} from 'react-native';
import {Context} from '../context/AuthContext';

const PinMsg = ({navigation}) => {
  const {state, PinMsg} = useContext(Context);

  useEffect(() => {
    PinMsg();
  }, []);

  return (
    <TouchableOpacity
      style={styles.container}
      onPress={() => navigation.navigate('PinVal')}>
      <Text style={styles.textIniciar}>Se enviará un</Text>
      <Text style={styles.Pin}>PIN de consulta</Text>
      <Text style={styles.textIniciar}>al número registrado</Text>
    </TouchableOpacity>
  );
};

PinMsg.navigationOptions = {
  headerShown: false,
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#333333',
  },
  textIniciar: {
    color: 'white',
    fontSize: 20,
    fontWeight: 'bold',
  },
  Pin: {
    color: '#b3b4b5',
  },
});

export default PinMsg;
