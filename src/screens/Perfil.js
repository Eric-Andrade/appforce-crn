import React, {useContext, useState, useEffect} from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Image,
  TextInput,
  ScrollView,
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import {NavigationEvents} from 'react-navigation';
import {Context as UserContext} from '../context/AuthContext';
import ForceApi from '../api/force';
import Header from '../components/Header';

const Perfil = ({navigation}) => {
  const [vName, setvName] = useState('');
  const [vEmail, setvEmail] = useState('');
  const [vNumber, setvNumber] = useState('');
  const [vbranch, setvbranch] = useState('');
  const [vemployee_id, setvemployee_id] = useState('');

  useEffect(() => {
    async function getInst() {
      const id = await AsyncStorage.getItem('id');
      const vIdUser = id;
      const {data} = await ForceApi.post(`/GetProfileController.php`, {
        vIdUser,
      });
      setvName(data.name);
      setvEmail(data.email);
      setvNumber(data.cellphone);
      setvbranch(data.branch);
      setvemployee_id(data.employee_id);
    }
    getInst();
  }, []);

  return (
    <View style={styles.container}>
      <Header />
      <View>
        <Text style={styles.textIniciar}>Mi Perfil</Text>
      </View>
      <View style={styles.perfil}>
        <Text style={styles.perfiltxt}>{vName}</Text>
      </View>
      <ScrollView>
        <View style={styles.buttonContainer}>
          <View style={styles.inputContainer}>
            <TextInput
              style={styles.inputs}
              editable={false}
              selectTextOnFocus={false}
              placeholder={vEmail}
              placeholderTextColor="white"
              underlineColorAndroid="transparent"
            />
          </View>
          <View style={styles.inputContainer}>
            <TextInput
              style={styles.inputs}
              editable={false}
              selectTextOnFocus={false}
              placeholder={vNumber}
              placeholderTextColor="white"
              underlineColorAndroid="transparent"
            />
          </View>
          <View style={styles.inputContainer}>
            <TextInput
              style={styles.inputs}
              editable={false}
              selectTextOnFocus={false}
              placeholder={vbranch}
              placeholderTextColor="white"
              underlineColorAndroid="transparent"
            />
          </View>
          <View style={styles.inputContainer}>
            <TextInput
              style={styles.inputs}
              editable={false}
              selectTextOnFocus={false}
              placeholder={vemployee_id}
              placeholderTextColor="white"
              underlineColorAndroid="transparent"
            />
          </View>

          <TouchableOpacity
            style={styles.viewData}
            onPress={() => navigation.navigate('CerrarSesion')}>
            <Text style={styles.loginText}>CERRAR SESIÓN</Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    </View>
  );
};

Perfil.navigationOptions = {
  headerShown: false,
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',

    backgroundColor: '#333333',
  },
  logo: {
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    marginTop: 40,
    marginBottom: 20,
    marginLeft: 10,
  },
  head: {
    backgroundColor: '#000',
    // borderRadius:20
    borderTopLeftRadius: 0,
    borderTopRightRadius: 0,
    borderBottomLeftRadius: 20,
    borderBottomRightRadius: 20,
  },
  textIniciar: {
    marginTop: 30,
    color: 'white',
    width: 300,
    height: 45,
    marginBottom: 10,
    fontSize: 20,
    fontWeight: 'bold',
    textAlign: 'left',
    marginLeft: 25,
  },
  perfil: {
    marginLeft: 30,
    flexDirection: 'row',
    marginBottom: 40,
  },
  perfilimg: {
    width: 120,
    height: 120,
    resizeMode: 'contain',
    borderRadius: 70,
    marginBottom: 10,
  },
  perfiltxt: {
    flex: 0.6,
    color: 'white',
    justifyContent: 'center',
    alignItems: 'center',
    fontWeight: 'bold',
    fontSize: 20,
    marginLeft: 20,
    marginTop: 30,
  },
  buttonContainer: {
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  viewData: {
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 20,
    width: 260,
    borderRadius: 30,
    height: 45,
    backgroundColor: '#D32345',
  },
  logout: {
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 20,
    width: 260,
    borderRadius: 30,
    height: 45,
    backgroundColor: '#911830',
  },
  loginText: {
    color: 'white',
    fontWeight: 'bold',
  },
  inputContainer: {
    backgroundColor: '#5e5e5e',
    borderRadius: 30,
    width: 300,
    height: 45,
    marginBottom: 20,
    flexDirection: 'row',
    alignItems: 'center',
  },
  inputs: {
    height: 45,
    marginLeft: 16,
    color: 'white',
    flex: 1,
  },
});

export default Perfil;
