import React, {useState, useEffect, useCallback} from 'react';
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  Image,
  ScrollView,
  TouchableOpacity,
  SafeAreaView,
  PermissionsAndroid
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import {withNavigationFocus} from 'react-navigation';
import ForceApi from '../api/force';
import Header from '../components/Header';

const Panel = ({isFocused, navigation}) => {
  const [irregularities, setvirregularities] = useState();
  const [approved, setvapproved] = useState();
  const [sent, setvsent] = useState();
  const [rejected, setvrejected] = useState();

  async function Badge() {
    const vCreationUser = await AsyncStorage.getItem('id');
    const {data} = await ForceApi.post(`/GetTotalRequestController.php`, {
      vCreationUser,
    });
    setvirregularities(data.irregularities);
    setvapproved(data.request_approved);
    setvsent(data.sent);
    setvrejected(data.rejected);
  }

  const requestCameraPermission = async () => {
    try {
      const granted = await PermissionsAndroid.requestMultiple(
        permissions=[
          PermissionsAndroid.PERMISSIONS.CAMERA,
          PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE,
        ],
        {
          title: 'Exitus Force Camera Permission',
          message:
            'Exitus Force needs access to your camera ' +
            'so you can take important pictures.',
          // buttonNeutral: 'Ask Me Later',
          // buttonNegative: 'Cancel',
          buttonPositive: 'OK'
        }
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        console.log('You can use the camera');
      } else {
        // console.log('Camera permission denied');
      }
    } catch (err) {
      console.warn(err);
    }
  };

  useEffect(() => {
    Badge();
  }, [irregularities]);
  useEffect(() => {
    Badge();
  }, [irregularities]);
  useEffect(() => {
    Badge();
  }, [approved]);
  useEffect(() => {
    Badge();
  }, [sent]);
  useEffect(() => {
    Badge();
  }, [rejected]);

  useEffect(() => {
    if (isFocused) {
      // console.warn('Estoy en Panel');
      requestCameraPermission()
      Badge();
    }
  }, [isFocused]);

  return (
    <View style={styles.container}>
      <Header />

      <View style={styles.backContainer}>
        <View style={styles.textContainer}>
          <View>
            <Text style={styles.textIniciar}>Panel de Control</Text>
            <View style={styles.welcomeView}>
              <Text style={styles.textInit}>
                Bienvenido a Exitus Force, le presentamos algunos reportes sobre
                su actividad como promotor de Exitus Credit.
              </Text>
            </View>
          </View>
        </View>
      </View>

      <ScrollView>
        <View style={styles.buttonContainer}>
          <TouchableOpacity
            style={styles.linkContainer}
            onPress={() => navigation.navigate('Sol1')}>
            <View style={approved == 0 ? styles.Badge2 : styles.Badge}>
              <Text style={styles.BadgeTXT}>
                {approved >= 100 ? '99+' : approved}
              </Text>
            </View>
            <View style={styles.buttonTextContainer}>
              <Text style={styles.textPanel}>Consultas Aprobadas</Text>
            </View>
          </TouchableOpacity>

          <TouchableOpacity
            style={styles.linkContainer}
            onPress={() => navigation.navigate('Sol2')}>
            <View style={sent == 0 ? styles.Badge2 : styles.Badge}>
              <Text style={styles.BadgeTXT}>{sent >= 100 ? '99+' : sent}</Text>
            </View>
            <View style={styles.buttonTextContainer}>
              <Text style={styles.textPanel}>Enviadas</Text>
            </View>
          </TouchableOpacity>

          <TouchableOpacity
            style={styles.linkContainer}
            onPress={() => navigation.navigate('Sol3')}>
            <View style={irregularities == 0 ? styles.Badge2 : styles.Badge}>
              <Text style={styles.BadgeTXT}>
                {irregularities >= 100 ? '99+' : irregularities}
              </Text>
            </View>
            <View style={styles.buttonTextContainer}>
              <Text style={styles.textPanel}>Irregularidades</Text>
            </View>
          </TouchableOpacity>

          <TouchableOpacity
            style={styles.linkContainer}
            onPress={() => navigation.navigate('Sol4')}>
            <View style={rejected == 0 ? styles.Badge2 : styles.Badge}>
              <Text style={styles.BadgeTXT}>
                {rejected >= 100 ? '99+' : rejected}
              </Text>
            </View>
            <View style={styles.buttonTextContainer}>
              <Text style={styles.textPanel}>Rechazadas</Text>
            </View>
          </TouchableOpacity>
        </View>
      </ScrollView>
    </View>
  );
};

Panel.navigationOptions = {
  headerShown: false,
};

const styles = StyleSheet.create({
  buttonTextContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginRight: 30,
    // backgroundColor: 'yellow'
  },
  welcomeView: {
    marginHorizontal: 40,
  },
  Badge: {
    borderRadius: 50,
    width: 34,
    height: 34,
    padding: 7,
    backgroundColor: '#e02626',
    marginBottom: 26,
    justifyContent: 'center',
    alignItems: 'center',
  },
  Badge2: {
    borderRadius: 50,
    width: 34,
    height: 34,
    padding: 7,
    backgroundColor: '#918c8c',
    marginBottom: 26,
    justifyContent: 'center',
    alignItems: 'center',
  },
  BadgeTXT: {
    color: 'white',
    fontSize: 11,
    fontWeight: '500',
  },
  BadgeTXT2: {
    color: 'white',
    fontSize: 11,
    fontWeight: '500',
  },
  container: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: '#333333',
  },
  logo: {
    height: 70,
    width: 110,
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    marginTop: 40,
    marginBottom: 20,
    marginLeft: 10,
  },
  backContainer: {
    flexDirection: 'column',
    marginBottom: 20,
  },
  head: {
    backgroundColor: '#000',
    // borderRadius:20,
    borderTopLeftRadius: 0,
    borderTopRightRadius: 0,
    borderBottomLeftRadius: 20,
    borderBottomRightRadius: 20,
  },
  back: {
    marginTop: 2,
    flexDirection: 'column',
    color: 'white',
  },
  textContainer: {
    flexDirection: 'row',
    marginTop: 30,
    marginBottom: 10,
    marginLeft: 10,
  },
  textIniciar: {
    color: 'white',
    width: 300,
    fontSize: 20,
    fontWeight: 'bold',
    textAlign: 'left',
    marginLeft: 25,
  },
  textInit: {
    color: '#b3b4b5',
    textAlign: 'justify',
    marginTop: 40,
    marginBottom: 25,
  },
  textInit2: {
    fontSize: 16,
    color: '#b3b4b5',
    textAlign: 'left',
    marginLeft: 25,
    fontWeight: 'bold',
  },
  buttonContainer2: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  logout: {
    marginTop: 20,
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 20,
    width: 142,
    borderRadius: 30,
    height: 45,
    backgroundColor: '#911830',
  },
  buttonContainer: {
    marginTop: 13,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  viewData: {
    marginTop: 20,
    marginLeft: 20,
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 20,
    width: 142,
    borderRadius: 30,
    height: 45,
    backgroundColor: '#D32345',
  },
  loginText: {
    color: 'white',
    fontWeight: 'bold',
  },
  inputContainer: {
    backgroundColor: '#5e5e5e',
    borderRadius: 30,
    width: 250,
    height: 45,
    marginBottom: 20,
    flexDirection: 'row',
    alignItems: 'center',
  },
  linkContainer: {
    backgroundColor: '#5e5e5e',
    borderRadius: 30,
    width: 250,
    height: 45,
    marginBottom: 20,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  SinputContainer: {
    backgroundColor: '#5e5e5e',
    borderRadius: 30,
    width: 145,
    height: 45,
    marginBottom: 20,
    flexDirection: 'row',
    alignItems: 'center',
    display: 'flex',
    justifyContent: 'center',
  },
  SinputContainer2: {
    backgroundColor: '#5e5e5e',
    borderRadius: 30,
    width: 145,
    height: 45,
    marginBottom: 20,
    flexDirection: 'row',
    alignItems: 'center',
    marginLeft: 10,
  },
  inputs: {
    height: 45,
    marginLeft: 16,
    color: 'white',
    flex: 1,
  },
  textPanel: {
    color: 'white',
    textAlign: 'center',
  },
});

// export default Panel;
export default withNavigationFocus(Panel);
/*<Text style={styles.textInit2}>¡Hola, Carlos!</Text>*/
