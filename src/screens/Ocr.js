import React, {useState, useContext, useEffect} from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  YellowBox,
  Image,
} from 'react-native';
import { Icon, Button, Tooltip } from 'react-native-elements';
// import * as Permissions from 'expo-permissions'
// import * as placesActions from '../components/places-actions';

// import UploadingOverlay from '../components/UploadingOverlay'
import Ionicons from 'react-native-vector-icons/Ionicons';
import OcrImg from '../components/OcrImg';
import {Context} from '../context/AuthContext';
import Header from '../components/Header';

const Ocr = ({navigation}) => {
  const {state, guardarOcr} = useContext(Context);
  const [errorContext, setError] = useState(false);

  useEffect(() => {
    setError(state.error);
  }, [state]);

  return (
    <View style={styles.container}>
      <Header />
      <View style={styles.backContainer}>
        <View style={styles.textContainer}>
          <TouchableOpacity onPress={() => navigation.goBack()}>
            <Ionicons style={styles.back} name="ios-arrow-back" size={27} />
          </TouchableOpacity>
          <View>
            <Text style={styles.textIniciar}>Nueva Solicitud</Text>
            <Text style={styles.textInit}>Carga de documentos</Text>
          </View>
        </View>
        <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', marginRight: 40 }}>
          <Text style={styles.textInit}>Identificación Oficial </Text>  
          <Tooltip 
            width={300}
            height={100}
            backgroundColor='#333'
            containerStyle={{
              marginRight: 20
            }}
            popover={<Text style={{ color: '#fff' }}>Tip: Para un mejor resultado gire el teléfono celular de modo horizontal cuando la cámara se abra.</Text>}>
            <Icon name='info' type='feather' color='#b3b4b5' size={20}/>
          </Tooltip>
        </View>
      </View>

      <OcrImg state={state} onImageTaken={guardarOcr} onSubmit={guardarOcr} />
    </View>
  );
};

Ocr.navigationOptions = {
  headerShown: false,
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: '#333333',
  },
  logo: {
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    marginTop: 40,
    marginBottom: 20,
    marginLeft: 10,
  },
  head: {
    backgroundColor: '#000',
    // borderRadius:20
    borderTopLeftRadius: 0,
    borderTopRightRadius: 0,
    borderBottomLeftRadius: 20,
    borderBottomRightRadius: 20,
  },
  backContainer: {
    flexDirection: 'column',
    marginBottom: 20,
  },
  back: {
    marginTop: 2,
    flexDirection: 'column',
    color: 'white',
  },
  textContainer: {
    flexDirection: 'row',
    marginTop: 30,
    marginBottom: 10,
    marginLeft: 10,
  },
  textIniciar: {
    color: 'white',
    width: 300,
    fontSize: 20,
    fontWeight: 'bold',
    textAlign: 'left',
    marginLeft: 25,
  },
  textInit: {
    color: '#b3b4b5',
    textAlign: 'left',
    marginLeft: 25,
  },
});

export default Ocr;
