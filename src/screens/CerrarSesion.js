import React, {useContext} from 'react';
import {StyleSheet, Text, View, TouchableOpacity} from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import {Context as AuthContext} from '../context/AuthContext';

const CerrarSesion = ({navigation}) => {
  const {logout} = useContext(AuthContext);

  return (
    <View style={styles.container}>
      <View>
        <TouchableOpacity onPress={() => navigation.goBack()}>
          <Ionicons style={styles.back} name="ios-arrow-back" size={27} />
        </TouchableOpacity>
      </View>

      <View style={styles.container2}>
        <View style={styles.textContainer}>
          <Text style={styles.textIniciar}>Cerrar sesión</Text>
          <Text style={styles.textInit}>
            ¿Está seguro de que desea salir de su sesión?
          </Text>
        </View>

        <View style={styles.buttonContainer}>
          <TouchableOpacity
            style={styles.viewData}
            onPress={() => navigation.navigate('MiPerfil')}>
            <Text style={styles.loginText}>CANCELAR</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.logout} onPress={logout}>
            <Text style={styles.loginText}>ACEPTAR</Text>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
};
const resizeMode = 'center';

CerrarSesion.navigationOptions = {
  headerShown: false,
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#333333',
  },
  container2: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  textContainer: {
    flexDirection: 'column',
    marginTop: 30,
    marginBottom: 10,
  },
  textIniciar: {
    color: 'white',
    width: 300,
    fontSize: 25,
    fontWeight: 'bold',
    textAlign: 'center',
  },
  back: {
    marginTop: 30,
    marginLeft: 20,
    flexDirection: 'column',
    color: 'white',
  },
  textInit: {
    marginTop: 30,
    color: '#b3b4b5',
    textAlign: 'center',
  },
  buttonContainer: {
    marginTop: 100,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  viewData: {
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 20,
    width: 260,
    borderRadius: 30,
    height: 45,
    backgroundColor: '#D32345',
  },
  logout: {
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 10,
    width: 260,
    borderRadius: 30,
    height: 45,
    backgroundColor: '#911830',
  },
  loginText: {
    color: 'white',
    fontWeight: 'bold',
  },
});

export default CerrarSesion;
