import React, {Component, Input, useContext, useState} from 'react';
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableOpacity,
  Image,
  ScrollView,
  FlatList,
} from 'react-native';
import {SolProvider} from '../context/AuthContext';
import Ionicons from 'react-native-vector-icons/Ionicons';
import {navigate} from '../navigationRef';
import Sol1Form from '../components/Sol1Form';
import {Context} from '../context/AuthContext';
import IrregularForm from '../components/IrregularForm';
import Header from '../components/Header';

const Solicitudes1 = ({navigation}) => {
  const {state, guardarIrre} = useContext(Context);
  console.log('Irregular: ', state);

  return (
    <View style={styles.container}>
      <Header />
      <View style={styles.backContainer}>
        <View style={styles.textContainer}>
          <TouchableOpacity onPress={() => navigation.goBack()}>
            <Ionicons style={styles.back} name="ios-arrow-back" size={27} />
          </TouchableOpacity>
          <View>
            <Text style={styles.textIniciar}>Nueva Solicitud</Text>
          </View>
        </View>
      </View>
        <IrregularForm
          state={state}
          onSubmit={guardarIrre}
          navigation={navigation}
        />
    </View>
  );
};

Solicitudes1.navigationOptions = {
  headerShown: false,
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: '#333333',
  },
  backContainer: {
    flexDirection: 'row',
    marginBottom: 10,
  },
  back: {
    marginTop: 2,
    color: 'white',
  },
  textContainer: {
    flexDirection: 'row',
    marginTop: 30,
    marginBottom: 10,
    marginLeft: 10,
  },
  textIniciar: {
    color: 'white',
    width: 300,
    fontSize: 20,
    fontWeight: 'bold',
    textAlign: 'left',
    marginLeft: 25,
  }, 
});

export default Solicitudes1;
