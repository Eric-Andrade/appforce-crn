import React, {useContext, useEffect} from 'react';
import {StyleSheet, Text, View, Image, TouchableOpacity} from 'react-native';
import {Context} from '../context/AuthContext';

const SolProcesada = ({navigation}) => {
  return (
    <TouchableOpacity
      style={styles.container}
      onPress={() => navigation.navigate('Seleccion')}>
      <Image
        style={styles.manita}
        source={require('../../assets/Thumbs-Up.png')}
      />

      <Text style={styles.textIniciar}>Solicitud enviada</Text>
      <Text style={styles.Pin}>Podrás revisar del proceso dentro de</Text>
      <Text style={styles.textIniciar}>Solicitudes Enviadas.</Text>
    </TouchableOpacity>
  );
};

SolProcesada.navigationOptions = {
  headerShown: false,
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#333333',
  },
  textIniciar: {
    color: 'white',
    fontSize: 20,
    fontWeight: 'bold',
    marginBottom: 20,
  },
  Pin: {
    color: '#b3b4b5',
  },
  manita: {
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 50,
  },
  perfilimg: {
    width: 120,
    height: 120,
    resizeMode: 'contain',
    borderRadius: 70,
    marginBottom: 50,
    backgroundColor: '#5e5e5e',
  },
});

export default SolProcesada;
