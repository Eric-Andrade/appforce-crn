import React, {useState, useContext} from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  YellowBox,
  ScrollView,
  Image,
} from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import MultiImagePicker from '../components/MultiImagePicker';
import {Context} from '../context/AuthContext';
import Spinner from 'react-native-loading-spinner-overlay';
import Header from '../components/Header';

const nsCargaDocumentosD8 = ({navigation}) => {
  const {state, guardarD8} = useContext(Context);
  console.log('nsCargaDocumentosD8 state: ', state);

  return (
    <View style={styles.container}>
      <Header />
      <View style={styles.backContainer}>
        <View style={styles.textContainer}>
          <TouchableOpacity onPress={() => navigation.goBack()}>
            <Ionicons style={styles.back} name="ios-arrow-back" size={27} />
          </TouchableOpacity>
          <View>
            <Text style={styles.textIniciar}>Nueva Solicitud</Text>
            <Text style={styles.textInit}>Carga de documentos</Text>
          </View>
        </View>
        <Text style={styles.textInit}>Comprobante de clabe interbancaria</Text>
      </View>
      <MultiImagePicker
        state={state}
        onImageTaken={guardarD8}
        onSubmit={guardarD8}
      />
    </View>
  );
};

nsCargaDocumentosD8.navigationOptions = {
  headerShown: false,
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: '#333333',
  },
  logo: {
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    marginTop: 40,
    marginBottom: 20,
    marginLeft: 10,
  },
  head: {
    backgroundColor: '#000',
    // borderRadius:20
    borderTopLeftRadius: 0,
    borderTopRightRadius: 0,
    borderBottomLeftRadius: 20,
    borderBottomRightRadius: 20,
  },
  backContainer: {
    flexDirection: 'column',
    marginBottom: 20,
  },
  back: {
    marginTop: 2,
    flexDirection: 'column',
    color: 'white',
  },
  textContainer: {
    flexDirection: 'row',
    marginTop: 30,
    marginBottom: 10,
    marginLeft: 10,
  },
  textIniciar: {
    color: 'white',
    width: 300,
    fontSize: 20,
    fontWeight: 'bold',
    textAlign: 'left',
    marginLeft: 25,
  },
  textInit: {
    color: '#b3b4b5',
    textAlign: 'left',
    marginLeft: 25,
  },
});

export default nsCargaDocumentosD8;
