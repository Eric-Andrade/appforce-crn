import React, {Component, Input, useContext, useState} from 'react';
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableOpacity,
  Image,
  ScrollView,
  FlatList,
} from 'react-native';
import Sol4Form from '../components/Sol4Form';
import Header from '../components/Header';

const Solicitudes4 = ({navigation}) => {
  return (
    <View style={styles.container}>
      <Header />
      <View style={styles.backContainer}>
        <View style={styles.textContainer}>
          <View>
            <Text style={styles.textIniciar}>Solicitudes Rechazadas</Text>
          </View>
        </View>
        <View style={styles.plazos}>
          <TouchableOpacity
            style={styles.plazo1}
            onPress={() => navigation.navigate('Solicitudes1')}>
            <Text style={styles.BTNText}>Consultas Aprobadas</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.plazo2}
            onPress={() => navigation.navigate('Solicitudes2')}>
            <Text style={styles.BTNText}>Enviadas</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.plazo3}
            onPress={() => navigation.navigate('Solicitudes3')}>
            <Text style={styles.BTNText}>Irregularidades</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.plazo4}>
            <Text style={styles.BTNText1}>Rechazadas</Text>
          </TouchableOpacity>
        </View>
      </View>

      <View style={styles.container}>
        <Sol4Form />
      </View>
    </View>
  );
};

Solicitudes4.navigationOptions = {
  headerShown: false,
};

const styles = StyleSheet.create({
  BTNText1: {
    color: '#D32345',
    fontWeight: 'bold',
  },
  BTNText: {
    color: 'white',
    fontWeight: 'bold',
  },
  plazos: {
    flexDirection: 'row',
    marginBottom: 40,
    justifyContent: 'center',
    alignItems: 'center',
  },
  plazo1: {
    borderWidth: 0,
    borderColor: 'transparent',
    color: '#b3b4b5',
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1 / 5,
    marginHorizontal: 2
  },
  plazo2: {
    color: '#b3b4b5',
    flex: 1 / 5,
    justifyContent: 'center',
    alignItems: 'center',
    marginHorizontal: 2
  },
  plazo3: {
    color: '#b3b4b5',
    flex: 1 / 4,
    justifyContent: 'center',
    alignItems: 'center',
    marginHorizontal: 2
  },
  plazo4: {
    color: '#b3b4b5',
    flex: 1 / 5,
    justifyContent: 'center',
    alignItems: 'center',
    marginHorizontal: 2
  },
  btncontainer: {
    height: 50,
    backgroundColor: '#333333',
  },
  container: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: '#333333',
  },
  logo: {
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    marginTop: 40,
    marginBottom: 20,
    marginLeft: 10,
  },
  head: {
    backgroundColor: '#000',
    // borderRadius:20
    borderTopLeftRadius: 0,
    borderTopRightRadius: 0,
    borderBottomLeftRadius: 20,
    borderBottomRightRadius: 20,
  },
  textIniciar: {
    marginTop: 30,
    color: 'white',
    width: 300,
    height: 45,
    marginBottom: 5,
    fontSize: 20,
    fontWeight: 'bold',
    textAlign: 'left',
    marginLeft: 20,
  },
  buttonContainer: {
    flex: 1,
    height: 35,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 20,
    borderRadius: 30,
    backgroundColor: 'transparent',
    marginVertical: 10,
    marginHorizontal: 20,
    flexBasis: '46%',
  },
  inputIcon: {
    marginRight: 20,
    justifyContent: 'center',
  },
  loginButton: {
    backgroundColor: '#D32345',
  },
  loginText: {
    color: 'white',
    fontWeight: 'bold',
  },
  Lcontainer: {
    backgroundColor: '#333333',
  },
  card: {
    backgroundColor: '#3d3d3d',
    flexBasis: '46%',
    flexDirection: 'column',
    height: 120,
    borderRadius: 8,
  },
  solicitud: {
    fontSize: 18,
    textAlign: 'left',
    color: 'white',
    fontWeight: 'bold',
    marginLeft: 10,
    marginTop: 5,
  },
  solicitudview: {
    backgroundColor: '#696969',
    color: 'white',
    height: 40,
    fontSize: 20,
    borderRadius: 10,
  },
  ocointainer: {
    marginTop: 30,
    marginBottom: 20,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  pickcontainer: {
    flexDirection: 'row',
    borderRadius: 10,
    borderWidth: 1,
    borderColor: '#696969',
    backgroundColor: '#696969',
    flexBasis: '90%',
  },
  pick: {
    backgroundColor: '#696969',
    color: 'white',
    height: 40,
    fontSize: 20,
    borderRadius: 50,
    flexBasis: '90%',
    marginLeft: 15,
  },
  tarjeta: {
    shadowOpacity: 0.37,
    shadowRadius: 7.49,
    elevation: 12,
    marginVertical: 10,
    marginHorizontal: 20,
    flexBasis: '46%',
    flexDirection: 'column',
    backgroundColor: '#3d3d3d',
    borderRadius: 10,
  },
  clave: {
    fontSize: 14,
    textAlign: 'left',
    color: '#9e9e9e',
    marginTop: 3,
    marginBottom: 0,
    marginLeft: 10,
  },

  /************ modals ************/
  popup: {
    backgroundColor: '#3d3d3d',
    marginTop: 150,
    marginHorizontal: 20,
    borderRadius: 7,
  },
  popupOverlay: {
    backgroundColor: '#00000057',
    flex: 1,
    marginTop: 30,
  },
  popupContent: {
    textAlign: 'left',
    margin: 5,
    height: 180,
    alignItems: 'center',
  },
  mod: {
    fontSize: 20,
    fontWeight: 'bold',
    textAlign: 'left',
    color: '#9e9e9e',
    marginTop: 3,
    marginBottom: 0,
    marginLeft: 10,
  },
  popupHeader: {
    marginBottom: 45,
  },
  popupButtons: {
    marginTop: 15,
    flexDirection: 'row',
    borderTopWidth: 1,
    borderColor: '#eee',
    justifyContent: 'center',
  },
  popupButton: {
    flex: 1,
    marginVertical: 16,
  },
  btnClose: {
    height: 20,
    backgroundColor: '#D32345',
    padding: 20,
  },
  modalInfo: {
    textAlign: 'left',
  },
  txtClose: {
    justifyContent: 'center',
    color: 'white',
    textAlign: 'center',
  },
});

export default Solicitudes4;
