import React, {useContext, useEffect} from 'react';
import {StyleSheet, Text, View, Image, TouchableOpacity} from 'react-native';

const SolRechazada = ({navigation}) => {
  return (
    <TouchableOpacity
      style={styles.container}
      onPress={() => navigation.navigate('Seleccion')}>
      <Image
        style={styles.perfilimg}
        source={require('../../assets/Rechazada.png')}
      />
      <View style={styles.contenedor}>
        <Text style={styles.sorry}>
          Lo sentimos
        </Text>
        <Text style={styles.textIniciar}>
        En este momento no podemos aceptar su solicitud debido a que no cumple con las políticas.
        </Text>
      </View>
    </TouchableOpacity>
  );
};

SolRechazada.navigationOptions = {
  headerShown: false,
};

const styles = StyleSheet.create({
  contenedor: {
    marginLeft: 50,
    marginRight: 50,
  },
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#333333',
  },
  sorry: {
    color: 'white',
    fontSize: 22,
    fontWeight: 'bold',
    textAlign: 'center'
  },
  textIniciar: {
    color: 'white',
    fontSize: 16,
    fontWeight: '900',
    textAlign: 'justify',
  },
  Pin: {
    color: '#b3b4b5',
  },
  perfilimg: {
    width: 120,
    height: 120,
    resizeMode: 'contain',
    borderRadius: 70,
    marginBottom: 10,
  },
});

export default SolRechazada;
