import React, {useState, useContext} from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  YellowBox,
  Image,
  NativeModules,
  Platform,
} from 'react-native';
import { Icon, Button  } from 'react-native-elements';
import ImagePicker from '../components/ImagePicker';
import Ionicons from 'react-native-vector-icons/Ionicons';
import {Context} from '../context/AuthContext';
import Header from '../components/Header';
// import MitekFacialID from '../components/MitekFacialID';

const Selfie = ({navigation}) => {
  const {state, guardarD0} = useContext(Context);

  return (
    <View style={styles.container}>
      <Header />
      <View style={styles.backContainer}>
        <View style={styles.textContainer}>
          <TouchableOpacity onPress={() => navigation.goBack()}>
            <Ionicons style={styles.back} name="ios-arrow-back" size={27} />
          </TouchableOpacity>
          <View>
            <Text style={styles.textIniciar}>Nueva Solicitud</Text>
            <Text style={styles.textInit}>Carga de documentos</Text>
          </View>
        </View> 
        <Text style={styles.textInit}>Selfie</Text>
      </View>

      {/* <MitekFacialID /> */}
      <ImagePicker
        state={state}
        onImageTaken={guardarD0}
        onSubmit={guardarD0}
      />

    </View>
  );
};

Selfie.navigationOptions = {
  headerShown: false,
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: '#333333',
  },
  logo: {
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    marginTop: 40,
    marginBottom: 20,
    marginLeft: 10,
  },
  head: {
    backgroundColor: '#000',
    // borderRadius:20
    borderTopLeftRadius: 0,
    borderTopRightRadius: 0,
    borderBottomLeftRadius: 20,
    borderBottomRightRadius: 20,
  },
  backContainer: {
    flexDirection: 'column',
    marginBottom: 20,
  },
  back: {
    marginTop: 2,
    flexDirection: 'column',
    color: 'white',
  },
  textContainer: {
    flexDirection: 'row',
    marginTop: 30,
    marginBottom: 10,
    marginLeft: 10,
  },
  textIniciar: {
    color: 'white',
    width: 300,
    fontSize: 20,
    fontWeight: 'bold',
    textAlign: 'left',
    marginLeft: 25,
  },
  textInit: {
    color: '#b3b4b5',
    textAlign: 'left',
    marginLeft: 25,
  },
  BTNLoad: {
    marginLeft: '28%',
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 20,
    width: 200,
    borderRadius: 30,
    height: 45,
    backgroundColor: '#D32345',
  },
  BTNText: {
    color: 'white',
    fontWeight: 'bold',
  },
});

export default Selfie;
