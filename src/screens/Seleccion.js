import React, {useState} from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  TextInput,
} from 'react-native';

const Seleccion = ({navigation}) => {
  return (
    <View style={styles.container}>
      <View style={styles.container2}>
        <View style={styles.textContainer}>
          <Text style={styles.textIniciar}>
            Selección de Captura de Solicitud
          </Text>
          <Text style={styles.textInit}>
            Seleccione la forma en que capturará los datos
          </Text>
        </View>
        <View style={styles.buttonContainer}>
          <TouchableOpacity
            style={styles.viewData}
            onPress={() => navigation.navigate('Ocr')}>
            <Text style={styles.loginText}>Automática</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.logout}
            onPress={() => navigation.navigate('SolDatosPersonales')}>
            <Text style={styles.loginText}>Tradicional</Text>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
};
const resizeMode = 'center';

Seleccion.navigationOptions = {
  headerShown: false,
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#333333',
  },
  container2: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  textContainer: {
    flexDirection: 'column',
    marginTop: 30,
    marginBottom: 10,
  },
  textIniciar: {
    color: 'white',
    width: 300,
    fontSize: 25,
    fontWeight: 'bold',
    textAlign: 'center',
  },
  back: {
    marginTop: 30,
    marginLeft: 20,
    flexDirection: 'column',
    color: 'white',
  },
  textInit: {
    marginTop: 30,
    color: '#b3b4b5',
    textAlign: 'center',
  },
  buttonContainer: {
    marginTop: 100,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  viewData: {
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 20,
    width: 260,
    borderRadius: 30,
    height: 45,
    backgroundColor: '#D32345',
  },
  logout: {
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 10,
    width: 260,
    borderRadius: 30,
    height: 45,
    backgroundColor: '#911830',
  },
  loginText: {
    color: 'white',
    fontWeight: 'bold',
  },
});

export default Seleccion;

//newSliderValue => (newSliderValue <= 100 && setSliderValue(parseInt(newSliderValue)))}
