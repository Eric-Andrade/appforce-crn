import React, {useState, useEffect} from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  TextInput,
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import ForceApi from '../api/force';
import {navigate} from '../navigationRef';

const SolFinalizada = ({navigation}) => {
  const [helper, sethelper] = useState(true);
  const [vKindSignature, setvKindSignaturer] = useState();
  const [vzellId, setvzellId] = useState();

  useEffect(() => {
    console.log('aquiarriba');
    Zell();
  }, []);

  useEffect(() => {
    FirmaE();
  }, []);

  async function Zell() {
    const vSolicitudeId = await AsyncStorage.getItem('SolicitudeId');
    //const vSolicitudeId = 515;
    const {data} = await ForceApi.post(`/SendZellController.php`, {
      vSolicitudeId,
    });
    console.log('aquizell');
    console.log(data);
    setvzellId(data.zellId);
  }

  async function FirmaE() {
    console.log('entro');
    const vKindSignature = 0;

    const vSolicitudeId = await AsyncStorage.getItem('SolicitudeId');
    const {data} = await ForceApi.post(`/UpdateKindSignatureController.php`, {
      vSolicitudeId,
      vKindSignature,
    });
    console.log(data);
    //sethelper(data.error);
  }

  async function FirmaA() {
    const vKindSignature = 1;
    const vSolicitudeId = await AsyncStorage.getItem('SolicitudeId');
    const {data} = await ForceApi.post(`/UpdateKindSignatureController.php`, {
      vSolicitudeId,
      vKindSignature,
    });
    console.log(data);
    //sethelper(data.error);
  }
  return (
    <View style={styles.container}>
      <View style={styles.container2}>
        <View style={styles.textContainer}>
          <Text style={styles.textIniciar}>Solicitud finalizada</Text>
          <Text style={styles.textInit}>
            La solicitud ha sido enviada satisfactoriamente.
          </Text>
          <Text style={styles.textInit}>Su ID de Zell es {vzellId}.</Text>
        </View>

      </View>
    </View>
  );
};

const resizeMode = 'center';

SolFinalizada.navigationOptions = {
  headerShown: false,
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#333333',
  },
  container2: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  textContainer: {
    flexDirection: 'column',
    marginTop: 30,
    marginBottom: 10,
  },
  textIniciar: {
    color: 'white',
    width: 300,
    fontSize: 25,
    fontWeight: 'bold',
    textAlign: 'center',
  },
  back: {
    marginTop: 30,
    marginLeft: 20,
    flexDirection: 'column',
    color: 'white',
  },
  textInit: {
    marginTop: 30,
    color: '#b3b4b5',
    textAlign: 'center',
  },
  buttonContainer: {
    marginTop: 100,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  viewData: {
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 20,
    width: 260,
    borderRadius: 30,
    height: 45,
    backgroundColor: '#D32345',
  },
  logout: {
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 10,
    width: 260,
    borderRadius: 30,
    height: 45,
    backgroundColor: '#911830',
  },
  loginText: {
    color: 'white',
    fontWeight: 'bold',
  },
});

export default SolFinalizada;

//newSliderValue => (newSliderValue <= 100 && setSliderValue(parseInt(newSliderValue)))}
/*
<TouchableOpacity style={ styles.logout} onPress={() => FirmaA()}>
                <Text style={styles.loginText}>FENVIAR PARA FIRMA AUTÓGRAFA</Text>
              </TouchableOpacity>
              <View style={styles.buttonContainer}>
              <TouchableOpacity style={styles.viewData} onPress={() => FirmaE()}>
                <Text style={styles.loginText}>ENVIAR PARA FIRMA ELECTRÓNICA</Text>
              </TouchableOpacity>
            </View>*/