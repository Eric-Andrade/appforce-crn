import React, {useContext} from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  TextInput,
  ScrollView,
  Alert,
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import {Context} from '../context/AuthContext';
import Ionicons from 'react-native-vector-icons/Ionicons';
import CalculadoraFormCA from '../components/CalculadoraFormCA';
import Header from '../components/Header';

const Calculadora = ({navigation}) => {
  const {state, guardarCalcOCR} = useContext(Context);
  // const vSolicitudeId = navigation.getParam('_id');
  return (
    <View style={styles.container}>
      <Header />
      <View style={styles.backContainer}>
        <View style={styles.textContainer}>
          <TouchableOpacity 
          onPress={() => navigation.goBack()}>
           {/* onPress={() => {navigation.navigate('Solicitudes1'); AsyncStorage.removeItem('SolicitudeId'); console.warn('cleaned SolicitudeId')}}> */}
            <Ionicons style={styles.back} name="ios-arrow-back" size={27} />
          </TouchableOpacity>
          <View>
            <Text style={styles.textIniciar}>Nueva Solicitud</Text>
            <Text style={styles.textInit}>Respuesta del buró de crédito</Text>
          </View>
        </View>
      </View>
      <CalculadoraFormCA
        state={state}
        errorMessage={state.errorMessage}
        onSubmit={guardarCalcOCR}
        navigation={navigation}
      />
    </View>
  );
};

Calculadora.navigationOptions = {
  headerShown: false,
};

const styles = StyleSheet.create({
  top: {
    flexDirection: 'row',
  },
  colorgris: {
    color: '#b3b4b5',
    marginLeft: 15,
  },

  plazos: {
    flexDirection: 'row',
    marginBottom: 40,
  },
  plazo1: {
    color: '#b3b4b5',
    marginLeft: 30,
    flex: 1 / 3,
  },
  plazo2: {
    color: '#b3b4b5',
    flex: 1 / 3,
  },
  plazo3: {
    color: '#b3b4b5',
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
    flex: 1 / 3,
  },
  container: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: '#333333',
  },
  logo: {
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    marginTop: 40,
    marginBottom: 20,
    marginLeft: 10,
  },
  backContainer: {
    flexDirection: 'column',
    marginBottom: 20,
  },
  head: {
    backgroundColor: '#000',
    // borderRadius:20
    borderTopLeftRadius: 0,
    borderTopRightRadius: 0,
    borderBottomLeftRadius: 20,
    borderBottomRightRadius: 20,
  },
  back: {
    marginTop: 2,
    flexDirection: 'column',
    color: 'white',
  },
  textContainer: {
    flexDirection: 'row',
    marginTop: 30,
    marginBottom: 10,
    marginLeft: 10,
  },
  textIniciar: {
    color: 'white',
    width: 300,
    fontSize: 20,
    fontWeight: 'bold',
    textAlign: 'left',
    marginLeft: 25,
  },
  textInit: {
    color: '#b3b4b5',
    textAlign: 'left',
    marginLeft: 25,
  },

  centrado: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#333333',
  },
  Pin: {
    color: '#b3b4b5',
    marginLeft: 60,
    marginRight: 60,
  },
  inputContainer: {
    backgroundColor: '#5e5e5e',
    borderRadius: 30,
    width: 100,
    height: 45,
    marginTop: 30,
    marginBottom: 30,
    flexDirection: 'row',
    alignItems: 'center',
    marginLeft: 20,
  },
  inputs: {
    height: 45,
    marginLeft: 16,
    color: 'white',
    flex: 1,
  },
  buttonContainer: {
    height: 45,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 60,
    width: 300,
    borderRadius: 30,
    backgroundColor: 'transparent',
  },
  loginButton: {
    marginTop: 10,
    backgroundColor: '#D32345',
  },
  BtnText: {
    color: 'white',
    fontWeight: 'bold',
  },

  tarjeta: {
    width: 350,
    shadowOpacity: 0.37,
    shadowRadius: 7.49,
    elevation: 12,
    marginVertical: 10,
    marginHorizontal: 20,
    flexBasis: '57%',
    flexDirection: 'column',
    backgroundColor: '#3d3d3d',
    borderRadius: 10,
  },
  CreditText: {
    marginTop: 10,
    marginBottom: 20,
    marginLeft: 15,
    color: 'white',
    fontWeight: 'bold',
  },
  slideText: {
    color: '#b3b4b5',
    marginBottom: 5,
    marginLeft: 15,
  },
  PaymentText: {
    color: 'white',
    fontWeight: 'bold',
    marginBottom: 20,
    marginTop: 10,
    marginLeft: 15,
  },
});

export default Calculadora;
