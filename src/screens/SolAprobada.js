import React, {useContext, useEffect, useState} from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  TextInput,
} from 'react-native';
import {Context} from '../context/AuthContext';

const SolAprovada = ({navigation}) => {
  const {state, PinCode} = useContext(Context);
  const [vCode, setvCode] = useState('');
  useEffect(() => {
    PinCode({vCode});
  }, []);

  return (
    <View style={styles.container}>
      <View>
        <Text style={styles.textIniciar}>Verificación de PIN</Text>
      </View>
      <View>
        <Text style={styles.Pin}>
          Ingrese el código que hemos enviado al número registrado.
        </Text>
      </View>
      <View style={styles.inputContainer}>
        <TextInput
          style={styles.inputs}
          placeholder="Código"
          placeholderTextColor="white"
          underlineColorAndroid="transparent"
          onChangeText={newvCode => setvCode(newvCode)}
          value={vCode}
          autoCorrect={false}
          autoCapitalize="characters"
        />
      </View>
      <TouchableOpacity
        style={[styles.buttonContainer, styles.loginButton]}
        onPress={() => navigation.navigate('PinVal')}>
        <Text style={styles.loginText}>ENVIAR</Text>
      </TouchableOpacity>
      <View>
        <Text style={styles.Pin}>
          ¿No se ha recibido el código de verificación?
        </Text>
        <Text style={styles.Pin}>Reenviar</Text>
      </View>
    </View>
  );
};

SolAprovada.navigationOptions = {
  headerShown: false,
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#333333',
  },
  textIniciar: {
    color: 'white',
    fontSize: 20,
    fontWeight: 'bold',
    marginBottom: 20,
  },
  Pin: {
    color: '#b3b4b5',
    marginLeft: 60,
    marginRight: 60,
  },
  inputContainer: {
    backgroundColor: '#5e5e5e',
    borderRadius: 30,
    width: 300,
    height: 45,
    marginTop: 80,
    marginBottom: 60,
    flexDirection: 'row',
    alignItems: 'center',
  },
  inputs: {
    height: 45,
    marginLeft: 16,
    color: 'white',
    flex: 1,
  },
  buttonContainer: {
    height: 45,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 60,
    width: 300,
    borderRadius: 30,
    backgroundColor: 'transparent',
  },
  loginButton: {
    marginTop: 10,
    backgroundColor: '#D32345',
  },
  loginText: {
    color: 'white',
    fontWeight: 'bold',
  },
});

export default SolAprovada;
