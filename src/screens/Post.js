import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import Header from '../components/Header';

const Post = (props) => {
    const { navigation } = props;
    return (
        <View style={styles.container}>
        <Header />
            <Text style={styles.textIniciar}>{navigation.getParam('itemTitle', 'Post title')}</Text>
        </View>
    );
}

Post.navigationOptions = {
    headerShown: false,
};
  
const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor: '#333333',
    },
    textIniciar: {
        marginTop: 30,
        color: 'white',
        width: 300,
        height: 45,
        marginBottom: 10,
        fontSize: 20,
        fontWeight: 'bold',
        textAlign: 'left',
        marginLeft: 25,
    }
});

export default React.memo(Post);