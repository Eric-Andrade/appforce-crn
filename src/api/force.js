import axios from 'axios';
import AsyncStorage from '@react-native-community/async-storage';

//export default axios.create({
const instance = axios.create({
  // baseURL:'https://www.exituswebapps.com/AppForceControllers/controllers',
   baseURL:'https://www.exituswebapps.com/AppForceControllers_test/controllers'
});

instance.interceptors.request.use(
  async config => {
    const id = await AsyncStorage.getItem('id');
    if (id) {
      config.headers.Authorization = `Bearer ${id}`;
    }
    return config;
  },
  err => {
    return Promise.reject(err);
  },
);
export default instance;
