import createDataContext from './createDataContext';
import { useState, useEffect } from 'react';
import AsyncStorage from '@react-native-community/async-storage';
import ForceApi from '../api/force';
import {navigate} from '../navigationRef';

    const getRFC = ({vLastName,vSecondLastName,vName,vSecondName,vBirthDate}) => {
        axios.post(`http://exitusdesarrollo.eastus2.cloudapp.azure.com/AppForceControllers/controllers/GetRfcController.php`, {  vName, vSecondName, vLastName, vSecondLastName, vBirthDate })
            .then(res => {
              console.log(res.data.resultRFC);
              setvRFC(res.data.resultRFC);
            })
        }
        
        const getValidation = ({vCellphone,vRFC}) => {
          axios.post(`http://exitusdesarrollo.eastus2.cloudapp.azure.com/AppForceControllers/controllers/ValidateCellphoneController.php`, {vCellphone, vRFC})
              .then(res => {
                console.log(res.data.validCellphone);
                console.log(res.data.message);
                console.log(res.data.message);
                setvValidCellphone(res.data.validCellphone);
              })
          }
    
  



export const {Provider, Context} = createDataContext(
 
    { getRFC, getValidation,},
    
);