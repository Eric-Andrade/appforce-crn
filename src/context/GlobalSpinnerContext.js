import React, {createContext} from react

const GlobalSpinnerContext = createContext()

export default GlobalSpinnerContext