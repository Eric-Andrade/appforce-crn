import AsyncStorage from '@react-native-community/async-storage';import createDataContext from './createDataContext';
import ForceApi from '../api/force';
import {navigate} from '../navigationRef';

const dataReducer = (state, action) => {
    switch (action.type){
        case 'personalsave':
            return [
                ...state,
                {
                    vName:action.payload.vName,
                    vSecondName:action.payload.vSecondName,
                    vLastName: action.payload.vLastName,
                    vSecondLastName:action.payload,
                    vCellphone: action.payload.vCellphone,
                    vBirthDate: action.payload.vBirthDate,
                    vRFC: action.payload.vRFC,
                    vGender: action.payload.vGender,
                    vEmail:action.payload.vEmail,
                }
            ];
            default:
                return state;
    }
};

const guardar = dispatch => () => { 
    return (vName, vSecondName, vLastName, 
        vSecondLastName, vCellphone, vBirthDate, vRFC, vGender, vEmail, callback) =>{
        dispatch({type:'personalsave', payload: { vName:vName, vSecondName:vSecondName, vLastName, 
            vSecondLastName:vSecondLastName, vCellphone:vCellphone, vBirthDate:vBirthDate, vRFC:vRFC, vGender:vGender, vEmail:vEmail} });
            callback();
        };
};


export const {Context, Provider} = createDataContext(
    dataReducer,
    {guardar},
);
