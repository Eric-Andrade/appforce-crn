import React from 'react';
import AsyncStorage from '@react-native-community/async-storage';
import createDataContext from './createDataContext';
import ForceApi from '../api/force';
import {navigate} from '../navigationRef';

const SolContext = React.createContext();

export const SolProvider = ({children}) => {
    const solLists = [{title: 'algo'}, {title:'s 22222'}];

    return (
        <SolContext.Provider value ={solLists}>{children}</SolContext.Provider>
    );
};

export default SolContext;