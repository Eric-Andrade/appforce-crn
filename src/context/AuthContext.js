/* eslint-disable no-trailing-spaces */
/* eslint-disable prettier/prettier */
import AsyncStorage from '@react-native-community/async-storage';
import createDataContext from './createDataContext';
import ForceApi from '../api/force';
import {navigate} from '../navigationRef';

    const authReducer = (state, action) => {
        console.log('state', state);
        console.log('action', action.payload);
        
        switch (action.type) {
            case 'add_error':
                return {...state, errorMessage: action.payload};
            case 'login':
                    // return { errorMessage:'', id: action.payload.id};
                    return { error: action.payload.error ? action.payload.error : false, 
                            errorMessage: action.payload.error ? action.payload.message : '', 
                            id: action.payload.id 
                            };
            case 'clear_error_message':
                    return { ...state, 
                            errorMessage: '' 
                            };
            case 'guardar':
                return { error: action.payload.error ? action.payload.error : false,
                        errorMessage: action.payload.message ? action.payload.message : '',
                        id: action.payload.id ? action.payload.id : false,
                        continue: action.payload.continue  ? action.payload.continue :  false
                        };
            case 'rfc':
                return { ...state,
                        errorMessage: action.payload};
            case 'logout':
                return {id: null, errorMessage: ''}
            default:
                return state;
        }
    };
    const guardar = dispatch => async ({ vName, vSecondName, vLastName, vSecondLastName, vCellphone, vPhone, vBirthDate, vRFC, vGender, vEmail }) => {
        try {
            const id = await AsyncStorage.getItem('id');
            const vCreationUser = id;
            console.log('Guardar datos personales');
            console.log({ vName, vSecondName, vLastName, vSecondLastName, vCellphone, vPhone,vBirthDate, vRFC, vGender, vEmail, vCreationUser });
            const response = await ForceApi.post('/ValidationPersonalInfoController.php', { vName, vSecondName, vLastName, vSecondLastName, vCellphone, vPhone,vBirthDate, vRFC, vGender, vEmail, vCreationUser });
            await AsyncStorage.setItem('SolicitudeId', response.data.solicitude_id+"")
            
             const pay = {
                id: response.data.id ? response.data.id: null,
                message: response.data.message,
                error: response.data.error,
                solicitude_id: response.data.solicitude_id, 
                continue: response.data.continue ? response.data.continue : false
            }

            console.log('datos personales pay');
            
            console.log(pay);
            
            dispatch({ type: 'guardar', payload: pay });
            const Validar = response.data.error;
            if(response.data.continue == true){
                navigate('nsPais');
            }
        } catch (err) {
            console.log(err);
            dispatch({
            type: 'add_error',
            payload: 'Error de Conexion'
            });
        }
    };
    const guardarPais = dispatch => async ({ vBirthState, vCivilStatus, vResidenceTime, vCURP }) => {
        try {
            const vBirthCountry = 'MEXICO';
            const vNationality = 'MEXICANA';
            const vCreationUser = await AsyncStorage.getItem('id');
            const vSolicitudeId = await AsyncStorage.getItem('SolicitudeId');
            console.log('Guardar datos pais');
            console.log({vCreationUser, vSolicitudeId, vBirthState, vCivilStatus, vResidenceTime, vCURP });
            
            const response = await ForceApi.post('/UpdateComplementaryInfoController.php', 
            {vSolicitudeId, vCreationUser, vBirthState,vBirthCountry,vNationality, vCivilStatus, vResidenceTime,vCURP });
            const pay = {
                id: response.data.id ? response.data.id: null,
                message: response.data.message,
                error: response.data.error
            }
            dispatch({ type: 'guardar', payload: pay});
            console.log(response.data);
            console.log(response.data.solicitude_id);
            const Validar = response.data.error;
            if(Validar == false ){
                
                navigate('SolDatosDomicilio');
            }
        } catch (err) {
            console.log(err);
            dispatch({
            type: 'add_error',
            payload: 'Error de Conexion'
            });
        }
    };
    const guardarDom = dispatch => async ({vZipCode, vStreet, vExteriorNumber, vInteriorNumber, vState, vCity, vSuburb }) => {
        try {
            const vCreationUser = await AsyncStorage.getItem('id');
            // const vCreationUser = 6;
            const vSolicitudeId = await AsyncStorage.getItem('SolicitudeId');
            console.log('Guardar datos domicilio');
            console.log({vCreationUser, vSolicitudeId, vZipCode, vStreet, vExteriorNumber, vInteriorNumber, vState, vCity, vSuburb });
        
            const response = await ForceApi.post('/InsertAddressInfoController.php', { vSolicitudeId, vZipCode, vStreet, vExteriorNumber, vInteriorNumber, vState, vCity, vSuburb, vCreationUser});
        
            console.log('response.data guardarDom: ', response.data);
            dispatch({ type: 'guardar', payload: response.data.id, payload: response.data.error});

            const Validar = response.data.error;
            const Validar2 = response.data;
            console.log('Validar2: ', Validar2);

            if(Validar == false ){
                navigate('SolDatosLaborales');
            }
        } catch (err) {
            console.log(err);
            dispatch({
            type: 'add_error',
            payload: 'Error de Conexion'
            });
        }
    };
    const guardarLab = dispatch => async ({ vSalary, vPensionInstitute, vBank, vPensionKind}) => {
        try {
            const id = await AsyncStorage.getItem('id');
            const vCreationUser = id;
            const vSolicitudeId = await AsyncStorage.getItem('SolicitudeId');
            console.log('Guardar datos laborales');
            console.log({vCreationUser, vSolicitudeId, vSalary, vPensionInstitute, vBank, vPensionKind});
        
            const response = await ForceApi.post('/InsertWorkInfoController.php', {vSolicitudeId, vCreationUser, vSalary, vPensionInstitute, vBank, vPensionKind});
            const pay = {
                id: response.data.id ? response.data.id: null,
                message: response.data.message,
                error: response.data.error,
                solicitude_id: response.data.solicitude_id
            }
            console.log('guardarLab: ', pay)
            dispatch({ type: 'guardar', payload: pay});console.log(response.data);
            const Validar = response.data.error;
            console.log(Validar);
            if(Validar == false ){
                // navigate('Seleccion');
                navigate('SolAutorizacion');
            }
        } catch (err) {
            console.log(err);
            dispatch({
            type: 'add_error',
            payload: 'Error de Conexion'
            });
        }
    };
    const PinMsg = dispatch => async () => {
        try {
            const vSolicitudeId = await AsyncStorage.getItem('SolicitudeId');
            console.log('pinmsg fue clickeado');
            
            const response = await ForceApi.post('/SendValidationCodeController.php', {vSolicitudeId});
            const pay = {
                id: response.data.id ? response.data.id: null,
                message: response.data.message,
                error: response.data.error
            }
            dispatch({ type: 'guardar', payload: pay });
            console.log(response.data.message);
            console.log(response.data.code);
        } catch (err) {
            console.log(err);
            dispatch({
                type: 'add_error',
                payload: 'Error de Conexion'
            });
        }
    };
    const PinCode = dispatch => async ({vCode}) => {
        try {
            
            const vSolicitudeId = await AsyncStorage.getItem('SolicitudeId');
            console.log('PinCode: ');
            console.log('/////////////////: ');
            console.log({vSolicitudeId, vCode});
            // 
                // const pay = {
                //     id: null,
                //     message: 'Código enviado correctamente',
                //     error: !true,
                //     continue: !false
                // }
                
                // dispatch({ type: 'guardar', payload: pay});

                // if (pay.error !== true && pay.continue !== false) {
                //     navigate('CreditoAprobado',{msg:'ewe9'});
                // }else { 
                //     console.log('/////////////////: 4');
                //     navigate('SolRechazada');  
                // }
            // 
            
            const response = await ForceApi.post('/ValidateCodeController.php', {vSolicitudeId,vCode});
            const pay = {
                id: response.data.id ? response.data.id: null,
                message: response.data.message,
                error: response.data.error
            }

            console.log('PinCode response: ', response.data);
            console.log('/////////////////:2 ');
            dispatch({ type: 'guardar', payload: pay});
            console.log(response.data.message);
            console.log(response.data.code);
            const Validar = response.data.error;
            console.log(Validar);
            if(Validar == false ){
                console.log('/////////////////:3 ');
                const vCreationUser = await AsyncStorage.getItem('id');
                const { data } = await ForceApi.post(`/ConsultBCController.php`, {vSolicitudeId, vCreationUser});
                if (data.error !== true && data.politics !== false) {
                    navigate('CreditoAprobado',{msg:data.bureau_message});
                }else { 
                    console.log('/////////////////: 4');
                    navigate('SolRechazada');  
                }

            }
        } catch (err) {
            console.log(err);
            dispatch({
                type: 'add_error',
                payload: 'Error de Conexion'
            });
        }
    };
    const guardarCalc = dispatch => async ({vMount,lastObject, vPayment,vClientGet})=> {
        const vTerm = lastObject;
        const vCreationUser = await AsyncStorage.getItem('id');
        const vSolicitudeId = await AsyncStorage.getItem('SolicitudeId');
        console.log('guardar datos calculadora');
        console.log({vCreationUser, vSolicitudeId, vMount,lastObject, vPayment,vClientGet});
        console.log('//////////////////////////////////////');
        const response = await ForceApi.post(`/InsertSimulatorInfoController.php`, {vSolicitudeId,vCreationUser,vMount,vClientGet,vTerm,vPayment});
        const pay = {
            id: response.data.id ? response.data.id: null,
            message: response.data.message,
            error: response.data.error
        }
        console.log('//////////////////////////////////////');
        dispatch({ type: 'guardar', payload: pay});
        const Validar = response.data.error;
            console.log(Validar);
            if(Validar == false ){
                console.log('//////////////////////////////////////');
                navigate('nsDatosAdicionales');
            }
    };
     
    const guardarCalcOCR = dispatch => async ({
        vSolicitudeId,
        vMount,
        vClientGet,
        lastObject,
        vPayment,
      }) => {
        const vTerm = lastObject;
        const vCreationUser = await AsyncStorage.getItem('id');
        console.log('guardar datos calculadoraOCR');
        console.log({ vCreationUser, vSolicitudeId, vMount, vClientGet, lastObject, vPayment });
        
        const response = await ForceApi.post(`/InsertSimulatorInfoController.php`, {
          vSolicitudeId,
          vCreationUser,
          vMount,
          vClientGet,
          vTerm,
          vPayment,
        });
        console.log('RESPONSE:',response.data);
        console.log('RESPONSE DATA:',response.data.error);
        const pay = {
          id: response.data.id ? response.data.id : null,
          message: response.data.message,
          error: response.data.error,
        };
        dispatch({type: 'guardar', payload: pay});
        const Validar = response.data.error;
        console.log(Validar);
        if (Validar == false) {
          navigate('nsDatosAdicionales');
        }
      };
    const guardarPaisOCR = dispatch => async ({ vBirthState, vCivilStatus, vResidenceTime }) => {
        try {
            const vBirthCountry = 'MEXICO';
            const vNationality = 'MEXICANA';
            const vCreationUser = await AsyncStorage.getItem('id');
            const vSolicitudeId = await AsyncStorage.getItem('SolicitudeId');
            const response = await ForceApi.post('/UpdateComplementaryInfoController.php', 
            {vSolicitudeId, vCreationUser, vBirthState,vBirthCountry,vNationality, vCivilStatus, vResidenceTime });
            const pay = {
                id: response.data.id ? response.data.id: null,
                message: response.data.message,
                error: response.data.error
            }
            dispatch({ type: 'guardar', payload: pay});
            console.log(response.data);
            console.log(response.data.solicitude_id);
            const Validar = response.data.error;
            if(Validar == false ){
                navigate('nsPaisOCR');
            }
        } catch (err) {
            console.log(err);
            dispatch({
            type: 'add_error',
            payload: 'Error de Conexion'
            });
        }
    };
    const guardarRef = dispatch => async ({vFirstReferenceName,vFirstReferenceSecondName,vFirstReferenceLastName,vFirstReferenceSecondLastName,vFirstReferenceCellphone,vFirstReferenceRelationship,vFirstReferenceOccupation,vSecondReferenceName,vSecondReferenceSecondName,vSecondReferenceLastName,vSecondReferenceSecondLastName,vSecondReferenceCellphone,vSecondReferenceRelationship,vSecondReferenceOccupation }) => {
        try {console.log('ANTES DEL LOG');
            const vCreationUser = await AsyncStorage.getItem('id');
            const vSolicitudeId = await AsyncStorage.getItem('SolicitudeId');
            // const vSolicitudeId = 4;
            console.log('guardar datos referencias');
            console.log({vSolicitudeId,vCreationUser,vFirstReferenceName,vFirstReferenceSecondName,vFirstReferenceLastName,vFirstReferenceSecondLastName,vFirstReferenceCellphone,vFirstReferenceRelationship,vFirstReferenceOccupation,vSecondReferenceName,vSecondReferenceSecondName,vSecondReferenceLastName,vSecondReferenceSecondLastName,vSecondReferenceCellphone,vSecondReferenceRelationship,vSecondReferenceOccupation});
            
            const response = await ForceApi.post('/InsertReferenceInfoController.php', 
            {vSolicitudeId,vCreationUser,vFirstReferenceName,vFirstReferenceSecondName,vFirstReferenceLastName,vFirstReferenceSecondLastName,vFirstReferenceCellphone,vFirstReferenceRelationship,vFirstReferenceOccupation,vSecondReferenceName,vSecondReferenceSecondName,vSecondReferenceLastName,vSecondReferenceSecondLastName,vSecondReferenceCellphone,vSecondReferenceRelationship,vSecondReferenceOccupation });
            
            const pay = {
                id: response.data.id ? response.data.id: null,
                message: response.data.message,
                error: response.data.error
            }
            dispatch({ type: 'guardar', payload: pay});
            console.log('despues DEL LOG');
            console.log(response.data);
            console.log(response.data.solicitude_id);
            const Validar = response.data.error;
            if(Validar == false ){
                console.log('DESPUES DE');
                navigate('Selfie');
            }
        } catch (err) {
            console.log(err);
            dispatch({
            type: 'add_error',
            payload: 'Error de Conexion'
            });
        }
    };
    const guardarD0 = dispatch => async ({vDocumentB64})=> {
        const vKindDocument = 'D61';
        const vCreationUser = await AsyncStorage.getItem('id');
        const vSolicitudeId = await AsyncStorage.getItem('SolicitudeId');
        
        console.log('guardarD0 vDocumentB64', vDocumentB64);
        
        console.log({
            vDocumentB64,
            vKindDocument,
            vCreationUser,
            vSolicitudeId
        });
        
        const response = await ForceApi.post(`/InsertImagesController.php`, {vSolicitudeId,vCreationUser,vKindDocument,vDocumentB64});
        const pay = {
            id: response.data.id ? response.data.id: null,
            message: response.data.message,
            error: response.data.error
        }
        
        dispatch({ type: 'guardar', payload: pay});console.log(response.data);
        //navigate('nsDatosAdicionales');
        const Validar = response.data.error;
            console.log(Validar);
            if(Validar == false ){
                navigate('nsCargaDocumentos');
            }
    };
    const guardarD1 = dispatch => async ({vDocumentB64})=> {
    const vKindDocument = 'D1';
    const vCreationUser = await AsyncStorage.getItem('id');
    const vSolicitudeId = await AsyncStorage.getItem('SolicitudeId');
    // const vSolicitudeId = 1127
    // console.log('guardarD1: ',{vSolicitudeId,vCreationUser,vKindDocument,vDocumentB64});
    
    const response = await ForceApi.post(`/InsertImagesController.php`, {vSolicitudeId,vCreationUser,vKindDocument,vDocumentB64});
    const pay = {
        id: response.data.id ? response.data.id: null,
        message: response.data.message,
        error: response.data.error ? response.data.error : false
    }
    console.log('guardarD1 response.data: ', response.data);
    dispatch({ type: 'guardar', payload: pay});console.log(response.data);
    //navigate('nsDatosAdicionales');
    const Validar = response.data.error;
        console.log(Validar);
        const face = await ForceApi.post(`/FaceMatchController.php`, {vSolicitudeId});
    const pay2 = {
        id: face.data.id,
        error: response.data.error ? response.data.error : false

    }
    dispatch({ type: 'guardar', payload: pay2});console.log(face.data);
        if(Validar == false ){
            navigate('nsCargaDocumentosD2');
        }
    };
    const guardarD2 = dispatch => async ({vDocumentB64})=> {
        const vKindDocument = 'D2';
        const vCreationUser = await AsyncStorage.getItem('id');
        const vSolicitudeId = await AsyncStorage.getItem('SolicitudeId');
        // const vSolicitudeId = 400

        console.log('AuthContext guardarD2: ', {vDocumentB64, vCreationUser, vSolicitudeId });
        
        const response = await ForceApi.post(`/InsertImagesController.php`, {vSolicitudeId,vCreationUser,vKindDocument,vDocumentB64});
        const pay = {
            id: response.data.id ? response.data.id: null,
            message: response.data.message,
            error: response.data.error
        }
        const face = await ForceApi.post(`/FaceMatchController.php`, {vSolicitudeId});
        const pay2 = {
            id: face.data.id,
            error: response.data.error ? response.data.error : false
    
        }
        dispatch({ type: 'guardar', payload: pay});console.log(response.data);
        const Validar = response.data.error;
        console.log(Validar);
        if(Validar == false ){
            navigate('nsCargaDocumentosD3');
        }
    };
    const guardarD3 = dispatch => async ({vDocumentB64})=> {
    const vKindDocument = 'D3';
    const vCreationUser = await AsyncStorage.getItem('id');
    const vSolicitudeId = await AsyncStorage.getItem('SolicitudeId');
    // const vSolicitudeId = 400
    const response = await ForceApi.post(`/InsertImagesController.php`, {vSolicitudeId,vCreationUser,vKindDocument,vDocumentB64});
    const pay = {
        id: response.data.id ? response.data.id: null,
        message: response.data.message,
        error: response.data.error
    }
    dispatch({ type: 'guardar', payload: pay});console.log(response.data);
    const Validar = response.data.error;
    console.log(Validar);
    if(Validar == false ){
        navigate('nsCargaDocumentosD4');
    }
    };
    const guardarD4 = dispatch => async ({vDocumentB64})=> {
    const vKindDocument = 'D4';
    const vCreationUser = await AsyncStorage.getItem('id');
    const vSolicitudeId = await AsyncStorage.getItem('SolicitudeId');
    // const vSolicitudeId = 400
    const response = await ForceApi.post(`/InsertImagesController.php`, {vSolicitudeId,vCreationUser,vKindDocument,vDocumentB64});
    const pay = {
        id: response.data.id ? response.data.id: null,
        message: response.data.message,
        error: response.data.error
    }
    dispatch({ type: 'guardar', payload: pay});console.log(response.data);
    const Validar = response.data.error;
    console.log(Validar);
    if(Validar == false ){
        navigate('nsCargaDocumentosD5');
    }
    };
    const guardarD5 = dispatch => async ({vDocumentB64})=> {
    const vKindDocument = 'D5';
    const vCreationUser = await AsyncStorage.getItem('id');
    const vSolicitudeId = await AsyncStorage.getItem('SolicitudeId');
    // const vSolicitudeId = 400
    const response = await ForceApi.post(`/InsertImagesController.php`, {vSolicitudeId,vCreationUser,vKindDocument,vDocumentB64});
    const pay = {
        id: response.data.id ? response.data.id: null,
        message: response.data.message,
        error: response.data.error
    }
    dispatch({ type: 'guardar', payload: pay });console.log(response.data);
    const Validar = response.data.error;
    console.log(Validar);
    if(Validar == false ){
        navigate('nsCargaDocumentosD7');
    }
    };
    const guardarD7 = dispatch => async ({vDocumentB64})=> {
    const vKindDocument = 'D7';
    const vCreationUser = await AsyncStorage.getItem('id');
    const vSolicitudeId = await AsyncStorage.getItem('SolicitudeId');
    // const vSolicitudeId = 400
    const response = await ForceApi.post(`/InsertImagesController.php`, {vSolicitudeId,vCreationUser,vKindDocument,vDocumentB64});
    const pay = {
        id: response.data.id ? response.data.id: null,
        message: response.data.message,
        error: response.data.error
    }
    dispatch({ type: 'guardar', payload: pay});console.log(response.data);
    const Validar = response.data.error;
    console.log(Validar);
    if(Validar == false ){
        navigate('nsCargaDocumentosD8');
    }
    };
    const guardarD8 = dispatch => async ({vDocumentB64})=> {
    const vKindDocument = 'D8';
    const vCreationUser = await AsyncStorage.getItem('id');
    const vSolicitudeId = await AsyncStorage.getItem('SolicitudeId');
    // const vSolicitudeId = 400
    const response = await ForceApi.post(`/InsertImagesController.php`, {vSolicitudeId,vCreationUser,vKindDocument,vDocumentB64});
    const pay = {
        id: response.data.id ? response.data.id: null,
        message: response.data.message,
        error: response.data.error
    }
    dispatch({ type: 'guardar', payload: pay });console.log(response.data);
    const Validar = response.data.error;
    console.log(Validar);
    if(Validar == false ){
        navigate('nsCargaDocumentosOP1');
    }
    };
    const guardarOP1 = dispatch => async ({vDocumentB64})=> {
    const vKindDocument = 'OP1';
    const vCreationUser = await AsyncStorage.getItem('id');
    const vSolicitudeId = await AsyncStorage.getItem('SolicitudeId');
    // const vSolicitudeId = 400
    const response = await ForceApi.post(`/InsertImagesController.php`, {vSolicitudeId,vCreationUser,vKindDocument,vDocumentB64});
    const pay = {
        id: response.data.id ? response.data.id: null,
        message: response.data.message,
        error: response.data.error
    }
    dispatch({ type: 'guardar', payload: pay });console.log(response.data);
    const Validar = response.data.error;
    console.log(Validar);
    if(Validar == false ){
        navigate('nsCargaDocumentosD11');
    }
    };
    const guardarD11 = dispatch => async ({vDocumentB64})=> {
    const vKindDocument = 'D11';
    const vCreationUser = await AsyncStorage.getItem('id');
    const vSolicitudeId = await AsyncStorage.getItem('SolicitudeId');
    // const vSolicitudeId = 400
    const response = await ForceApi.post(`/InsertImagesController.php`, {vSolicitudeId,vCreationUser,vKindDocument,vDocumentB64});
    const pay = {
        id: response.data.id ? response.data.id: null,
        message: response.data.message,
        error: response.data.error
    }
    dispatch({ type: 'guardar', payload: pay });console.log(response.data);
    const Validar = response.data.error;
    console.log(Validar);
    if(Validar == false ){
        navigate('SolFinalizada');
    }
    };
    const guardarOcr = dispatch => async ({ vCredentialFront, vCredentialBack, vCellphone, vEmail })=> {
        const vCreationUser = await AsyncStorage.getItem('id');
        //   vCreationUser = 6;
        //   vCellphone = '1234567891';
        //   vEmail = 'buro@algo.com';

        const response = await ForceApi.post(`/ValidationPersonalInfoOCRController.php`, {vCreationUser,vCredentialFront,vCredentialBack,vCellphone,vEmail});
        await AsyncStorage.setItem('SolicitudeId', response.data.solicitude_id+"")
        const pay = {
            id: response.data.id ? response.data.id: null,
            message: response.data.message ? response.data.message : '',
            error: response.data.error,
            solicitude_id: response.data.solicitude_id
        }
        console.log('guardarOcr pay: ', pay);
        
        dispatch({ type: 'guardar', payload: pay });console.log(response.data);
        
        const Validar = response.data.error;
        console.log(Validar);
        if(Validar == false ){
            navigate('OcrInfo');
        }
    };
    const guardarIrre = dispatch => async ({vKindDocument,vComment,vDocument,vSolicitudeId})=> {
        const vCreationUser = await AsyncStorage.getItem('id');
        console.log('guardarIrre');
        
        console.log({vKindDocument,vComment,vCreationUser,vSolicitudeId});
        
        const response = await ForceApi.post(`/AttendIrregularityController.php`, {vKindDocument,vComment,vCreationUser,vSolicitudeId,vDocument});
        const pay = {
            id: response.data.id ? response.data.id: null,
            message: response.data.message,
            error: response.data.error
        }
        dispatch({ type: 'guardar', payload: pay });
        const Validar = response.data.error;
        /*if(Validar == false ){
            setTimeout(() => {
                navigate('Solicitudes1');
            }, 3000);
        }*/
    };
    const clearErrorMessage = dispatch => () => {
        dispatch({ type: 'clear_error_message' });
    };
    const tryLocalSignin = dispatch => async () => {
        const id = await AsyncStorage.getItem('id');
        if (id) {
            dispatch({ type: 'login', payload: id });
            navigate('Panel');
        } else {
            navigate('login');
        }
    };
    const login  = dispatch => async ({ vCellphone, vPassword, loading }) => {
        try {
            const response = await ForceApi.post('/LoginController.php', { vCellphone, vPassword });
            //console.log(response);
            const Validar = response.data.error;
            // console.log(Validar);
            // console.log(response.data.message);  
            const pay = {
                id: response.data.id ? response.data.id: null,
                message: response.data.message,
                error: response.data.error
            }

            if(!response.data.error) {
                await AsyncStorage.setItem('id', response.data.id);
                dispatch({ type: 'login', payload: pay});
                // dispatch({ type: 'login', payload: response.data.id});
                if(Validar == false ){
                navigate('Panel');
                }
            }

            dispatch({ type: 'login', payload: pay});
             

        } catch (err) {
            console.log(err);
            dispatch({
            type: 'add_error',
            payload: 'Error de Conexion'
            });
        }
    };
    const logout = dispatch => async () => {
        await AsyncStorage.removeItem('id');
        dispatch({ type: 'logout' });
        navigate('login');
    };

export const {Provider, Context} = createDataContext(
    authReducer,
    { login, logout, clearErrorMessage, guardarCalcOCR,tryLocalSignin,guardarIrre,guardarPaisOCR, guardar,guardarDom,guardarLab,PinMsg,PinCode,guardarCalc,guardarD0,guardarRef,guardarD1,guardarOP1,guardarD2,guardarD3,guardarD4,guardarD5,guardarD7,guardarD8,guardarD11,guardarOcr,guardarPais
},
    {id:null, vSolicitudeId:null,errorMessage: ''}, []
);
