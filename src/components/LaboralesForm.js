import React, {useState, useEffect} from 'react';
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableOpacity,
  Image,
  Alert,
  ScrollView,
  Picker,
  ActivityIndicator,
  Platform
} from 'react-native';
import ForceApi from '../api/force';
import Ionicons from 'react-native-vector-icons/Ionicons';
import {useModal} from './useModal';
import InfoModal from './InfoModal';
import RNPickerSelect from 'react-native-picker-select';
import Spinner from 'react-native-loading-spinner-overlay';
import { Formik } from 'formik';
import * as yup from 'yup';

const validationSchema = yup.object().shape({
    fSalary: yup
      .string()
      .matches(/^[0-9]*$/, 'Ingrese únicamente números')
      // .min(5, 'Código postal debe tener 5 dígitos')
      // .max(5, 'Código postal debe tener 5 dígitos')
      .typeError('Ingrese únicamente números')
      .required('Este campo no puede estar vacío'),
})

const LaboralesForm = ({state, errorMessage, onSubmit}) => {
  const [itemModalOpen, setItemModalOpen, toggleModal] = useModal();
  const [pPensionInstitute, setpPensionInstitute] = useState([]);
  const [vPensionInstitute, setvPensionInstitute] = useState('');
  const [vSalary, setvSalary] = useState('');
  const [pBank, setpBank] = useState([]);
  const [vBank, setvBank] = useState(2);
  const [pPensionKind, setpPensionKind] = useState([]);
  const [vPensionKind, setvPensionKind] = useState('');

  const [CPensionInstitute, setCPensionInstitute] = useState(false);
  const [CSalary, setCSalary] = useState(false);
  const [CBank, setCBank] = useState(false);
  const [CPensionKind, setCPensionKind] = useState(false);

  const [Visible, setVisible] = useState(false);
  const [isLoading, setisLoading] = useState(false);

  CheckTextInput = () => {
    // console.log('vPensionInstitute',vPensionInstitute,'vBank',vBank,'vPensionKind',vPensionKind)
    if (
      vPensionInstitute != '' &&
      vSalary != '' &&
      vBank != '' &&
      vPensionKind != ''
    ) {
      setCPensionInstitute(false);
      setCSalary(false);
      setCBank(false);
      setCPensionKind(false);
      onSubmit({vSalary, vPensionInstitute, vBank, vPensionKind});
      setVisible(true);
      // setisLoading(false);
      toggleModal(true);
    }
    if (vSalary != '') {
      setCSalary(false);
    } else {
      setCSalary(true);
    }

    if (vPensionInstitute != 0) {
      setCPensionInstitute(false);
    } else {
      setCPensionInstitute(true);
    }

    if (vBank > 0) {
      setCBank(false);
    } else {
      setCBank(true);
    }

    if (vPensionKind > 0) {
      setCPensionKind(false);
    } else {
      setCPensionKind(true);
    }
  };

  async function getBank({vPensionInstitute}) {
    const {data} = await ForceApi.post(`/GetBanksController.php`, {
      vPensionInstitute,
    });
    var arrayBank = data.banks || [];
    var arrBank = [];

    //console.log('ewe');
    //console.log(arrayBank);
    
    arrayBank.forEach((element, index) => {
      arrBank.push({
        label: element.text,
        value: element.value
      })
    });

    setpBank(arrBank);
  }

  useEffect(() => {
    let isCancelled = false;
    async function getInst() {
      setpPensionInstitute([])
      const {data} = await ForceApi.post(`/GetPensionInstituteController.php`);
      var array = data.pension;
      var arr = [];
      array.forEach((element, index) => {
        arr.push({
          label: element.institute_alias,
          value: element.id
        })
      });
      setpPensionInstitute(arr);
    }

    async function getKind() {
      const {data} = await ForceApi.post(`/GetKindPensionController.php`);
      var arrayKind = data.pension;
      console.log('ewe0');
      console.log(data.pension);
      
      var arrKind = [];
      arrayKind.forEach((element, index) => {
        arrKind.push({
          label: element.pension_alias,
          value: element.id
        })
      });
      
      setpPensionKind(arrKind);
    }

    getInst();
    getKind();

    return () => {
      isCancelled = true;
    };
  }, []);

  useEffect(() => {
    let isCancelled = false;
    if (Visible && state.errorMessage != '') {
      setVisible(!Visible);
    } else {
      setVisible(false);
    }
    return () => {
      isCancelled = true;
    };
  }, [state]);
  // pPensionInstitute
  // pBank
  // pPensionKind

  if (Visible) {
    return (
      <Spinner
        size="large"
        visible={Visible}
        color="#D32345"
        textContent={'Un momento por favor'}
        textStyle={styles.spinnerTextStyle}
      />
    );
  }

  return (
    <ScrollView>
      {state.error == true ? (
        <InfoModal
          text={`${state.errorMessage}`}
          isActive={itemModalOpen}
          handleClose={() => setItemModalOpen(false)}
        />
      ) : null}

      <Formik
        enableReinitialize={true}
        initialValues={{
          fSalary: '' 
        }}
        onSubmit={(values, actions) => {
          setvSalary(values.fSalary);
          CheckTextInput()
          setTimeout(() => {
              actions.setSubmitting(false)
          }, 2000);
        }}
        validationSchema={validationSchema}
        >
        {({ handleChange, handleBlur, handleSubmit, values, isSubmitting, errors }) => (
          <>
          <View style={styles.buttonContainer}>
            <View style={styles.inputContainer}>
              <TextInput
                style={styles.inputs}
                placeholder="Ingreso mensual neto"
                placeholderTextColor="white"
                underlineColorAndroid="transparent"
                // onChangeText={newvSalary => setvSalary(newvSalary.toUpperCase())}
                // value={vSalary}
                autoCorrect={false}
                autoCapitalize="characters"
                keyboardType={'numeric'}
                maxLength={10}
                
                onChangeText={handleChange('fSalary')}
                onBlur={handleBlur('fSalary')}
                value={values.fSalary}
              />
            </View>
            {
              errors.fSalary
              ? <Text style={styles.ErrorM}>{errors.fSalary}</Text>
              : null
            }
            {/* <View style={styles.inputContainer}>
              <TextInput
                style={styles.inputs}
                placeholder="Ingreso mensual neto"
                placeholderTextColor="white"
                underlineColorAndroid="transparent"
                onChangeText={newvSalary => setvSalary(newvSalary.toUpperCase())}
                value={vSalary}
                autoCorrect={false}
                autoCapitalize="characters"
                keyboardType={'numeric'}
              />
            </View>
            {CSalary == true ? (
              <Text style={styles.ErrorM}>Este campo no puede estar vacío</Text>
            ) : null} */}

            <View style={styles.ocointainer}>
              <View style={styles.inputContainer}>
              <RNPickerSelect
                  placeholder={{
                    label: 'Instituto que paga pensión',
                    value: null,
                    color: '#9EA0A4'
                  }}
                  style={{
                    placeholder: {
                      flexDirection: 'column',
                      justifyContent: 'center',
                      alignItems: 'center',
                      color: 'white',
                      marginLeft: 15
                    },
                    inputAndroid: {
                      color: 'white',
                      width: 270,
                      height: 40,
                      flexDirection: 'column',
                      justifyContent: 'center',
                      alignItems: 'center',
                      color: 'white',
                      marginLeft: 15
                    },
                    inputIOS: {
                      color: 'white',
                      width: 270,
                      height: 40,
                      flexDirection: 'column',
                      justifyContent: 'center',
                      alignItems: 'center',
                      color: 'white',
                      marginLeft: 15,
                      marginTop: 15
                    },
                    iconContainer: {
                      top: 10,
                      right: 15,
                    }
                  }}
                  useNativeAndroidPickerStyle={false}
                  Icon={() => {
                    return <Ionicons name='md-arrow-dropdown' size={20} color='#FFF' />
                  }}
                  onClose={Platform.OS === 'ios' ? () => getBank({vPensionInstitute}) : getBank({vPensionInstitute})}
                  onValueChange={newpPensionInstitute => setvPensionInstitute(newpPensionInstitute)}
                  items={pPensionInstitute}
                  // items={pSuburb}
                />
                {/* <Picker
                  style={styles.pick}
                  selectedValue={vPensionInstitute}
                  itemStyle={styles.onePickerItem}
                  onValueChange={newpPensionInstitute =>
                    setvPensionInstitute(newpPensionInstitute)
                  }
                  onChange={getBank({vPensionInstitute})}>
                  <Picker.Item label="Instituto que paga pensión" value="10" />
                  {(pPensionInstitute || []).map((item, index) => {
                    return (
                      <Picker.Item
                        key={index}
                        label={`${item.institute_alias}`}
                        value={`${item.id}`}
                      />
                    );
                  })}
                </Picker> */}
              </View>
            </View>
            {CPensionInstitute == true ? (
              <Text style={styles.ErrorM}>Este campo no puede estar vacío</Text>
            ) : null}
            <View style={styles.ocointainer}>
              <View style={styles.inputContainer}>
                <RNPickerSelect
                  placeholder={{
                    label: 'Seleccione banco',
                    value: null,
                    color: '#9EA0A4'
                  }}
                  style={{
                    placeholder: {
                      flexDirection: 'column',
                      justifyContent: 'center',
                      alignItems: 'center',
                      color: 'white',
                      marginLeft: 15
                    },
                    inputAndroid: {
                      color: 'white',
                      flexDirection: 'column',
                      justifyContent: 'center',
                      alignItems: 'center',
                      color: 'white',
                      marginLeft: 15
                    },
                    inputIOS: {
                      color: 'white',
                      flexDirection: 'column',
                      justifyContent: 'center',
                      alignItems: 'center',
                      color: 'white',
                      marginLeft: 15,
                      marginTop: 15
                    },
                    iconContainer: {
                      top: 10,
                      right: 15,
                    }
                  }}
                  useNativeAndroidPickerStyle={false}
                  onValueChange={newpBank => setvBank(newpBank)}
                  items={pBank}
                />
                
                {/* <Picker
                  style={styles.pick}
                  selectedValue={vBank}
                  itemStyle={styles.onePickerItem}
                  onValueChange={newpBank => setvBank(newpBank)}>
                  <Picker.Item label="Seleccione banco" value="word2" />
                  {(pBank || []).map((item, index) => {
                    return (
                      <Picker.Item
                        key={index}
                        label={`${item.text}`}
                        value={`${item.value}`}
                      />
                    );
                  })}
                </Picker> */}
                <TouchableOpacity style={styles.inputIcon}>
                  <Image source={require('../../assets/Flecha.png')} />
                </TouchableOpacity>
              </View>
            </View>
            {CBank == true ? (
              <Text style={styles.ErrorM}>Este campo no puede estar vacío</Text>
            ) : null}
            <View style={styles.ocointainer}>
              <View style={styles.inputContainer}>
              <RNPickerSelect
                placeholder={{
                  label: 'Tipo pensión',
                  value: null,
                  color: '#9EA0A4'
                }}
                style={{
                  placeholder: {
                    flexDirection: 'column',
                    justifyContent: 'center',
                    alignItems: 'center',
                    color: 'white',
                    marginLeft: 15
                  },
                  inputAndroid: {
                    color: 'white',
                    flexDirection: 'column',
                    justifyContent: 'center',
                    alignItems: 'center',
                    color: 'white',
                    marginLeft: 15
                  },
                  inputIOS: {
                    color: 'white',
                    flexDirection: 'column',
                    justifyContent: 'center',
                    alignItems: 'center',
                    color: 'white',
                    marginLeft: 15,
                    marginTop: 15
                  },
                  iconContainer: {
                    top: 10,
                    right: 15,
                  }
                }}
                useNativeAndroidPickerStyle={false}
                onValueChange={newpPensionKind => setvPensionKind(newpPensionKind)}
                items={pPensionKind}
              />
                {/* <Picker
                  style={styles.pick}
                  selectedValue={vPensionKind}
                  itemStyle={styles.onePickerItem}
                  onValueChange={newpPensionKind =>
                    setvPensionKind(newpPensionKind)
                  }>
                  <Picker.Item label="Tipo pensión" value="word3" />
                  {(pPensionKind || []).map((item, index) => {
                    return (
                      <Picker.Item
                        key={index}
                        label={`${item.pension_alias}`}
                        value={`${item.id}`}
                      />
                    );
                  })}
                </Picker> */}
                <TouchableOpacity style={styles.inputIcon}>
                  <Image source={require('../../assets/Flecha.png')} />
                </TouchableOpacity>
              </View>
            </View>
            {CPensionKind == true ? (
              <Text style={styles.ErrorM}>Este campo no puede estar vacío</Text>
            ) : null}
          </View>
          <View style={styles.buttonContainer2}>
            <TouchableOpacity style={styles.viewData} onPress={handleSubmit} title="Submit" disabled={isSubmitting ? true: false}>          
              {
              isSubmitting 
                ? <ActivityIndicator color='#fff' />
                : <Text style={styles.loginText}>CONTINUAR</Text>
              }
            </TouchableOpacity>
          </View>
          </>
          )}
        </Formik>
      {/* <View style={styles.buttonContainer2}>
        <TouchableOpacity
          style={styles.viewData}
          onPress={() => CheckTextInput()}>
          <Text style={styles.loginText}>CONTINUAR</Text>
        </TouchableOpacity>
      </View> */}
    </ScrollView>
  );
};

LaboralesForm.navigationOptions = {
  headerShown: false,
};

const styles = StyleSheet.create({
  spinnerTextStyle: {
    color: '#D32345',
  },
  container: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: '#333333',
  },
  logo: {
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    marginTop: 40,
    marginBottom: 20,
    marginLeft: 10,
  },
  backContainer: {
    flexDirection: 'column',
    marginBottom: 20,
  },
  head: {
    backgroundColor: '#000',
    borderRadius: 20,
  },
  back: {
    marginTop: 2,
    flexDirection: 'column',
    color: 'white',
  },
  textContainer: {
    flexDirection: 'row',
    marginTop: 30,
    marginBottom: 10,
    marginLeft: 10,
  },
  textIniciar: {
    color: 'white',
    width: 300,
    fontSize: 20,
    fontWeight: 'bold',
    textAlign: 'left',
    marginLeft: 25,
  },
  textInit: {
    color: '#b3b4b5',
    textAlign: 'left',
    marginLeft: 25,
  },
  textInit: {
    color: '#b3b4b5',
    textAlign: 'left',
    marginLeft: 25,
  },
  buttonContainer2: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  logout: {
    marginTop: 20,
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 20,
    width: 142,
    borderRadius: 30,
    height: 45,
    backgroundColor: '#911830',
  },
  buttonContainer: {
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  viewData: {
    marginTop: 20,
    marginLeft: 20,
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 20,
    width: 142,
    borderRadius: 30,
    height: 45,
    backgroundColor: '#D32345',
  },
  loginText: {
    color: 'white',
    fontWeight: 'bold',
  },
  inputContainer: {
    backgroundColor: '#5e5e5e',
    borderRadius: 30,
    width: 300,
    height: 45,
    marginBottom: 20,
    flexDirection: 'row',
    alignItems: 'center',
  },

  SinputContainer: {
    backgroundColor: '#5e5e5e',
    borderRadius: 30,
    width: 145,
    height: 45,
    marginBottom: 20,
    flexDirection: 'row',
    alignItems: 'center',
  },
  SinputContainer2: {
    backgroundColor: '#5e5e5e',
    borderRadius: 30,
    width: 145,
    height: 45,
    marginBottom: 20,
    flexDirection: 'row',
    alignItems: 'center',
    marginLeft: 10,
  },
  inputs: {
    height: 45,
    marginLeft: 16,
    color: 'white',
    flex: 1,
  },
  ocointainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  pick: {
    backgroundColor: '#696969',
    color: 'white',
    height: 40,
    fontSize: 20,
    borderRadius: 50,
    flexBasis: '88%',
    marginLeft: 15,
  },
  inputIcon: {
    marginRight: 15,
    justifyContent: 'center',
    height: 10,
    width: 10,
  },
  ErrorM: {
    color: '#D32345',
    fontWeight: 'bold',
    marginBottom: 15,
  },
  onePickerItem: {
    height: 44,
    color: 'red',
  },
});

export default LaboralesForm;

/*
<TouchableOpacity 
style={ styles.OUB}  
onPress={() => getInst()}
>  
<Text style={styles.loginText}>Obtener Ubicacion</Text>
</TouchableOpacity>

            <TouchableOpacity style={ styles.logout}   onPress={() => onSubmit({ vSalary, vPensionInstitute, vBank, vPensionKind })}>
                <Text style={styles.loginText}>GUARDAR</Text>
            </TouchableOpacity>



            
  const getInst = () => {
    ForceApi.post(`/GetPensionInstituteController.php`)
      .then(res => {
        setpPensionInstitute(res.data.pension);
    })
  }*/

/* const getBank = ({vPensionInstitute}) => {
    ForceApi.post(`/GetBanksController.php`,{vPensionInstitute})
      .then(res => {
        setpBank(res.data.banks);
    })
  }*/

/*
  const getKind = () => {
    ForceApi.post(`/GetKindPensionController.php`)
      .then(res => {
        setpPensionKind(res.data.pension);
    })
  }*/
