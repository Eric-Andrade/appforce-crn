import {useState} from 'react';

export const useSpinner = (initialMode = false) => {
  const [spinnerOpen, setspinnerOpen] = useState(initialMode);
  const toggle = () => setspinnerOpen(!spinnerOpen);
  return [spinnerOpen, setspinnerOpen, toggle];
};

export const useSpinnerWithData = (
  initialMode = false,
  initialSelected = null,
) => {
  const [spinnerOpen, setspinnerOpen] = useSpinner(initialMode);
  const [selected, setSelected] = useState(initialSelected);
  const setSpinnerState = state => {
    setspinnerOpen(state);
    if (state === false) {
      setSelected(null);
    }
  };
  return {
    spinnerOpen,
    setspinnerOpen,
    selected,
    setSelected,
    setSpinnerState,
    toggle,
  };
};
