import React, {useState, useEffect } from 'react';
import {Image, View, NativeModules, Platform, SafeAreaView, Button } from 'react-native'

const MitekFacialID = () => {
    
  const [ imageSelfie, setimageSelfie ] = useState()

  //Inicia los sdk
  const blessStart = () => {
    if(Platform.OS ==='android'){
      
      //Esta es una promesa que regresa la iamgen en base64
      NativeModules.Facial.startFacial().then(res => {
          // console.log(`data:image/jpeg;base64,${res}`)
          let value = JSON.parse(res)
          let imageLoad = value.image
          setimageSelfie(imageLoad)
          console.log('ress',value)
      }).catch(err => console.log('err',err));
    } else {
      //esto es apenas la promesa para poder utilizarlo en IOS
      var onFacial = NativeModules.Facial;
      onFacial.startFacial('test ', 'Test conexion');
    }
  }
  
    return (
        <SafeAreaView style={{flex:1,justifyContent:'center',alignItems:'center'}}>
          {
            imageSelfie
            ? <Image source={{uri:imageSelfie}} style={{width:100,height:100}}/>
            : null
          }
            <Button onPress={blessStart} title="Iniciar Facial"/>
        </SafeAreaView>
    );
}

export default MitekFacialID