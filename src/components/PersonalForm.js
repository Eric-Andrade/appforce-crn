import React, {useContext, useState, useEffect} from 'react';
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableOpacity,
  ScrollView,
  ActivityIndicator,
  TouchableWithoutFeedback,
  Button
} from 'react-native';

import DateTimePicker from '@react-native-community/datetimepicker';
import {CheckBox} from 'react-native-elements';
import DatePicker from 'react-native-datepicker';
import ForceApi from '../api/force';
import {useModal} from './useModal';
import InfoModal from './InfoModal';
import Spinner from 'react-native-loading-spinner-overlay';
import { Formik } from 'formik';
import * as yup from 'yup';

const validationSchema = yup.object().shape({
  fName: yup
    .string()
    .matches(/^[A-Za-zÑñ ]*$/, 'Ingrese únicamente letras sin tildes')
    .required('Este campo no puede estar vacío')
    .uppercase(),
  fSecondName: yup
    .string()
    .matches(/^[A-Za-zÑñ ]*$/, 'Ingrese únicamente letras sin tildes')
    // .required('Este campo no puede estar vacío')
    .uppercase(),
  fLastName: yup
    .string()
    .matches(/^[A-Za-zÑñ ]*$/, 'Ingrese únicamente letras sin tildes')
    // .required('Este campo no puede estar vacío')
    .uppercase(),
  fSecondLastName: yup
    .string()
    .matches(/^[A-Za-zÑñ ]*$/, 'Ingrese únicamente letras sin tildes')
    // .required('Este campo no puede estar vacío')
    .uppercase(),
  // fCellphone: yup
  //   .string()
  //   .matches(/^[0-9]*$/, 'Ingrese únicamente números')
  //   .min(10, 'Celular debe tener 10 dígitos')
  //   .max(10, 'Celular debe tener 10 dígitos')
  //   .typeError('Ingrese únicamente números')
  //   .required('Este campo no puede estar vacío'),
  fPhone: yup
      .string()
      .matches(/^[0-9]*$/, 'Ingrese únicamente números')
      .min(10, 'Teléfono debe tener 10 dígitos')
      .max(10, 'Teléfono debe tener 10 dígitos')
      .typeError('Ingrese únicamente números'),
      // .required('Este campo no puede estar vacío'),
  fEmail: yup
      .string()
      // .matches(/^[A-Za-z0-9 ]*$/, 'No ingrese caracteres especiales ni con tildes')
      .email('Ingrese un email válido'),
})

const PersonalForm = ({state, errorMessage, onSubmit}) => {
  const [itemModalOpen, setItemModalOpen, toggleModal] = useModal();
  const [vName, setvName] = useState('');
  const [vSecondName, setvSecondName] = useState('');
  const [vLastName, setvLastName] = useState('');
  const [vSecondLastName, setvSecondLastName] = useState('');
  const [vCellphone, setvCellphone] = useState('');
  const [vPhone, setvPhone] = useState('');
  const [vBirthDate, setvBirthDate] = useState('Fecha de nacimiento');
  // const [vBirthDate, setvBirthDate] = useState('');
  const [vRFC, setvRFC] = useState('');
  const [vGender, setvGender] = useState(0);
  const [vEmail, setvEmail] = useState('');
  const [vValidCellphone, setvValidCellphone] = useState(true);

  const [CName, setCName] = useState(false);
  const [CLastName, setCLastName] = useState(false);
  const [CSecondLastName, setCSecondLastName] = useState(false);
  const [CCellphone, setCCellphone] = useState(false);
  const [CBirthDate, setCBirthDate] = useState(false);
  const [CEmail, setCEmail] = useState(false);

  const [Visible, setVisible] = useState(false);
  const [isLoading, setisLoading] = useState(false);
  const [stateError, setstateError] = useState(state.error);
  const [MessagePhone, setMessagePhone] = useState('');

  /* Datepicker */
  const [date, setDate] = useState();
  // const [date, setDate] = useState(new Date(1018106662000));
  let datetime = new Date();
  let year = datetime.getFullYear();
  let month = datetime.getMonth();;
  let day = datetime.getDate();
  
  useEffect(() => {
    const today = new Date();
    let yearconvertedewe = today.getFullYear();
    let monthconvertedewe = ('0' + (today.getMonth() + 1)).slice(-2)
    let dayconvertedewe = Platform.OS === 'ios' ? today.getDate() - 1 : today.getDate()
    const eighteenyearsago = toTimestamp(yearconvertedewe - 18, monthconvertedewe, dayconvertedewe + 1, 0,0,0)
    console.log(eighteenyearsago);
    
    setDate(new Date(eighteenyearsago))
  },[])
  const [mode, setMode] = useState('date');
  const [show, setShow] = useState(false);

  const onChange = (event, selectedDate) => {
    const currentDate = selectedDate || date;
    setShow(Platform.OS === 'ios');
    setDate(currentDate);
    // console.log(currentDate);

    let yearconverted = currentDate.getFullYear();
    let monthconverted = ('0' + (currentDate.getMonth() + 1)).slice(-2)
    let dayconverted = Platform.OS === 'ios' ? ('0' + (currentDate.getDate() + 1)).slice(-2) : ('0' + (currentDate.getDate())).slice(-2);
    // let dayconverted = currentDate.getDate()
    var _completeDate = `${dayconverted}/${monthconverted}/${yearconverted}`;
    setvBirthDate(_completeDate)
  };

  const showMode = currentMode => {
    setShow(true);
    setMode(currentMode);
  };

  const showDatepicker = () => {
    showMode('date');
  };

  const toTimestamp = (year,month,day,hour,minute,second) => {
    var datum = new Date(Date.UTC(year,month-1,day,hour,minute,second));
    
    console.log(datum.getTime());
    
    return datum.getTime();
   }
  /* /Datepicker*/

  useEffect(() => {
    if (isLoading != false) {
      setInterval(() => {
        // setVisible(false);
        setisLoading(false);
      }, 3000);
    }
  }, [isLoading]);

  CheckTextInput = () => {
    setVisible(false);
    if (vName != '' && vCellphone != '' && vBirthDate != '') {
      console.log('CheckTextInput clicked');
      setCName(false);
      setCLastName(false);
      setCSecondLastName(false);
      setCCellphone(false);
      setCBirthDate(false);
      setCEmail(false);
      onSubmit({
        vName,
        vSecondName,
        vLastName,
        vSecondLastName,
        vCellphone,
        vPhone,
        vBirthDate,
        vRFC,
        vGender,
        vEmail,
      });
      setVisible(true);
      setisLoading(false);
      toggleModal(true);
    }
    if (vName != '') {
      setCName(false);
    } else {
      setCName(true);
    }

    if (vCellphone != '') {
      setCCellphone(false);
    } else {
      setCCellphone(true);
    }

    if (vBirthDate != '') {
      setCBirthDate(false);
    } else {
      setCBirthDate(true);
    }

    if (vEmail != '') {
      setCEmail(false);
    } else {
      setCEmail(true);
    }
  };

  async function getRFC({
    vLastName,
    vSecondLastName,
    vName,
    vSecondName,
    vBirthDate,
  }) {
    const {data} = await ForceApi.post(`/GetRfcController.php`, {
      vName,
      vSecondName,
      vLastName,
      vSecondLastName,
      vBirthDate,
    });
    setvRFC(data.resultRFC);
    console.log(data.resultRFC);
  }

  async function getRFC2({vLastName, vName, vSecondName, vBirthDate}) {
    const {data} = await ForceApi.post(`/GetRfcController.php`, {
      vName,
      vSecondName,
      vLastName,
      vBirthDate,
    });
    setvRFC(data.resultRFC);
    console.log(data.resultRFC);
  }

  async function getRFC3({vSecondLastName, vName, vSecondName, vBirthDate}) {
    const {data} = await ForceApi.post(`/GetRfcController.php`, {
      vName,
      vSecondName,
      vSecondLastName,
      vBirthDate,
    });
    setvRFC(data.resultRFC);
    console.log(data.resultRFC);
  }

  useEffect(() => {
    if ((vLastName || vSecondLastName) && vName && vBirthDate) {
      console.log('getRFC we');
      
      getRFC({vLastName, vSecondLastName, vName, vSecondName, vBirthDate});
    }
  }, [vLastName, vSecondLastName, vName, vSecondName, vBirthDate]);

  useEffect(() => {
    if (vCellphone && vRFC) {
      getValidation({vCellphone, vRFC});
    }
  }, [vCellphone, vRFC]);

  async function getValidation({vCellphone, vRFC}) {
    ForceApi.post(`/ValidateCellphoneController.php`, {vCellphone, vRFC}).then(
      res => {
        console.log('getValidation');
        console.log(res.data);
        setvValidCellphone(res.data.validCellphone);
        console.log(res.data.validCellphone);
        if (res.data.validCellphone) {
          setMessagePhone('');
        } else {
          setCCellphone(false);
          setMessagePhone(res.data.message);
        }
      },
    );
  }

  useEffect(() => {
    let isCancelled = false;
    if (Visible && state.id != null) {
      setVisible(!Visible);
      setvName('');
      setvSecondName('');
      setvLastName('');
      setvSecondLastName('');
      setvCellphone('');
      setvBirthDate('');
      setvEmail('');
      setvRFC('')
    }
    else if (state.continue === false) {
    }
    else {
      setVisible(false);
    }
    return () => {
      isCancelled = true;
    };
  }, [state]);

  if (Visible) {
    return (
      <Spinner
        size="large"
        visible={Visible}
        color="#D32345"
        textContent={'Un momento por favor'}
        textStyle={styles.spinnerTextStyle}
      />
    );
  }

  return (
    <ScrollView>
      {state.continue !== true ? (
        <InfoModal
          text={`${state.errorMessage}`}
          isActive={itemModalOpen}
          handleClose={() => setItemModalOpen(false)}
        />
      ) : null}
      <Formik
        enableReinitialize={true}
        initialValues={{ 
          fName: '',
          fSecondName: '',
          fLastName: '',
          fSecondLastName: '',
          fPhone: '',
          fEmail: '',
        }}
        onSubmit={(values, actions) => {
        // alert(JSON.stringify(values))
        const uName = values.fName;
        const uSecondName = values.fSecondName;
        const uLastName = values.fLastName;
        const uSecondLastName = values.fSecondLastName;
        const uEmail = values.fEmail;

        setvName(uName.toUpperCase());
        setvSecondName(uSecondName.toUpperCase());
        setvLastName(uLastName.toUpperCase());
        setvSecondLastName(uSecondLastName.toUpperCase());
        // setvCellphone(values.fCellphone);
        setvPhone(values.fPhone);
        setvEmail(uEmail.toUpperCase());
        CheckTextInput()
        setTimeout(() => {
            actions.setSubmitting(false)
        }, 2000);
        }}
        validationSchema={validationSchema}
        >
        {({ handleChange, handleBlur, handleSubmit, values, isSubmitting, errors }) => (
          <View style={styles.buttonContainer}>
            <View style={styles.inputContainer}>
              <TextInput
                style={styles.inputs}
                placeholder="Nombre"
                placeholderTextColor="white"
                underlineColorAndroid="transparent"
                // onChangeText={newvName => setvName(newvName.toUpperCase())}
                // value={vName}
                autoCorrect={false}
                autoCapitalize="characters"
                
                onChangeText={() => setvName(values.fName), handleChange('fName')}
                onBlur={() => setvName(values.fName)}
                value={values.fName}
              />
            </View>
            {
              errors.fName
              ? <Text style={styles.ErrorM}>{errors.fName}</Text>
              : null
            }
            {/* {CName == true ? (
              <Text style={styles.ErrorM}>Este campo no puede estar vacío</Text>
            ) : null} */}
            <View style={styles.inputContainer}>
              <TextInput
                style={styles.inputs}
                placeholder="Segundo nombre"
                placeholderTextColor="white"
                underlineColorAndroid="transparent"
                // onChangeText={newvSecondName =>
                //   setvSecondName(newvSecondName.toUpperCase())
                // }
                // value={vSecondName}
                autoCorrect={false}
                autoCapitalize="characters"
                
                onChangeText={() => setvSecondName(values.fSecondName), handleChange('fSecondName')}
                onBlur={() => setvSecondName(values.fSecondName)}
                value={values.fSecondName}
              />
            </View>
            {
              errors.fSecondName
              ? <Text style={styles.ErrorM}>{errors.fSecondName}</Text>
              : null
            }
            <View style={styles.inputContainer}>
              <TextInput
                style={styles.inputs}
                placeholder="Apellido paterno"
                placeholderTextColor="white"
                underlineColorAndroid="transparent"
                // onChangeText={newvLastName =>
                //   setvLastName(newvLastName.toUpperCase())
                // }
                // value={vLastName}
                autoCorrect={false}
                autoCapitalize="characters"
                
                onChangeText={() => setvLastName(values.fLastName), handleChange('fLastName')}
                onBlur={() => setvLastName(values.fLastName)}
                value={values.fLastName}
              />
            </View>
            {
              errors.fLastName
              ? <Text style={styles.ErrorM}>{errors.fLastName}</Text>
              : null
            }
            {/* {CLastName == true ? (
              <Text style={styles.ErrorM}>Este campo no puede estar vacío</Text>
            ) : null} */}
            <View style={styles.inputContainer}>
              <TextInput
                style={styles.inputs}
                placeholder="Apellido materno"
                placeholderTextColor="white"
                // onChangeText={newvSecondLastName =>
                //   setvSecondLastName(newvSecondLastName.toUpperCase())
                // }
                // value={vSecondLastName}
                underlineColorAndroid="transparent"
                autoCorrect={false}
                autoCapitalize="characters"
                
                onChangeText={() => setvSecondLastName(values.fSecondLastName), handleChange('fSecondLastName')}
                onBlur={() => setvSecondLastName(values.fSecondLastName)}
                value={values.fSecondLastName}
              />
            </View>
            {
              errors.fSecondLastName
              ? <Text style={styles.ErrorM}>{errors.fSecondLastName}</Text>
              : null
            }
            {CSecondLastName == true ? (
              <Text style={styles.ErrorM}>Este campo no puede estar vacío</Text>
            ) : null} 

          <View style={{
            flex: 1,
            flexDirection: 'column',
            display: 'flex',
            // justifyContent: 'center',
            // alignItems: 'center'
          }}>
            <View style={{
              backgroundColor: '#5e5e5e',
              borderRadius: 30,
              width: 300,
              height: 45,
              marginBottom: 20,
              flexDirection: 'row',
              // justifyContent: 'center',
              alignItems: 'center',
            }}>
              <TouchableWithoutFeedback onPress={showDatepicker}>
                <Text style={{ color: 'white', marginLeft: 20}}>{vBirthDate}</Text>
              </TouchableWithoutFeedback>
            </View>
            {show && (
              <DateTimePicker
                testID='dateTimePicker' 
                timeZoneOffsetInMinutes={0}
                value={date}
                mode={mode}
                is24Hour={true}
                display='spinner'
                onChange={onChange}
                style={styles.iOsPicker}
                maximumDate={new Date(year-18, month, day+1)}
              />
            )}
          </View>

            {/* <View style={styles.containerdate}>
              <DatePicker
                date={vBirthDate} //initial date from state
                mode="date" //The enum of date, datetime and time
                placeholder="Seleccione Fecha"
                placeholderTextColor="white"
                format="DD/MM/YYYY"
                minDate="01/01/1900"
                maxDate={`01/01/${year - 18}`}
                confirmBtnText="Confirm"
                cancelBtnText="Cancel"
                androidMode="spinner"
                customStyles={{
                  placeholderText: {
                    fontSize: 16,
                  },
                  dateIcon: {
                    height: 0,
                    width: 0,
                  },
                  dateText: {
                    color: '#b3b4b5',
                    fontSize: 16,
                  },
                  dateInput: {
                    borderWidth: 0,
                  },
                }}
                onDateChange={date => {
                  setvBirthDate(date);
                }}
              />
            </View>
            {CBirthDate == true ? (
              <Text style={styles.ErrorM}>Este campo no puede estar vacío</Text>
            ) : null} */}
            <View style={styles.inputContainer}>
              <TextInput
                style={styles.inputs}
                placeholder={'RFC'}
                placeholderTextColor="white"
                underlineColorAndroid="transparent"
                value={vRFC}
                editable={false}
                selectTextOnFocus={false}
                autoCorrect={false}
                autoCapitalize="characters"
              />
            </View>
            <View style={styles.inputContainer}>
              <TextInput
                style={styles.inputs}
                placeholder="Teléfono celular"
                placeholderTextColor="white"
                underlineColorAndroid="transparent"
                onChangeText={newvCellphone => { setvCellphone(newvCellphone.toUpperCase())}
                }
                // value={vCellphone}
                autoCorrect={false}
                autoCapitalize="characters"
                keyboardType={'numeric'}
                maxLength={10}
                
                // onChangeText={handleChange('fCellphone'), setvCellphone(values.fCellphone)}
                onBlur={handleBlur('fCellphone')}
                // value={values.fCellphone}
              />
            </View>
            {/* {
              errors.fCellphone
              ? <Text style={styles.ErrorM}>{errors.fCellphone}</Text>
              : null
            } */}
            {CCellphone == true ? (
              <Text style={styles.ErrorM}>Este campo no puede estar vacío</Text>
            ) : null}
            {MessagePhone !== '' ? (
              <Text style={styles.ErrorM}>{MessagePhone}</Text>
            ) : null}
            <View style={styles.inputContainer}>
              <TextInput
                style={styles.inputs}
                placeholder="Teléfono fijo"
                placeholderTextColor="white"
                // onChangeText={newvPhone => setvPhone(newvPhone.toUpperCase())}
                // value={vPhone}
                underlineColorAndroid="transparent"
                autoCorrect={false}
                keyboardType={'numeric'}
                maxLength={10}
                
                onChangeText={handleChange('fPhone')}
                onBlur={handleBlur('fPhone')}
                value={values.fPhone}
              />
            </View> 
            {
              errors.fPhone
              ? <Text style={styles.ErrorM}>{errors.fPhone}</Text>
              : null
            }
            <View style={styles.checkbox}>
              <CheckBox
                containerStyle={styles.checkbox}
                textStyle={styles.checkboxTxt}
                uncheckedColor={'#b3b4b5'}
                checkedColor={'#911830'}
                title="Masculino"
                checkedIcon="stop"
                checked={vGender === 0 ? true : false}
                onPress={() => setvGender(0)}
              />
              <CheckBox
                containerStyle={styles.checkbox}
                textStyle={styles.checkboxTxt}
                uncheckedColor={'#b3b4b5'}
                checkedColor={'#911830'}
                title="Femenino"
                checkedIcon="stop"
                checked={vGender === 1 ? true : false}
                onPress={() => setvGender(1)}
              />
            </View>

            <View style={styles.inputContainer}>
              <TextInput
                style={styles.inputs}
                placeholder="Correo electrónico"
                placeholderTextColor="white"
                underlineColorAndroid="transparent"
                // onChangeText={newvEmail => setvEmail(newvEmail)}
                // value={vEmail}
                autoCorrect={false}
                keyboardType='email-address'
                
                onChangeText={handleChange('fEmail')}
                onBlur={handleBlur('fEmail')}
                value={values.fEmail}
              />
            </View>
            {
              errors.fEmail
              ? <Text style={styles.ErrorM}>{errors.fEmail}</Text>
              : null
            }
            <View style={styles.buttonContainer2}>
              <TouchableOpacity
                style={vValidCellphone ? styles.viewData : styles.viewDataLocked}
                onPress={handleSubmit}
                disabled={vValidCellphone ? false : true}>
                {
                  isSubmitting 
                  ? <ActivityIndicator color='#fff' />
                  : <Text style={styles.loginText}>CONTINUAR</Text>
                }
              </TouchableOpacity>
            </View>
          </View>
        )}
      </Formik>
    </ScrollView>
  );
};

PersonalForm.navigationOptions = {
  headerShown: false,
};

const styles = StyleSheet.create({
  spinnerTextStyle: {
    color: '#D32345',
  },
  containerdate: {
    backgroundColor: '#5e5e5e',
    borderRadius: 30,
    width: 300,
    height: 45,
    marginBottom: 20,
    flexDirection: 'row',
  },
  checkbox: {
    flexDirection: 'row',
    backgroundColor: '#333333',
    borderWidth: 0,
    marginBottom: 10,
  },
  checkboxTxt: {
    color: '#b3b4b5',
  },
  buttonContainer2: {
    marginTop: 10,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttonContainer: {
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  inputContainer: {
    backgroundColor: '#5e5e5e',
    borderRadius: 30,
    width: 300,
    height: 45,
    marginBottom: 20,
    flexDirection: 'row',
    alignItems: 'center',
  },
  inputs: {
    height: 45,
    marginLeft: 16,
    color: 'white',
    flex: 1,
  },
  logout: {
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 20,
    width: 142,
    borderRadius: 30,
    height: 45,
    backgroundColor: '#911830',
  },
  loginText: {
    color: 'white',
    fontWeight: 'bold',
  },
  viewData: {
    marginLeft: 20,
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 20,
    width: 142,
    borderRadius: 30,
    height: 45,
    backgroundColor: '#D32345',
  },
  viewDataLocked: {
    marginLeft: 20,
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 20,
    width: 142,
    borderRadius: 30,
    height: 45,
    backgroundColor: '#911830',
  },
  ErrorM: {
    color: '#D32345',
    fontWeight: 'bold',
    marginBottom: 15,
  },
  iOsPicker: {
    flex: 1,
  },
});

export default PersonalForm;