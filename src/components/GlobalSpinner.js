import React, {Component} from 'react';
import {StyleSheet, View, Modal, ActivityIndicator, Text} from 'react-native';

export default function GlobalSpinner() {
  return (
    <View style={styles.preloader}>
      <ActivityIndicator size="large" color="#D32345" />
      <Text>Loading...</Text>
    </View>
  );
}
const styles = StyleSheet.create({
  preloader: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
});
