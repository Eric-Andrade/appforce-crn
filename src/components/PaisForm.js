import React, {useState, useEffect} from 'react';
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableOpacity,
  Image,
  ScrollView,
  Picker,
  Platform
} from 'react-native';
import PickerModal from 'react-native-picker-modal-view';
import Ionicons from 'react-native-vector-icons/Ionicons';
import RNPickerSelect from 'react-native-picker-select';
import AsyncStorage from '@react-native-community/async-storage';
import ForceApi from '../api/force';
import Spinner from 'react-native-loading-spinner-overlay';
import mexstates from '../api/mexstates.json';

const PaisForm = ({state, errorMessage, onSubmit}) => {
  console.log('PaisForm state: ', state);
  const [vCURP, setvCURP] = useState();
  const [vResidenceTime, setvResidenceTime] = useState(0);
  const [vCivilStatus, setvCivilStatus] = useState(0);

  const [pBirthState, setpBirthState] = useState([]);
  const [vBirthState, setvBirthState] = useState();
  const [vBirthStateName, setvBirthStateName] = useState();

  const [CBirthState, setCBirthState] = useState(false);
  const [CCivilStatus, setCCivilStatus] = useState(false);
  const [CResidenceTime, setCResidenceTime] = useState(false);
  const [CCURP, setCCURP] = useState(false);

  const [Visible, setVisible] = useState(false);
  const [isLoading, setisLoading] = useState(false);
  const [ vestados, setvestados ] = useState([])

  useEffect(() => {
    setvestados(mexstates.estados);
    if (isLoading != false) {
      setInterval(() => {
        setisLoading(false);
      }, 5000);
    }
  }, [isLoading]);

  CheckTextInput = () => {
    if (
      vBirthState != '' &&
      vCivilStatus != 0 &&
      vResidenceTime != 0  &&
      vCURP !== ''
    ) {
      console.log(vBirthState);
      setisLoading(true);
      setVisible(true);
      setCBirthState(false);
      setCCivilStatus(false);
      setCResidenceTime(false);
      setCCURP(false);
      onSubmit({vBirthState, vCivilStatus, vResidenceTime, vCURP});
      // onSubmit({ vBirthState, vCivilStatus, vResidenceTime, vCURP})
    }
    if (vBirthState != '') {
      setCBirthState(false);
    } else {
      setCBirthState(true);
    }

    if (vCivilStatus != 0) {
      setCCivilStatus(false);
    } else {
      setCCivilStatus(true);
    }

    if (vCURP != '') {
      setCCURP(false);
    } else {
      setCCURP(true);
    }

    if (vResidenceTime != 0) {
      setCResidenceTime(false);
    } else {
      setCResidenceTime(true);
    }
  };

  async function getCURP({vBirthState}) {
    const vEntity = vBirthState;
    //const vSolicitudeId = 4;
    const vSolicitudeId = await AsyncStorage.getItem('SolicitudeId');
    const {data} = await ForceApi.post(`/GetCURPController.php`, {
      vSolicitudeId,
      vEntity,
    });
    setvCURP(data.curp);
    console.log(data.curp);
  }

  useEffect(() => {
    // if (vBirthState != 'AAA') {
      console.log('entro');
      getCURP({vBirthState});
    // }
  }, [vBirthState]);

  /* const getTime = () => {
    ForceApi.post(`/GetEntitiesController.php`,)
        .then(res => {
            console.log(res.data.entities);
          setpBirthState(res.data.entities);
        })
    }

    useEffect(() => {
        getTime();
    }, []);*/

  // useEffect(() => {
  //   let isCancelled = false;
  //   setVisible(false);
  //   if (!state.error) {
  //     setVisible(false);
  //   }

  //   return () => {
  //     setVisible(false);
  //     isCancelled = true;
  //   };
  // }, [state.error]);

  useEffect(() => {
    let isCancelled = false;
    if (Visible && state.errorMessage != '') {
      setVisible(!Visible);
    } else {
      setVisible(false);
    }
    return () => {
      isCancelled = true;
    };
  }, [state]);

  if (Visible) {
    return (
      <Spinner
        size="large"
        visible={Visible}
        color="#D32345"
        textContent={'Un momento por favor'}
        textStyle={styles.spinnerTextStyle}
      />
    );
  }

  return (
    <ScrollView>
      {/* <Spinner 
        size="large"
        visible={isLoading}
        color ='#D32345'
        textContent={'Cargando Datos'}
        textStyle={styles.spinnerTextStyle}
        /> */}
      <View style={styles.buttonContainer}>
        <View style={styles.inputContainer}>
          <TextInput
            style={styles.inputs}
            placeholder="MEXICO"
            placeholderTextColor="white"
            underlineColorAndroid="transparent"
            //value={vStreet}
            autoCorrect={false}
            editable={false}
            selectTextOnFocus={false}
          />
        </View>

        <View style={styles.ocointainer}>
          <View style={styles.inputContainer}>
            <RNPickerSelect
              placeholder={{
                label: 'Seleccione el estado de nacimiento',
                value: null,
                color: '#9EA0A4'
              }}
              style={{
                placeholder: {
                  flexDirection: 'column',
                  justifyContent: 'center',
                  alignItems: 'center',
                  color: 'white',
                  marginLeft: 15
                },
                inputAndroid: {
                  color: 'white',
                  width: 270,
                  height: 40,
                  flexDirection: 'column',
                  justifyContent: 'center',
                  alignItems: 'center',
                  color: 'white',
                  marginLeft: 15
                },
                inputIOS: {
                  color: 'white',
                  width: 270,
                  height: 40,
                  flexDirection: 'column',
                  justifyContent: 'center',
                  alignItems: 'center',
                  color: 'white',
                  marginLeft: 15,
                  marginTop: 15
                },
                iconContainer: {
                  top: 10,
                  right: 15,
                }
              }}
              useNativeAndroidPickerStyle={false}
              Icon={() => {
                return <Ionicons name='md-arrow-dropdown' size={20} color='#FFF' />
              }}
              onValueChange={newvBirthState => setvBirthState(newvBirthState)}
              items={vestados}
              />
            {/* <PickerModal
              renderSelectView={(disabled, selected, showModal) =>
                <TouchableOpacity disabled={disabled} onPress={showModal} style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', marginHorizontal: 15}}>
                  <Text style={{ color: 'white'}}>{ vBirthStateName ? vBirthStateName : 'Estado de nacimiento' }</Text>
                  <Image source={require('../../assets/Flecha.png')} />
                </TouchableOpacity>
              }
              onSelected={vBirthState => { setvBirthState(vBirthState.Value); setvBirthStateName(vBirthState.Name)}}
              items={vestados}
              sortingLanguage={'tr'}
              showToTopButton={true}
              selected={vBirthState}
              autoGenerateAlphabeticalIndex={true}
              selectPlaceholderText={'Estado de nacimiento'}
              onEndReached={() => console.log('list ended...')}
              searchPlaceholderText={'Search...'}
              requireSelection={false}
              autoSort={false}
            /> */}
            {/* <Picker
              style={styles.pick}
              // mode='dropdown'
              itemStyle={styles.onePickerItem}
              selectedValue={vBirthState}
              onValueChange={newvBirthState => setvBirthState(newvBirthState)}>
              <Picker.Item label="Estado de nacimiento" value="AAA" />
              <Picker.Item label="AGUASCALIENTES" value="AS" />
              <Picker.Item label="BAJA CALIFORNIA" value="BC" />
              <Picker.Item label="BAJA CALIFORNIA SUR" value="BS" />
              <Picker.Item label="CAMPECHE" value="CC" />
              <Picker.Item label="CHIAPAS" value="CS" />
              <Picker.Item label="CHIHUAHUA" value="CH" />
              <Picker.Item label="COAHUILA DE ZARAGOZA" value="CL" />
              <Picker.Item label="COLIMA" value="CM" />
              <Picker.Item label="CIUDAD DE MEXICO" value="DF" />
              <Picker.Item label="DURANGO" value="DG" />
              <Picker.Item label="GUANAJUATO" value="GT" />
              <Picker.Item label="GUERRERO" value="GR" />
              <Picker.Item label="HIDALGO" value="HG" />
              <Picker.Item label="JALISCO" value="JC" />
              <Picker.Item label="MEXICO" value="MC" />
              <Picker.Item label="MICHOACÁN DE OCAMPO" value="MN" />
              <Picker.Item label="MORELOS" value="MS" />
              <Picker.Item label="NAYARIT" value="NT" />
              <Picker.Item label="NUEVO LEÓN" value="NL" />
              <Picker.Item label="OAXACA" value="OC" />
              <Picker.Item label="PUEBLA" value="PL" />
              <Picker.Item label="QUERÉTARO" value="QO" />
              <Picker.Item label="QUINTANA ROO" value="QR" />
              <Picker.Item label="SAN LUIS POTOSÍ" value="SP" />
              <Picker.Item label="SINALOA" value="SL" />
              <Picker.Item label="SONORA" value="SR" />
              <Picker.Item label="TABASCO" value="TC" />
              <Picker.Item label="TAMAULIPAS" value="TS" />
              <Picker.Item label="TLAXCALA" value="TL" />
              <Picker.Item label="VERACRUZ DE IGNACIO DE LA LLAVE" value="VZ" />
              <Picker.Item label="YUCATÁN" value="YN" />
              <Picker.Item label="ZACATECAS" value="ZS" />
            </Picker> */}
            {/* <TouchableOpacity style={styles.inputIcon}>
              <Image source={require('../../assets/Flecha.png')} />
            </TouchableOpacity> */}
          </View>
        </View>
        {CBirthState == true ? (
          <Text style={styles.ErrorM}>Este campo no puede estar vacío</Text>
        ) : null}
        <View style={styles.inputContainer}>
          <TextInput
            style={styles.inputs}
            placeholder="MEXICANA"
            placeholderTextColor="white"
            underlineColorAndroid="transparent"
            //value={vStreet}
            autoCorrect={false}
            editable={false}
            selectTextOnFocus={false}
          />
        </View>

        <View style={styles.ocointainer}>
          <View style={styles.inputContainer}>
            <RNPickerSelect
              placeholder={{
                label: 'Seleccione el estado civil',
                value: null,
                color: '#9EA0A4'
              }}
              style={{
                placeholder: {
                  flexDirection: 'column',
                  justifyContent: 'center',
                  alignItems: 'center',
                  color: 'white',
                  marginLeft: 15
                },
                inputAndroid: {
                  color: 'white',
                  width: 270,
                  height: 40,
                  flexDirection: 'column',
                  justifyContent: 'center',
                  alignItems: 'center',
                  color: 'white',
                  marginLeft: 15
                },
                inputIOS: {
                  color: 'white',
                  width: 270,
                  height: 40,
                  flexDirection: 'column',
                  justifyContent: 'center',
                  alignItems: 'center',
                  color: 'white',
                  marginLeft: 15,
                  marginTop: 15
                },
                iconContainer: {
                  top: 10,
                  right: 15,
                }
              }}
              useNativeAndroidPickerStyle={false}
              Icon={() => {
                return <Ionicons name='md-arrow-dropdown' size={20} color='#FFF' />
              }}
              onValueChange={newvCivilStatus => setvCivilStatus(newvCivilStatus)}
              items={[
                { label: 'Casado', value: '1' },
                { label: 'Divorciado', value: '2' },
                { label: 'Separado', value: '3' },
                { label: 'Soltero', value: '4' },
                { label: 'Union libre', value: '5' },
                { label: 'Viudo', value: '6' }
              ]}
              />
            {/* <Picker
              style={styles.pick}
              // mode='dropdown'
              itemStyle={styles.onePickerItem}
              selectedValue={vCivilStatus}
              placeholder="Estado civil"
              onValueChange={newvCivilStatus =>
                setvCivilStatus(newvCivilStatus)
              }>
              <Picker.Item label="Estado civil" value="0" />
              <Picker.Item label="Casado" value="1" />
              <Picker.Item label="Divorciado" value="2" />
              <Picker.Item label="Separado" value="3" />
              <Picker.Item label="Soltero" value="4" />
              <Picker.Item label="Union libre" value="5" />
              <Picker.Item label="Viudo" value="6" />
            </Picker>
            <TouchableOpacity style={styles.inputIcon}>
              <Image source={require('../../assets/Flecha.png')} />
            </TouchableOpacity> */}
          </View>
        </View>
        {CCivilStatus == true ? (
          <Text style={styles.ErrorM}>Este campo no puede estar vacío</Text>
        ) : null}

        <View style={styles.ocointainer}>
          <View style={styles.inputContainer}>
            <RNPickerSelect
              placeholder={{
                label: 'Tiempo de residencia',
                value: null,
                color: '#9EA0A4'
              }}
              style={{
                placeholder: {
                  flexDirection: 'column',
                  justifyContent: 'center',
                  alignItems: 'center',
                  color: 'white',
                  marginLeft: 15
                },
                inputAndroid: {
                  color: 'white',
                  width: 270,
                  height: 40,
                  flexDirection: 'column',
                  justifyContent: 'center',
                  alignItems: 'center',
                  color: 'white',
                  marginLeft: 15
                },
                inputIOS: {
                  color: 'white',
                  width: 270,
                  height: 40,
                  flexDirection: 'column',
                  justifyContent: 'center',
                  alignItems: 'center',
                  color: 'white',
                  marginLeft: 15,
                  marginTop: 15
                },
                iconContainer: {
                  top: 10,
                  right: 15,
                }
              }}
              useNativeAndroidPickerStyle={false}
              Icon={() => {
                return <Ionicons name='md-arrow-dropdown' size={20} color='#FFF' />
              }}
              onValueChange={newvResidenceTime => setvResidenceTime(newvResidenceTime)}
              items={[
                { label: "1 año", value: "1" }, 
                { label: "2 años", value: "2" }, 
                { label: "3 años", value: "3" }, 
                { label: "4 años", value: "4" }, 
                { label: "5 años", value: "5" }, 
                { label: "6 años", value: "6" }, 
                { label: "7 años", value: "7" }, 
                { label: "8 años", value: "8" }, 
                { label: "9 años", value: "9" }, 
                { label: "10 años", value: "10" }, 
                { label: "Mas de 10 años", value: "+10" }
              ]}
            />
            {/* <Picker
              style={styles.pick}
              // mode='dropdown'
              itemStyle={styles.onePickerItem}
              selectedValue={vResidenceTime}
              placeholder="Tiempo de residencia"
              onValueChange={newvResidenceTime =>
                setvResidenceTime(newvResidenceTime)
              }>
              <Picker.Item label="Tiempo de residencia" value="0" />
              <Picker.Item label="1 año" value="1" />
              <Picker.Item label="2 años" value="2" />
              <Picker.Item label="3 años" value="3" />
              <Picker.Item label="4 años" value="4" />
              <Picker.Item label="5 años" value="5" />
              <Picker.Item label="6 años" value="6" />
              <Picker.Item label="7 años" value="7" />
              <Picker.Item label="8 años" value="8" />
              <Picker.Item label="9 años" value="9" />
              <Picker.Item label="10 años" value="10" />
              <Picker.Item label="Mas de 10 años" value="+10" />
            </Picker> */}
            {/* <TouchableOpacity style={styles.inputIcon}>
              <Image source={require('../../assets/Flecha.png')} />
            </TouchableOpacity> */}
          </View>
        </View>

        {CResidenceTime == true ? (
          <Text style={styles.ErrorM}>Este campo no puede estar vacío</Text>
        ) : null}
        <View style={styles.inputContainer}>
          <TextInput
            style={styles.inputs}
            placeholderTextColor="white"
            placeholder={'CURP'}
            underlineColorAndroid="transparent"
            editable={false}
            value={vCURP}
            selectTextOnFocus={false}
            autoCorrect={false}
            autoCapitalize="characters"
          />
        </View>
        {CCURP == true ? (
          <Text style={styles.ErrorM}>Este campo no puede estar vacío</Text>
        ) : null}
      </View>
      <View style={styles.buttonContainer2}>
        <TouchableOpacity
          style={styles.lightOn}
          onPress={() => CheckTextInput()}>
          <Text style={styles.loginText}>CONTINUAR</Text>
        </TouchableOpacity>
      </View>
    </ScrollView>
  );
};

PaisForm.navigationOptions = {
  headerShown: false,
};

const styles = StyleSheet.create({
  spinnerTextStyle: {
    color: '#D32345',
  },
  container: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: '#333333',
  },
  logo: {
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    marginTop: 40,
    marginBottom: 20,
    marginLeft: 10,
  },
  backContainer: {
    flexDirection: 'column',
    marginBottom: 20,
  },
  head: {
    backgroundColor: '#000',
    borderRadius: 20,
  },
  back: {
    marginTop: 2,
    flexDirection: 'column',
    color: 'white',
  },
  textContainer: {
    flexDirection: 'row',
    marginTop: 30,
    marginBottom: 10,
    marginLeft: 10,
  },
  textIniciar: {
    color: 'white',
    width: 300,
    fontSize: 20,
    fontWeight: 'bold',
    textAlign: 'left',
    marginLeft: 25,
  },
  textInit: {
    color: '#b3b4b5',
    textAlign: 'left',
    marginLeft: 25,
  },
  textInit: {
    color: '#b3b4b5',
    textAlign: 'left',
    marginLeft: 25,
  },
  buttonContainer2: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  logout: {
    marginTop: 20,
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 20,
    width: 142,
    borderRadius: 30,
    height: 45,
    backgroundColor: '#911830',
  },
  buttonContainer: {
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  lightOn: {
    marginTop: 20,
    marginLeft: 20,
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 20,
    width: 142,
    borderRadius: 30,
    height: 45,
    backgroundColor: '#D32345',
  },
  OUB: {
    marginLeft: 20,
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 20,
    width: 142,
    borderRadius: 30,
    height: 45,
    backgroundColor: '#D32345',
  },
  loginText: {
    color: 'white',
    fontWeight: 'bold',
  },
  inputContainer: {
    backgroundColor: '#5e5e5e',
    borderRadius: 30,
    width: 300,
    height: 45,
    marginBottom: 20,
    flexDirection: 'row',
    alignItems: 'center',
  },

  SinputContainer: {
    backgroundColor: '#5e5e5e',
    borderRadius: 30,
    width: 145,
    height: 45,
    marginBottom: 20,
    flexDirection: 'row',
    alignItems: 'center',
  },

  SinputContainer2: {
    backgroundColor: '#5e5e5e',
    borderRadius: 30,
    width: 145,
    height: 45,
    marginBottom: 20,
    flexDirection: 'row',
    alignItems: 'center',
    marginLeft: 10,
  },
  inputs: {
    height: 45,
    marginLeft: 16,
    color: 'white',
    flex: 1,
  },
  ocointainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  pick: {
    backgroundColor: '#696969',
    color: 'white',
    height: 40,
    fontSize: 20,
    borderRadius: 50,
    flexBasis: '88%',
    marginLeft: 15,
  },
  inputIcon: {
    marginRight: 15,
    justifyContent: 'center',
    height: 10,
    width: 10,
  },
  ErrorM: {
    color: '#D32345',
    fontWeight: 'bold',
    marginBottom: 15,
  },
});

export default PaisForm;

/* <TouchableOpacity style={ styles.logout}  onPress={() => this.props.navigation.navigate('Logout'), console.log(vZipCode, vStreet, vExteriorNumber,
                 vInteriorNumber, vState, vCity, vResidenceTime)}>
              <Text style={styles.loginText}>GUARDAR</Text>
            </TouchableOpacity>*/
