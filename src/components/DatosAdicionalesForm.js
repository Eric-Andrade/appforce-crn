import React, {useContext, useState, useEffect} from 'react';
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableOpacity,
  ScrollView,
  Picker,
  Image,
  ActivityIndicator,
  Platform
} from 'react-native';
import RNPickerSelect from 'react-native-picker-select';
import Ionicons from 'react-native-vector-icons/Ionicons';
import Spinner from 'react-native-loading-spinner-overlay';
import {useModal} from '../components/useModal';
import InfoModal from '../components/InfoModal';
import { Formik } from 'formik';
import * as yup from 'yup';

const validationSchema = yup.object().shape({
  fFirstReferenceName: yup
                        .string()
                        .matches(/^[A-Za-zÑñ ]*$/, 'Ingrese únicamente letras sin tildes')
                        .required('Este campo no puede estar vacío')
                        .uppercase(),
  fFirstReferenceSecondName: yup
                        .string()
                        .matches(/^[A-Za-zÑñ ]*$/, 'Ingrese únicamente letras sin tildes')
                        // .required('Este campo no puede estar vacío')
                        .uppercase(),
  fFirstReferenceLastName: yup
                        .string()
                        .matches(/^[A-Za-zÑñ ]*$/, 'Ingrese únicamente letras sin tildes')
                        // .required('Este campo no puede estar vacío')
                        .uppercase(),
  fFirstReferenceSecondLastName: yup
                        .string()
                        .matches(/^[A-Za-zÑñ ]*$/, 'Ingrese únicamente letras sin tildes')
                        // .required('Este campo no puede estar vacío')
                        .uppercase(),
  fFirstReferenceCellphone: yup
                        .string()
                        .matches(/^[0-9]*$/, 'Ingrese únicamente números')
                        .min(10, 'Teléfono celular debe tener 10 dígitos')
                        .max(10, 'Teléfono celular debe tener 10 dígitos')
                        .typeError('Ingrese únicamente números')
                        .required('Este campo no puede estar vacío'),                    
  fFirstReferenceOccupation: yup
                        .string()
                        .matches(/^[A-Za-zÑñ ]*$/, 'Ingrese únicamente letras sin tildes')
                        .required('Este campo no puede estar vacío')
                        .uppercase(),
  
  fSecondReferenceName: yup
                        .string()
                        .matches(/^[A-Za-zÑñ ]*$/, 'Ingrese únicamente letras sin tildes')
                        .required('Este campo no puede estar vacío')
                        .uppercase(),
  fSecondReferenceSecondName: yup
                        .string()
                        .matches(/^[A-Za-zÑñ ]*$/, 'Ingrese únicamente letras sin tildes')
                        // .required('Este campo no puede estar vacío')
                        .uppercase(),
  fSecondReferenceLastName: yup
                        .string()
                        .matches(/^[A-Za-zÑñ ]*$/, 'Ingrese únicamente letras sin tildes')
                        // .required('Este campo no puede estar vacío')
                        .uppercase(),
  fSecondReferenceSecondLastName: yup
                        .string()
                        .matches(/^[A-Za-zÑñ ]*$/, 'Ingrese únicamente letras sin tildes')
                        // .required('Este campo no puede estar vacío')
                        .uppercase(),
  fSecondReferenceCellphone: yup
                        .string()
                        .matches(/^[0-9]*$/, 'Ingrese únicamente números')
                        .min(10, 'Teléfono celular debe tener 10 dígitos')
                        .max(10, 'Teléfono celular debe tener 10 dígitos')
                        .typeError('Ingrese únicamente números')
                        .required('Este campo no puede estar vacío'),
  fSecondReferenceOccupation: yup
                        .string()
                        .matches(/^[A-Za-zÑñ ]*$/, 'Ingrese únicamente letras sin tildes')
                        .required('Este campo no puede estar vacío')
                        .uppercase(),
})

const DatosAdicionalesForm = ({state, errorMessage, onSubmit}) => {
  console.log('DatosAdicionalesForm state: ', state);
  const [itemModalOpen, setItemModalOpen, toggleModal] = useModal();
  const [vFirstReferenceName, setvFirstReferenceName] = useState('');
  const [vFirstReferenceSecondName, setvFirstReferenceSecondName] = useState('');
  const [vFirstReferenceLastName, setvFirstReferenceLastName] = useState('');
  const [vFirstReferenceSecondLastName, setvFirstReferenceSecondLastName] = useState('');
  const [vFirstReferenceCellphone, setvFirstReferenceCellphone] = useState('');
  const [vFirstReferenceRelationship, setvFirstReferenceRelationship] = useState(0);
  const [vFirstReferenceOccupation, setvFirstReferenceOccupation] = useState('');

  const [vSecondReferenceName, setvSecondReferenceName] = useState('');
  const [vSecondReferenceSecondName, setvSecondReferenceSecondName] = useState('');
  const [vSecondReferenceLastName, setvSecondReferenceLastName] = useState('');
  const [vSecondReferenceSecondLastName, setvSecondReferenceSecondLastName] = useState('');
  const [vSecondReferenceCellphone, setvSecondReferenceCellphone] = useState('');
  const [vSecondReferenceRelationship, setvSecondReferenceRelationship] = useState(0);
  const [vSecondReferenceOccupation, setvSecondReferenceOccupation] = useState('');

  const [CFirstReferenceName, setCFirstReferenceName] = useState(false);
  const [CFirstReferenceCellphone, setCFirstReferenceCellphone] = useState(false);
  const [CFirstReferenceOccupation, setCFirstReferenceOccupation] = useState(false);
  const [CFirstReferenceRelationship, setCFirstReferenceRelationship] = useState(false);

  const [CSecondReferenceName, setCSecondReferenceName] = useState(false);
  const [CSecondReferenceCellphone, setCSecondReferenceCellphone] = useState(false);
  const [CSecondReferenceRelationship, setCSecondReferenceRelationship] = useState(false);
  const [CSecondReferenceOccupation, setCSecondReferenceOccupation] = useState(false);
  const [Disable, setDisable] = useState(false);
  const [isLoading, setisLoading] = useState(false);
  const [Visible, setVisible] = useState(false);

  useEffect(() => {
    if (isLoading != false) {
      setInterval(() => {
        setisLoading(false);
      }, 6000);
    }
  }, [isLoading]);

  enviar = () => {
    setisLoading(false);
    setVisible(true);
    onSubmit({
      vFirstReferenceName,
      vFirstReferenceSecondName,
      vFirstReferenceLastName,
      vFirstReferenceSecondLastName,
      vFirstReferenceCellphone,
      vFirstReferenceRelationship,
      vFirstReferenceOccupation,
      vSecondReferenceName,
      vSecondReferenceSecondName,
      vSecondReferenceLastName,
      vSecondReferenceSecondLastName,
      vSecondReferenceCellphone,
      vSecondReferenceRelationship,
      vSecondReferenceOccupation,
    });
    setVisible(true);
    toggleModal(true);
  };

  CheckTextInput = () => {
    if (
      vFirstReferenceName != '' &&
      vFirstReferenceCellphone != '' &&
      vFirstReferenceOccupation != '' &&
      vFirstReferenceRelationship != '' &&
      vSecondReferenceName != '' &&
      vSecondReferenceCellphone != '' &&
      vSecondReferenceRelationship != '' &&
      vSecondReferenceOccupation != ''
    ) {
      setCFirstReferenceName(false);
      setCFirstReferenceCellphone(false);
      setCFirstReferenceOccupation(false);
      setCFirstReferenceRelationship(false);

      setCSecondReferenceName(false);
      setCSecondReferenceCellphone(false);
      setCSecondReferenceRelationship(false);
      setCSecondReferenceOccupation(false);

      setisLoading(false);
      setVisible(true);
      onSubmit({
        vFirstReferenceName,
        vFirstReferenceSecondName,
        vFirstReferenceLastName,
        vFirstReferenceSecondLastName,
        vFirstReferenceCellphone,
        vFirstReferenceRelationship,
        vFirstReferenceOccupation,
        vSecondReferenceName,
        vSecondReferenceSecondName,
        vSecondReferenceLastName,
        vSecondReferenceSecondLastName,
        vSecondReferenceCellphone,
        vSecondReferenceRelationship,
        vSecondReferenceOccupation,
      });
      setVisible(true);
      toggleModal(true);
    }

    if (vFirstReferenceName != '') {
      setCFirstReferenceName(false);
    } else {
      setCFirstReferenceName(true);
    }
    if (vFirstReferenceCellphone != '') {
      setCFirstReferenceCellphone(false);
    } else {
      setCFirstReferenceCellphone(true);
    }
    if (vFirstReferenceOccupation != '') {
      setCFirstReferenceOccupation(false);
    } else {
      setCFirstReferenceOccupation(true);
    }
    if (vFirstReferenceRelationship != '') {
      setCFirstReferenceRelationship(false);
    } else {
      setCFirstReferenceRelationship(true);
    }
    if (vSecondReferenceName != '') {
      setCSecondReferenceName(false);
    } else {
      setCSecondReferenceName(true);
    }
    if (vSecondReferenceCellphone != '') {
      setCSecondReferenceCellphone(false);
    } else {
      setCSecondReferenceCellphone(true);
    }
    if (vSecondReferenceRelationship != '') {
      setCSecondReferenceRelationship(false);
    } else {
      setCSecondReferenceRelationship(true);
    }
    if (vSecondReferenceOccupation != '') {
      setCSecondReferenceOccupation(false);
    } else {
      setCSecondReferenceOccupation(true);
    }
  };

  useEffect(() => {
    let isCancelled = false;
    if (Visible && state.errorMessage != '') {
      setVisible(!Visible);
    } else {
      setVisible(false);
    }
    return () => {
      isCancelled = true;
    };
  }, [state]);

  if (Visible) {
    return (
      <Spinner
        size="large"
        visible={Visible}
        color="#D32345"
        textContent={'Un momento por favor'}
        textStyle={styles.spinnerTextStyle}
      />
    );
  }

  return (
    <ScrollView>
      {state.error == true ? (
        <InfoModal
          text={`${state.errorMessage}`}
          isActive={itemModalOpen}
          handleClose={() => setItemModalOpen(false)}
        />
      ) : null}
      <Formik
        enableReinitialize={true}
        initialValues={{ 
          fFirstReferenceName: '',
          fFirstReferenceSecondName: '',
          fFirstReferenceLastName: '',
          fFirstReferenceSecondLastName: '',
          fFirstReferenceCellphone: '',
          fFirstReferenceOccupation: '',
          fSecondReferenceName: '',
          fSecondReferenceSecondName: '',
          fSecondReferenceLastName: '',
          fSecondReferenceSecondLastName: '',
          fSecondReferenceCellphone: '',
          fSecondReferenceOccupation: '',
        }}
        onSubmit={(values, actions) => {
        // alert(JSON.stringify(values))
        const uFirstReferenceName = values.fFirstReferenceName
        const uFirstReferenceSecondName = values.fFirstReferenceSecondName
        const uFirstReferenceLastName = values.fFirstReferenceLastName
        const uFirstReferenceSecondLastName = values.fFirstReferenceSecondLastName
        const uFirstReferenceOccupation = values.fFirstReferenceOccupation
        const uSecondReferenceSecondName = values.fSecondReferenceSecondName
        const uSecondReferenceLastName = values.fSecondReferenceLastName
        const uSecondReferenceSecondLastName = values.fSecondReferenceSecondLastName
        const uSecondReferenceOccupation = values.fSecondReferenceOccupation
        
        setvFirstReferenceName(uFirstReferenceName.toUpperCase())
        setvFirstReferenceSecondName(uFirstReferenceSecondName.toUpperCase())
        setvFirstReferenceLastName(uFirstReferenceLastName.toUpperCase())
        setvFirstReferenceSecondLastName(uFirstReferenceSecondLastName.toUpperCase())
        setvFirstReferenceCellphone(values.fFirstReferenceCellphone)
        setvFirstReferenceOccupation(uFirstReferenceOccupation.toUpperCase())
        setvSecondReferenceName(values.fSecondReferenceName.toUpperCase())
        setvSecondReferenceSecondName(uSecondReferenceSecondName.toUpperCase())
        setvSecondReferenceLastName(uSecondReferenceLastName.toUpperCase())
        setvSecondReferenceSecondLastName(uSecondReferenceSecondLastName.toUpperCase())
        setvSecondReferenceCellphone(values.fSecondReferenceCellphone)
        setvSecondReferenceOccupation(uSecondReferenceOccupation.toUpperCase())
        CheckTextInput()
        setTimeout(() => {
            actions.setSubmitting(false)
        }, 2000);
        }}
        validationSchema={validationSchema}
        >
        {({ handleChange, handleBlur, handleSubmit, values, isSubmitting, errors }) => (
        <View style={styles.buttonContainer}>
          <View style={styles.group1}>
            <Text style={styles.HeadTxt}>Referencia personal 1</Text>
            <View style={styles.inputContainer}>
              <TextInput
                style={styles.inputs}
                placeholder="Nombre"
                placeholderTextColor="white"
                underlineColorAndroid="transparent"
                // onChangeText={newvFirstReferenceName =>
                //   setvFirstReferenceName(newvFirstReferenceName.toUpperCase())
                // }
                // value={vFirstReferenceName}
                autoCorrect={false}
                autoCapitalize="characters"
                
                onChangeText={handleChange('fFirstReferenceName')}
                onBlur={handleBlur('fFirstReferenceName')}
                value={values.fFirstReferenceName}
              />
            </View>
            {
              errors.fFirstReferenceName
              ?   <Text style={styles.ErrorM}>{errors.fFirstReferenceName}</Text>
              :   null
            }
            {/* {CFirstReferenceName == true ? (
              <Text style={styles.ErrorM}>Este campo no puede estar vacío</Text>
            ) : null} */}
            <View style={styles.inputContainer}>
              <TextInput
                style={styles.inputs}
                placeholder="Segundo nombre"
                placeholderTextColor="white"
                underlineColorAndroid="transparent"
                // onChangeText={newvFirstReferenceSecondName =>
                //   setvFirstReferenceSecondName(
                //     newvFirstReferenceSecondName.toUpperCase(),
                //   )
                // }
                // value={vFirstReferenceSecondName}
                autoCorrect={false}
                autoCapitalize="characters"
                
                onChangeText={handleChange('fFirstReferenceSecondName')}
                onBlur={handleBlur('fFirstReferenceSecondName')}
                value={values.fFirstReferenceSecondName}
              />
            </View>
            {
              errors.fFirstReferenceSecondName
              ?   <Text style={styles.ErrorM}>{errors.fFirstReferenceSecondName}</Text>
              :   null
            }
            <View style={styles.inputContainer}>
              <TextInput
                style={styles.inputs}
                placeholder="Apellido paterno"
                placeholderTextColor="white"
                underlineColorAndroid="transparent"
                // onChangeText={newvFirstReferenceLastName =>
                //   setvFirstReferenceLastName(
                //     newvFirstReferenceLastName.toUpperCase(),
                //   )
                // }
                // value={vFirstReferenceLastName}
                autoCorrect={false}
                autoCapitalize="characters"
                
                onChangeText={handleChange('fFirstReferenceLastName')}
                onBlur={handleBlur('fFirstReferenceLastName')}
                value={values.fFirstReferenceLastName}
              />
            </View>
            {
              errors.fFirstReferenceLastName
              ?   <Text style={styles.ErrorM}>{errors.fFirstReferenceLastName}</Text>
              :   null
            }
            <View style={styles.inputContainer}>
              <TextInput
                style={styles.inputs}
                placeholder="Apellido materno"
                placeholderTextColor="white"
                // onChangeText={newvFirstReferenceSecondLastName =>
                //   setvFirstReferenceSecondLastName(
                //     newvFirstReferenceSecondLastName.toUpperCase(),
                //   )
                // }
                // value={vFirstReferenceSecondLastName}
                underlineColorAndroid="transparent"
                autoCorrect={false}
                autoCapitalize="characters"
                
                onChangeText={handleChange('fFirstReferenceSecondLastName')}
                onBlur={handleBlur('fFirstReferenceSecondLastName')}
                value={values.fFirstReferenceSecondLastName}
              />
            </View>
            {
              errors.fFirstReferenceSecondLastName
              ?   <Text style={styles.ErrorM}>{errors.fFirstReferenceSecondLastName}</Text>
              :   null
            }
            <View style={styles.inputContainer}>
              <TextInput
                style={styles.inputs}
                placeholder="Teléfono celular"
                placeholderTextColor="white"
                underlineColorAndroid="transparent"
                // onChangeText={newvFirstReferenceCellphone =>
                //   setvFirstReferenceCellphone(newvFirstReferenceCellphone)
                // }
                // value={vFirstReferenceCellphone}
                autoCorrect={false}
                keyboardType={'numeric'}
                autoCapitalize="characters"
                maxLength={10}
                
                onChangeText={handleChange('fFirstReferenceCellphone')}
                onBlur={handleBlur('fFirstReferenceCellphone')}
                value={values.fFirstReferenceCellphone}
              />
            </View>
            {
              errors.fFirstReferenceCellphone
              ?   <Text style={styles.ErrorM}>{errors.fFirstReferenceCellphone}</Text>
              :   null
            }
            {/* {CFirstReferenceCellphone == true ? (
              <Text style={styles.ErrorM}>Este campo no puede estar vacío</Text>
            ) : null} */}
            <View style={styles.ocointainer}>
              <View style={styles.inputContainer}>
                {/* <Picker
                      style={styles.pick}
                      selectedValue={vFirstReferenceRelationship}
                      placeholder="Parentesco"
                      onValueChange={newvFirstReferenceRelationship =>
                        setvFirstReferenceRelationship(newvFirstReferenceRelationship)
                      }>
                      <Picker.Item label="Seleccione parentesco" value="3" />
                      <Picker.Item label="Familiar" value="0" />
                      <Picker.Item label="Amistad" value="1" />
                    </Picker>
                */}

                <RNPickerSelect
                  placeholder={{
                      label: 'Parentesco',
                      value: null,
                      color: '#9EA0A4'
                  }}
                  style={{
                    placeholder: {
                      flexDirection: 'column',
                      justifyContent: 'center',
                      alignItems: 'center',
                      color: 'white',
                      marginLeft: 15,
                      marginTop: Platform.OS === 'ios' ? 15 : 0
                    },
                    inputAndroid: {
                      color: 'white',
                      width: 270,
                      height: 40,
                      flexDirection: 'column',
                      justifyContent: 'center',
                      alignItems: 'center',
                      color: 'white',
                      marginLeft: 15
                    },
                    inputIOS: {
                      color: 'white',
                      width: 270,
                      height: 40,
                      flexDirection: 'column',
                      justifyContent: 'center',
                      alignItems: 'center',
                      color: 'white',
                      marginLeft: 15,
                      marginTop: 15
                    },
                    iconContainer: {
                      top: 10,
                      left: 270,
                    },
                  }}
                  useNativeAndroidPickerStyle={false}
                  Icon={() => {
                    return <Ionicons name='md-arrow-dropdown' size={20} color='#FFF' />
                  }} 
                  onValueChange={newvFirstReferenceRelationship =>
                    setvFirstReferenceRelationship(newvFirstReferenceRelationship)}
                  items={[
                      { label: 'Familiar', value: '0' },
                      { label: 'Amistad', value: '1' }
                  ]}
                />
                {/* <TouchableOpacity style={styles.inputIcon}>
                  <Image source={require('../../assets/Flecha.png')} />
                </TouchableOpacity> */}
              </View>
            </View>
            {CFirstReferenceRelationship == true ? (
              <Text style={styles.ErrorM}>Este campo no puede estar vacío</Text>
            ) : null}

            <View style={styles.inputContainer}>
              <TextInput
                style={styles.inputs}
                placeholder="Ocupación"
                placeholderTextColor="white"
                underlineColorAndroid="transparent"
                // onChangeText={newvFirstReferenceOccupation =>
                //   setvFirstReferenceOccupation(
                //     newvFirstReferenceOccupation.toUpperCase(),
                //   )
                // }
                // value={vFirstReferenceOccupation}
                autoCorrect={false}
                autoCapitalize="characters"
                
                onChangeText={handleChange('fFirstReferenceOccupation')}
                onBlur={handleBlur('fFirstReferenceOccupation')}
                value={values.fFirstReferenceOccupation}
              />
            </View>
          </View>
          {
            errors.fFirstReferenceOccupation
            ?   <Text style={styles.ErrorM}>{errors.fFirstReferenceOccupation}</Text>
            :   null
          }
          {/* {CFirstReferenceOccupation == true ? (
            <Text style={styles.ErrorM}>Este campo no puede estar vacío</Text>
          ) : null} */}

          <View style={styles.group1}>
            <Text style={styles.HeadTxt}>Referencia personal 2</Text>
            <View style={styles.inputContainer}>
              <TextInput
                style={styles.inputs}
                placeholder="Nombre"
                placeholderTextColor="white"
                underlineColorAndroid="transparent"
                // onChangeText={newvSecondReferenceName =>
                //   setvSecondReferenceName(newvSecondReferenceName.toUpperCase())
                // }
                // value={vSecondReferenceName}
                autoCorrect={false}
                autoCapitalize="characters"
                
                onChangeText={handleChange('fSecondReferenceName')}
                onBlur={handleBlur('fSecondReferenceName')}
                value={values.fSecondReferenceName}
              />
            </View>
            {
              errors.fSecondReferenceName
              ?   <Text style={styles.ErrorM}>{errors.fSecondReferenceName}</Text>
              :   null
            }
            {/* {CSecondReferenceName == true ? (
              <Text style={styles.ErrorM}>Este campo no puede estar vacío</Text>
            ) : null} */}
            <View style={styles.inputContainer}>
              <TextInput
                style={styles.inputs}
                placeholder="Segundo nombre"
                placeholderTextColor="white"
                underlineColorAndroid="transparent"
                // onChangeText={newvSecondReferenceSecondName =>
                //   setvSecondReferenceSecondName(
                //     newvSecondReferenceSecondName.toUpperCase(),
                //   )
                // }
                // value={vSecondReferenceSecondName}
                autoCorrect={false}
                autoCapitalize="characters"
                
                onChangeText={handleChange('fSecondReferenceSecondName')}
                onBlur={handleBlur('fSecondReferenceSecondName')}
                value={values.fSecondReferenceSecondName}
              />
            </View>
            {
              errors.fSecondReferenceSecondName
              ?   <Text style={styles.ErrorM}>{errors.fSecondReferenceSecondName}</Text>
              :   null
            }
            <View style={styles.inputContainer}>
              <TextInput
                style={styles.inputs}
                placeholder="Apellido paterno"
                placeholderTextColor="white"
                underlineColorAndroid="transparent"
                // onChangeText={newvSecondReferenceLastName =>
                //   setvSecondReferenceLastName(
                //     newvSecondReferenceLastName.toUpperCase(),
                //   )
                // }
                // value={vSecondReferenceLastName}
                autoCorrect={false}
                autoCapitalize="characters"
                
                onChangeText={handleChange('fSecondReferenceLastName')}
                onBlur={handleBlur('fSecondReferenceLastName')}
                value={values.fSecondReferenceLastName}
              />
            </View>
            {
              errors.fSecondReferenceLastName
              ?   <Text style={styles.ErrorM}>{errors.fSecondReferenceLastName}</Text>
              :   null
            }
            <View style={styles.inputContainer}>
              <TextInput
                style={styles.inputs}
                placeholder="Apellido materno"
                placeholderTextColor="white"
                // onChangeText={newvSecondReferenceSecondLastName =>
                //   setvSecondReferenceSecondLastName(
                //     newvSecondReferenceSecondLastName.toUpperCase(),
                //   )
                // }
                // value={vSecondReferenceSecondLastName}
                underlineColorAndroid="transparent"
                autoCorrect={false}
                autoCapitalize="characters"
                
                onChangeText={handleChange('fSecondReferenceSecondLastName')}
                onBlur={handleBlur('fSecondReferenceSecondLastName')}
                value={values.fSecondReferenceSecondLastName}
              />
            </View>
            {
              errors.fSecondReferenceSecondLastName
              ?   <Text style={styles.ErrorM}>{errors.fSecondReferenceSecondLastName}</Text>
              :   null
            }
            <View style={styles.inputContainer}>
              <TextInput
                style={styles.inputs}
                placeholder="Teléfono celular"
                placeholderTextColor="white"
                underlineColorAndroid="transparent"
                // onChangeText={newvSecondReferenceCellphone =>
                //   setvSecondReferenceCellphone(newvSecondReferenceCellphone)
                // }
                // value={vSecondReferenceCellphone}
                autoCorrect={false}
                keyboardType={'numeric'}
                autoCapitalize="characters"
                maxLength={10}
                
                onChangeText={handleChange('fSecondReferenceCellphone')}
                onBlur={handleBlur('fSecondReferenceCellphone')}
                value={values.fSecondReferenceCellphone}
              />
            </View>
            {
              errors.fSecondReferenceCellphone
              ?   <Text style={styles.ErrorM}>{errors.fSecondReferenceCellphone}</Text>
              :   null
            }
            {/* {CSecondReferenceCellphone == true ? (
              <Text style={styles.ErrorM}>Este campo no puede estar vacío</Text>
            ) : null} */}
            <View style={styles.ocointainer}>
              <View style={styles.inputContainer}>
                {/* <Picker
                  style={styles.pick}
                  selectedValue={vSecondReferenceRelationship}
                  placeholder="Parentesco"
                  onValueChange={newvSecondReferenceRelationship =>
                    setvSecondReferenceRelationship(
                      newvSecondReferenceRelationship,
                    )
                  }>
                  <Picker.Item label="Seleccione parentesco" value="3" />
                  <Picker.Item label="Familiar" value="0" />
                  <Picker.Item label="Amistad" value="1" />
                </Picker> */}
                <RNPickerSelect
                  placeholder={{
                      label: 'Parentesco',
                      value: null,
                      color: '#9EA0A4'
                  }}
                  style={{
                    placeholder: {
                      flexDirection: 'column',
                      justifyContent: 'center',
                      alignItems: 'center',
                      color: 'white',
                      marginLeft: 15,
                      marginTop: Platform.OS === 'ios' ? 15 : 0
                    },
                    inputAndroid: {
                      color: 'white',
                      width: 270,
                      height: 40,
                      flexDirection: 'column',
                      justifyContent: 'center',
                      alignItems: 'center',
                      color: 'white',
                      marginLeft: 15,
                      marginTop: 20
                    },
                    inputIOS: {
                      color: 'white',
                      width: 270,
                      height: 40,
                      flexDirection: 'column',
                      justifyContent: 'center',
                      alignItems: 'center',
                      color: 'white',
                      marginLeft: 15,
                      marginTop: 15
                    },
                    iconContainer: {
                      top: 10,
                      right: 15,
                    }
                  }}
                  useNativeAndroidPickerStyle={false}
                  Icon={() => {
                    return <Ionicons name='md-arrow-dropdown' size={20} color='#FFF' />
                  }}
                  onValueChange={newvSecondReferenceRelationship =>
                    setvSecondReferenceRelationship(newvSecondReferenceRelationship)
                  }
                  items={[
                      { label: 'Familiar', value: '0' },
                      { label: 'Amistad', value: '1' },
                  ]}
                  // Icon={() => {
                  //   return <Image source={require('../../assets/Flecha.png')} />
                  // }}
                />
                {/* <TouchableOpacity style={styles.inputIcon}>
                  <Image source={require('../../assets/Flecha.png')} />
                </TouchableOpacity> */}
              </View>
            </View>
            {CSecondReferenceRelationship == true ? (
              <Text style={styles.ErrorM}>Este campo no puede estar vacío</Text>
            ) : null}
            <View style={styles.inputContainer}>
              <TextInput
                style={styles.inputs}
                placeholder="Ocupación"
                placeholderTextColor="white"
                underlineColorAndroid="transparent"
                // onChangeText={newvSecondReferenceOccupation =>
                //   setvSecondReferenceOccupation(
                //     newvSecondReferenceOccupation.toUpperCase(),
                //   )
                // }
                // value={vSecondReferenceOccupation}
                autoCorrect={false}
                autoCapitalize="characters"
                
                onChangeText={handleChange('fSecondReferenceOccupation')}
                onBlur={handleBlur('fSecondReferenceOccupation')}
                value={values.fSecondReferenceOccupation}
              />
            </View>
            {
              errors.fSecondReferenceOccupation
              ?   <Text style={styles.ErrorM}>{errors.fSecondReferenceOccupation}</Text>
              :   null
            }

            {/* {CSecondReferenceOccupation == true ? (
              <Text style={styles.ErrorM}>Este campo no puede estar vacío</Text>
            ) : null} */}

            <View style={styles.buttonContainer2}>
              <TouchableOpacity style={styles.viewData} onPress={handleSubmit} title="Submit" disabled={isSubmitting ? true: false}>          
                {
                isSubmitting 
                  ? <ActivityIndicator color='#fff' />
                  : <Text style={styles.loginText}>CONTINUAR</Text>
                }
              </TouchableOpacity>
            </View>
          </View>
        </View>
        )}
      </Formik>

      {/* <View style={styles.buttonContainer2}>
        <TouchableOpacity
          disabled={Disable}
          style={styles.viewData}
          onPress={this.CheckTextInput}>
          <Text style={styles.loginText}>CONTINUAR</Text>
        </TouchableOpacity>
      </View> */}
    </ScrollView>
  );
};

DatosAdicionalesForm.navigationOptions = {
  headerShown: false,
};

const styles = StyleSheet.create({
  spinnerTextStyle: {
    color: '#D32345',
  },
  HeadTxt: {
    color: 'white',
    marginBottom: 20,
  },
  containerdate: {
    backgroundColor: '#5e5e5e',
    borderRadius: 30,
    width: 300,
    height: 45,
    marginBottom: 20,
    flexDirection: 'row',
  },
  checkbox: {
    flexDirection: 'row',
    backgroundColor: '#333333',
    borderWidth: 0,
    marginBottom: 10,
  },
  checkboxTxt: {
    color: '#b3b4b5',
  },
  buttonContainer2: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttonContainer: {
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  inputContainer: {
    backgroundColor: '#5e5e5e',
    borderRadius: 30,
    width: 300,
    height: 45,
    marginBottom: 20,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start'
  },
  ocointainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center'
  },
  inputs: {
    height: 45,
    marginLeft: 16,
    color: 'white',
    flex: 1,
  },
  logout: {
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 20,
    width: 142,
    borderRadius: 30,
    height: 45,
    backgroundColor: '#911830',
  },
  loginText: {
    color: 'white',
    fontWeight: 'bold',
  },
  viewData: {
    marginLeft: 20,
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 20,
    width: 142,
    borderRadius: 30,
    height: 45,
    backgroundColor: '#D32345',
  },
  pick: {
    backgroundColor: '#696969',
    color: 'white',
    height: 40,
    fontSize: 20,
    borderRadius: 50,
    flexBasis: '88%',
    marginLeft: 15,
  },
  inputIcon: {
    marginRight: 15,
    justifyContent: 'center',
    height: 10,
    width: 10,
  },
  ErrorM: {
    color: '#D32345',
    fontWeight: 'bold',
    marginBottom: 15,
  },
  group1: {
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default DatosAdicionalesForm;

/*
<TouchableOpacity 
style={ styles.logout}  
onPress={() => onSubmit({vFirstReferenceName,vFirstReferenceSecondName,vFirstReferenceLastName,
    vFirstReferenceSecondLastName,
    vFirstReferenceCellphone,vFirstReferenceRelationship,vFirstReferenceOccupation,
    vSecondReferenceName ,vSecondReferenceSecondName ,vSecondReferenceLastName ,vSecondReferenceSecondLastName,
    vSecondReferenceCellphone ,vSecondReferenceRelationship ,vSecondReferenceOccupation })}>  
<Text style={styles.loginText}>GUARDAR</Text>
</TouchableOpacity>*/
