import React, {useContext, useState, useEffect, useRef} from 'react';
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableOpacity,
  ScrollView,
  Picker,
  Image,
  ActivityIndicator,
} from 'react-native';
import PickerModal from 'react-native-picker-modal-view';
import Ionicons from 'react-native-vector-icons/Ionicons';
import RNPickerSelect from 'react-native-picker-select';
import {navigate} from '../navigationRef';
import AsyncStorage from '@react-native-community/async-storage';
import {CheckBox} from 'react-native-elements';
import DatePicker from 'react-native-datepicker';
import ForceApi from '../api/force';
import {useModal} from './useModal';
import InfoModal from './InfoModal';
import Spinner from 'react-native-loading-spinner-overlay';
import { Formik } from 'formik';
import * as yup from 'yup';

const validationSchema = yup.object().shape({
  fStreetInput: yup
              .string()
              .matches(/^[A-Za-zÑñ0-9 ]*$/, 'No ingrese caracteres especiales ni palabras con tildes')
              .required('Este campo no puede estar vacío')
              .uppercase(),
  fNumExt: yup
              .string()
              .matches(/^[A-Za-zÑñ0-9 ]*$/, 'No ingrese caracteres especiales ni palabras con tildes')
              .required('Este campo no puede estar vacío')
              .uppercase(),
  fInteriorNumber: yup
          .string()
          .matches(/^[A-Za-zÑñ0-9 ]*$/, 'No ingrese caracteres especiales ni con tildes')
          // .required('Este campo no puede estar vacío')
          .uppercase(),
  fZipCode: yup
              .string()
              .matches(/^[0-9]*$/, 'Ingrese únicamente números')
              .min(5, 'Código postal debe tener 5 dígitos')
              .max(5, 'Código postal debe tener 5 dígitos')
              .typeError('Ingrese únicamente números')
              .required('Este campo no puede estar vacío'),
})

const OcrInfoForm = ({state, onSubmit, errorMessage}) => {
  console.log('.................OcrInfoForm state..................: ', state);

  const [vName, setvName] = useState('');
  const [vSecondName, setvSecondName] = useState('');
  const [vLastName, setvLastName] = useState('');
  const [vSecondLastName, setvSecondLastName] = useState('');
  const [vBirthDate, setvBirthDate] = useState('');
  const [vRFC, setvRFC] = useState('');
  const [vGender, setvGender] = useState(1);
  const [itemModalOpen, setItemModalOpen, toggleModal] = useModal();

  const [vZipCode, setvZipCode] = useState('');
  const [vStreet, setvStreet] = useState('');
  const [vExteriorNumber, setvExteriorNumber] = useState('');
  const [vInteriorNumber, setvInteriorNumber] = useState('');
  const [vState, setvState] = useState('Estado');
  const [vCity, setvCity] = useState('Ciudad');
  const [vSuburb, setvSuburb] = useState('MySuburb');

  const [pSuburb, setpSuburb] = useState([]);

  const [CCalle, setCCalle] = useState(false);
  const [CNum, setCNum] = useState(false);
  const [CCode, setCCode] = useState(false);
  const [CCol, setCCol] = useState(false);
  const [CCiud, setCCiud] = useState(false);
  const [CEstado, setCCEstado] = useState(false);
  const [isLoading, setisLoading] = useState(false);
  const [Visible, setVisible] = useState(false);
  const [selectedItem, setselectedItem] = useState({});
  const [newSuburb, setnewSuburb] = useState([]);

  CheckTextInput = () => {
    if (
      vStreet != '' &&
      vExteriorNumber != '' &&
      vSuburb != '' &&
      vCity != '' &&
      vState != ''
    ) {
      setisLoading(false);
      setCCalle(false);
      setCNum(false);
      setCCol(false);
      setCCiud(false);
      setCCEstado(false);
      onSubmit({
        vZipCode,
        vStreet,
        vExteriorNumber,
        vInteriorNumber,
        vState,
        vCity,
        vSuburb,
      });
      setVisible(true);
      toggleModal(true);
    }

    if (vStreet != '') {
      setCCalle(false);
    } else {
      setCCalle(true);
    }

    if (vExteriorNumber != '') {
      setCNum(false);
    } else {
      setCNum(true);
    }

    if (vZipCode != '') {
      setCCode(false);
    } else {
      setCCode(true);
    }

    if (vSuburb != '') {
      setCCol(false);
    } else {
      setCCol(true);
    }

    if (vCity != '') {
      setCCiud(false);
    } else {
      setCCiud(true);
    }

    if (vState != '') {
      setCCEstado(false);
    } else {
      setCCEstado(true);
    }
  };

  const getAddres = ({vZipCode}) => {
    ForceApi.post(`/GetAddressController.php`, {vZipCode}).then(res => {
      setnewSuburb([])
      setvState(res.data.state);
      setvCity(res.data.city);
      setpSuburb(res.data.suburbs);
      
      var array = res.data.suburbs;
      var arr = [];
      array.forEach((element, index) => {
        for (let [key, value] of Object.entries(element)) {
          arr.push({
            label: value,
            value: value
          });
        }
      });
      
      setnewSuburb(arr)
      
    });
  };

  useEffect(() => {
    let isCancelled = false;
    if (vZipCode) {
      getAddres({vZipCode});
    }

    return () => {
      isCancelled = true;
    };
  }, [vZipCode]);

  useEffect(() => {
    let isCancelled = false;
    async function getValores() {
      const vSolicitudeId = await AsyncStorage.getItem('SolicitudeId');
      const {data} = await ForceApi.post(`/GetOCRResponseController.php`, {
        vSolicitudeId,
      });

        try {
          const vCellphoneOCR = await AsyncStorage.getItem('vCellphoneOCR')
          const rfcOCR = data.rfc;
          if(vCellphoneOCR !== null) {
            getValidation(vCellphoneOCR, rfcOCR)
          }
        } catch(e) {
          // error reading value
          console.warn('Fail getting vCellphoneOCR:', e);
          
        }

        async function getValidation(vCellphone, vRFC) {
          
          ForceApi.post(`/ValidateCellphoneController.php`, {vCellphone, vRFC}).then(
            res => {
              const errorVal = res.data.error;
              console.log('getValidation!:');
              console.log(res.data);
              if (res.data.validCellphone) {
                console.log('pasó sin errores');
                // navigate('Ocr');
              } else {
                // navigate('Ocr');
                console.log('hay errores');
              }
              
            },
          );
        }

      console.log(data);
      console.log('ENTRO');
      console.log('username', data.name);

      setvName(data.name);
      setvSecondName(data.second_name);
      setvLastName(data.last_name);
      setvSecondLastName(data.second_last_name);
      setvBirthDate(data.birth_date);
      setvRFC(data.rfc);
      setvGender(data.gender);
      setvZipCode(data.zip_code);
      setvStreet(data.street);
      setvExteriorNumber(data.exterior_number);
      setvState(data.state);
      setvCity(data.city);
      setvSuburb('');
      // setvSuburb(data.suburb);
    }
    getValores();

    return () => {
      isCancelled = true;
    };
  }, []);

  useEffect(() => {
    let isCancelled = false;
    if (Visible && state.errorMessage != '') {
      setVisible(!Visible);
    } else {
      setVisible(false);
    }
    return () => {
      isCancelled = true;
    };
  }, [state]);

  if (Visible) {
    return (
      <Spinner
        size="large"
        visible={Visible}
        color="#D32345"
        textContent={'Un momento por favor'}
        textStyle={styles.spinnerTextStyle}
      />
    );
  }

  return (
    <ScrollView>
      {state.error == true ? (
        <InfoModal
          text={`${state.errorMessage}`}
          isActive={itemModalOpen}
          handleClose={() => setItemModalOpen(false)}
        />
      ) : null}
      <View style={styles.buttonContainer}>
        <Formik
          enableReinitialize={true}
          initialValues={{ 
            fStreetInput: vStreet,
            fNumExt: vExteriorNumber,
            fInteriorNumber: vInteriorNumber,
            fZipCode: vZipCode 
          }}
          onSubmit={(values, actions) => {
            // alert(JSON.stringify(values))
            const uStreetInput = values.fStreetInput;
            const uNumExt = values.fNumExt;
            const _InteriorNumber = values.fInteriorNumber;

            setvStreet(uStreetInput.toUpperCase());
            setvExteriorNumber(uNumExt.toUpperCase());
            setvInteriorNumber(_InteriorNumber.toUpperCase())
            setvZipCode(values.fZipCode);
            CheckTextInput()
            setTimeout(() => {
              actions.setSubmitting(false)
            }, 2000);
          }}
          validationSchema={validationSchema}
        >
          {({ handleChange, handleBlur, handleSubmit, values, isSubmitting, errors }) => (
            <>
              <View style={styles.inputContainer}>
                <TextInput
                style={styles.inputs}
                placeholder={vName}
                placeholderTextColor="white"
                underlineColorAndroid="transparent"
                editable={false}
                selectTextOnFocus={false}
                />
              </View>
              <View style={styles.inputContainer}>
                  <TextInput
                  style={styles.inputs}
                  placeholder={vSecondName}
                  placeholderTextColor="white"
                  underlineColorAndroid="transparent"
                  editable={false}
                  selectTextOnFocus={false}
                  />
              </View>
              <View style={styles.inputContainer}>
                  <TextInput
                  style={styles.inputs}
                  placeholder={vLastName}
                  placeholderTextColor="white"
                  underlineColorAndroid="transparent"
                  editable={false}
                  selectTextOnFocus={false}
                  />
              </View>
              <View style={styles.inputContainer}>
                  <TextInput
                  style={styles.inputs}
                  placeholder={vSecondLastName}
                  placeholderTextColor="white"
                  underlineColorAndroid="transparent"
                  editable={false}
                  selectTextOnFocus={false}
                  />
              </View>
              <View style={styles.inputContainer}>
                  <TextInput
                  style={styles.inputs}
                  placeholder={vBirthDate}
                  placeholderTextColor="white"
                  underlineColorAndroid="transparent"
                  editable={false}
                  selectTextOnFocus={false}
                  />
              </View>
              <View style={styles.inputContainer}>
                  <TextInput
                  style={styles.inputs}
                  placeholder={vRFC}
                  placeholderTextColor="white"
                  underlineColorAndroid="transparent"
                  editable={false}
                  selectTextOnFocus={false}
                  />
              </View>
              <View style={styles.inputContainer}>
                <TextInput
                  style={styles.inputs}
                  placeholder="Código postal"
                  placeholderTextColor="white"
                  underlineColorAndroid="transparent"
                  autoCorrect={false}
                  autoCapitalize="characters"
                  keyboardType={'numeric'}
                  type="text"
                  maxLength={5}
                  
                  onChangeText={handleChange('fZipCode')}
                  // onBlur={handleBlur('fZipCode')}
                  onBlur={() => setvZipCode(values.fZipCode)}
                  value={values.fZipCode}
                />
              </View>
              {
                errors.fZipCode
                ? <Text style={styles.ErrorM}>{errors.fZipCode}</Text>
                : null
              }
              <View style={styles.inputContainer}>
                <TextInput
                  style={styles.inputs}
                  placeholder="Ingrese la calle"
                  placeholderTextColor="white"
                  underlineColorAndroid="transparent"
                  autoCorrect={false}
                  autoCapitalize="characters"

                  onChangeText={handleChange('fStreetInput')}
                  onBlur={handleBlur('fStreetInput')}
                  value={values.fStreetInput}
                />
              </View>
              {
                errors.fStreetInput
                ? <Text style={styles.ErrorM}>{errors.fStreetInput}</Text>
                : null
              }
              
              <View style={styles.inputContainer}>
                <TextInput
                  style={styles.inputs}
                  placeholder="Ingrese número exterior"
                  placeholderTextColor="white"
                  underlineColorAndroid="transparent"
                  autoCorrect={false}
                  autoCapitalize="characters"
                  
                  onChangeText={handleChange('fNumExt')}
                  onBlur={handleBlur('fNumExt')}
                  value={values.fNumExt}
                />
              </View>
              {
                errors.fNumExt 
                ? <Text style={styles.ErrorM}>{errors.fNumExt}</Text>
                : null
              }
              <View style={styles.inputContainer}>
                  <TextInput
                    style={styles.inputs}
                    placeholder="Ingrese número interior"
                    placeholderTextColor="white"
                    underlineColorAndroid="transparent"
                    // onChangeText={newvInteriorNumber =>
                    //   setvInteriorNumber(newvInteriorNumber.toUpperCase())
                    // }
                    // value={vInteriorNumber}
                    autoCorrect={false}
                    autoCapitalize="characters"
                    onChangeText={handleChange('fInteriorNumber')}
                    onBlur={handleBlur('fInteriorNumber')}
                    value={values.fInteriorNumber}
                  />
                </View>
              {
                errors.fInteriorNumber
                ? <Text style={styles.ErrorM}>{errors.fInteriorNumber}</Text>
                : null
              }

              {/* <View style={styles.inputContainer}>
                <TextInput
                  style={styles.inputs}
                  placeholder="Código postal"
                  // placeholder={vZipCode}
                  placeholderTextColor="white"
                  underlineColorAndroid="transparent"
                  onChangeText={newvZipCode => setvZipCode(newvZipCode.toUpperCase())}
                  value={vZipCode}
                  autoCorrect={false}
                  autoCapitalize="characters"
                  keyboardType={'numeric'}
                  type="text"
                  maxLength={5}
                />
              </View> */}

              <View style={styles.ocointainer}>
                <View style={styles.inputContainer}>
                <RNPickerSelect
                    placeholder={{
                      label: 'Seleccione una colonia',
                      value: null,
                      color: '#9EA0A4'
                    }}
                    style={{
                      placeholder: {
                        flexDirection: 'column',
                        justifyContent: 'center',
                        alignItems: 'center',
                        color: 'white',
                        marginLeft: 15
                      },
                      inputAndroid: {
                        color: 'white',
                        width: 270,
                        height: 40,
                        flexDirection: 'column',
                        justifyContent: 'center',
                        alignItems: 'center',
                        color: 'white',
                        marginLeft: 15
                      },
                      inputIOS: {
                        color: 'white',
                        width: 270,
                        height: 40,
                        flexDirection: 'column',
                        justifyContent: 'center',
                        alignItems: 'center',
                        color: 'white',
                        marginLeft: 15,
                        marginTop: 15
                      },
                      iconContainer: {
                        top: 10,
                        right: 15,
                      }
                    }}
                    useNativeAndroidPickerStyle={false}
                    Icon={() => {
                      return <Ionicons name='md-arrow-dropdown' size={20} color='#FFF' />
                    }}
                    onValueChange={newpSuburb => setvSuburb(newpSuburb)}
                    items={newSuburb}
                    // items={pSuburb}
                  />
                  {/* <PickerModal
                      renderSelectView={(disabled, selected, showModal) =>
                        <TouchableOpacity disabled={disabled} onPress={showModal} style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', marginHorizontal: 15}}>
                          <Text style={{ color: 'white' }}>{ vSuburb ? vSuburb : 'Seleccione una colonia' }</Text>
                          <Image source={require('../../assets/Flecha.png')} />
                        </TouchableOpacity>
                      }
                      onSelected={newpSuburb => setvSuburb(newpSuburb.Name)}
                      items={newSuburb}
                      sortingLanguage={'tr'}
                      showToTopButton={true}
                      selected={vSuburb}
                      autoGenerateAlphabeticalIndex={true}
                      selectPlaceholderText={'Seleccione una colonia'}
                      onEndReached={() => console.log('list ended...')}
                      searchPlaceholderText={'Search...'}
                      requireSelection={false}
                      autoSort={false}
                    /> */}
                  {/* <Picker
                    style={styles.pick}
                    selectedValue={vSuburb}
                    itemStyle={styles.onePickerItem}
                    placeholderTextColor="white"
                    onValueChange={newpSuburb => setvSuburb(newpSuburb)}>
                    <Picker.Item label="Seleccione la colonia" value="word1" />
                    {(pSuburb || []).map((item, index) => {
                      return (
                        <Picker.Item
                          key={index}
                          label={`${item[index]}`}
                          value={`${item[index]}`}
                        />
                      );
                    })}
                  </Picker>
                  <TouchableOpacity style={styles.inputIcon}>
                    <Image source={require('../../assets/Flecha.png')} />
                  </TouchableOpacity> */}
                </View>
              </View>
              <View style={styles.inputContainer}>
                <TextInput
                  style={styles.inputs}
                  placeholder={vCity}
                  placeholderTextColor="white"
                  underlineColorAndroid="transparent"
                  editable={false}
                  value={vCity}
                  selectTextOnFocus={false}
                  autoCorrect={false}
                  autoCapitalize="characters"
                />
              </View>
              <View style={styles.inputContainer}>
                <TextInput
                  style={styles.inputs}
                  placeholder={vState}
                  placeholderTextColor="white"
                  underlineColorAndroid="transparent"
                  value={vState}
                  editable={false}
                  selectTextOnFocus={false}
                  autoCorrect={false}
                  autoCapitalize="characters"
                />
              </View>
              <View style={styles.buttonContainer2}>
                <TouchableOpacity style={styles.viewData} onPress={handleSubmit} title="Submit" disabled={isSubmitting ? true: false}>          
                  {
                  isSubmitting 
                  ? <ActivityIndicator color='#fff' />
                  : <Text style={styles.loginText}>CONTINUAR</Text>
                  }
                </TouchableOpacity>
              </View>
            </>
          )}
        </Formik>
      </View>

      {/* <View style={styles.buttonContainer2}>
        <TouchableOpacity
          style={styles.viewData}
          onPress={() => CheckTextInput()}
          >
          <Text style={styles.loginText}>CONTINUAR</Text>
        </TouchableOpacity>
      </View> */}
    </ScrollView>
  );
};

OcrInfoForm.navigationOptions = {
  headerShown: false,
};

const styles = StyleSheet.create({
  spinnerTextStyle: {
    color: '#D32345',
  },
  container: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: '#333333',
  },
  logo: {
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    marginTop: 40,
    marginBottom: 20,
    marginLeft: 10,
  },
  backContainer: {
    flexDirection: 'column',
    marginBottom: 20,
  },
  head: {
    backgroundColor: '#000',
    borderRadius: 20,
  },
  back: {
    marginTop: 2,
    flexDirection: 'column',
    color: 'white',
  },
  textContainer: {
    flexDirection: 'row',
    marginTop: 30,
    marginBottom: 10,
    marginLeft: 10,
  },
  textIniciar: {
    color: 'white',
    width: 300,
    fontSize: 20,
    fontWeight: 'bold',
    textAlign: 'left',
    marginLeft: 25,
  },
  textInit: {
    color: '#b3b4b5',
    textAlign: 'left',
    marginLeft: 25,
  },
  textInit: {
    color: '#b3b4b5',
    textAlign: 'left',
    marginLeft: 25,
  },

  buttonContainer2: {
    marginTop: 10,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  logout: {
    marginTop: 20,
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 20,
    width: 142,
    borderRadius: 30,
    height: 45,
    backgroundColor: '#911830',
  },
  buttonContainer: {
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  lightOn: {
    marginTop: 20,
    marginLeft: 20,
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 20,
    width: 142,
    borderRadius: 30,
    height: 45,
    backgroundColor: '#D32345',
  },
  OUB: {
    marginLeft: 20,
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 20,
    width: 142,
    borderRadius: 30,
    height: 45,
    backgroundColor: '#D32345',
  },
  loginText: {
    color: 'white',
    fontWeight: 'bold',
  },
  inputContainer: {
    backgroundColor: '#5e5e5e',
    borderRadius: 30,
    width: 300,
    height: 45,
    marginBottom: 20,
    flexDirection: 'row',
    alignItems: 'center',
  },

  SinputContainer: {
    backgroundColor: '#5e5e5e',
    borderRadius: 30,
    width: 145,
    height: 45,
    marginBottom: 20,
    flexDirection: 'row',
    alignItems: 'center',
  },

  SinputContainer2: {
    backgroundColor: '#5e5e5e',
    borderRadius: 30,
    width: 145,
    height: 45,
    marginBottom: 20,
    flexDirection: 'row',
    alignItems: 'center',
    marginLeft: 10,
  },
  inputs: {
    height: 45,
    marginLeft: 16,
    color: 'white',
    flex: 1,
  },
  ocointainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  pick: {
    backgroundColor: '#696969',
    color: 'white',
    height: 40,
    fontSize: 20,
    borderRadius: 50,
    flexBasis: '88%',
    marginLeft: 15,
  },
  inputIcon: {
    marginRight: 15,
    justifyContent: 'center',
    height: 10,
    width: 10,
  },
  viewData: {
    marginLeft: 20,
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 20,
    width: 142,
    borderRadius: 30,
    height: 45,
    backgroundColor: '#D32345',
  },
  ErrorM: {
    color: '#D32345',
    fontWeight: 'bold',
    marginBottom: 15,
  },
  onePickerItem: {
    height: 44,
    color: 'red',
  },
  validationText: {
    color: '#D32345',
    fontWeight: 'bold',
    marginBottom: 15,
    fontSize: 13,
  }
});

export default OcrInfoForm;
