import React from 'react';
import {StyleSheet} from 'react-native';
import Spinner from 'react-native-loading-spinner-overlay';

const CustomSpinner = ({isActive, text}) => {
  return (
    <Spinner
      size="large"
      visible={isActive}
      color="#D32345"
      textContent={text}
      textStyle={styles.spinnerTextStyle}
    />
  );
};

const styles = StyleSheet.create({
  spinnerTextStyle: {
    color: '#D32345',
  },
});

export default CustomSpinner;
