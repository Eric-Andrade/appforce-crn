import React, { useRef, useState, useEffect } from 'react'
import { View, StyleSheet, Text, TouchableOpacity, Image, Platform, ScrollView, TextInput, ActivityIndicator } from 'react-native';
import {Icon} from 'react-native-elements';
import DocumentScanner from '@woonivers/react-native-document-scanner';
import ImgToBase64 from 'react-native-image-base64';
import Header from '../components/Header';
import Ionicons from 'react-native-vector-icons/Ionicons';
import {withNavigationFocus} from 'react-navigation';
import RNPickerSelect from 'react-native-picker-select';
import ImagePicker from 'react-native-image-crop-picker';
import Spinner from 'react-native-loading-spinner-overlay';
import {useModal} from './useModal';
import InfoModal from './InfoModal';
import { Formik } from 'formik';
import * as yup from 'yup';

const validationSchema = yup.object().shape({
    fComment: yup
        .string()
        .matches(/^[A-Za-zÑñ0-9., ]*$/, 'No ingrese caracteres especiales ni con tildes')
        .required('Este campo no puede estar vacío')
        .uppercase(),
})

let localdata = new FormData();
let localdataDocuments = new FormData();
const IrregularForm = ({state, onSubmit, navigation, isFocused}) => {
  const pdfScannerElement = useRef(null)
  const [itemModalOpen, setItemModalOpen, toggleModal] = useModal();
  const [data, setData] = useState({})
  const [scannerVisible, setvisible] = useState(!true)
  const [image64, setimage64] = useState(!true)
  const [images, setimages] = useState([]);
  const [items, setitems] = useState([]);
  const [Visible, setVisible] = useState(false);
  const [aComment, setaComment] = useState(false);
  const [vComment, setvComment] = useState('');
  const [vKindDocument, setvKindDocument] = useState('');

  const vSolicitudeId = navigation.getParam('_id');
  
  function handleOnPress() {
    pdfScannerElement.current.capture()
  }

  function handleOnPressVisible() {
    setvisible(!scannerVisible)
  }
  
  const openImagePicker = () => {
    ImagePicker.openPicker({
      multiple: true,
      includeBase64: true,
      waitAnimationEnd: true,
      includeExif: true,
      mediaType: 'photo',
      // maxFiles: 2,
    })
      .then(images => {
        images.map((images, index) => {
          
          localdata.append({
            uri: images.data,
            path: images.path,
          });
          let arrayjoined = localdata['_parts'].concat(localdataDocuments._parts)
          // setimages(localdata['_parts']);
          setimages(arrayjoined);
          // console.log('///wqeqwwq////',images);
        });
      })
      .catch(e => console.warn(`Error al cargar imágenes: ${e}`));
  };

  const scanner = ({ croppedImage }) => {
    console.log('cayó en scanner', croppedImage)
      ImgToBase64.getBase64String(croppedImage)
        .then(image => {
          try {
            if (images.length !== 0) {
              console.log('if');
              localdataDocuments.append({
                uri: image,
                path: croppedImage
              });
             let mydata = localdata['_parts'].concat([[{
                uri: image,
                path: croppedImage
              }]])
  
              setimages(mydata)
            }
            else {
              console.log('else');
              localdataDocuments.append({
                uri: image,
                path: croppedImage
              });
              
              setimages([
                [
                  {
                    uri: image,
                    path: croppedImage
                  }
                ]
              ])
            }
            
          setvisible(!scannerVisible)
          } catch (error) {
            console.log(error); 
          }
        })
        .catch(err => console.warn(err));
  }
  
  const upload = () => {
    var newArray = [];
    images.forEach(element => {
      // const itemJSON = element[0];
      newArray = newArray.concat({ uri: element[0].uri });
      console.log('elements: ', newArray);
      
    });

    setVisible(true);
    onSubmit({
      vDocumentB64: newArray,
    });
    toggleModal(true);
  };

  const CheckTextInput = () => {
    if (vComment != '') {
      var newArray = [];
      images.forEach(element => {
        // const itemJSON = element[0];
        newArray = newArray.concat({ uri: element[0].uri });
        console.log('elements: ', newArray);
        
      });
  
      setVisible(true);
      onSubmit({ vDocument: newArray, vComment, vKindDocument, vSolicitudeId});
      toggleModal(true);
      // console.log(`vComment: ${vComment} vKindDocument: ${vKindDocument} vDocument: ${vDocument}`);
    }

    if (vComment != '') {
      // setvComment(false)
      setaComment(false);
    } else {
      // setvComment(true)
      setaComment(true);
    }
    // if (vKindDocument != '') {
    //     setvKindDocument(false)
    //     setaKindDocument(false)
    // } else {
    //     setvKindDocument(true)
    //     setaKindDocument(true)
    // }
  };

  const cleanImages = () => {
    ImagePicker.clean()
      .then(() => {
        console.log('removed all tmp images from tmp directory');
        setimages([]);
        localdata['_parts'].length = 0
        localdataDocuments['_parts'].length = 0
      })
      .then(() => {
        console.log('cleaned localdata');
        console.log(JSON.stringify(localdata['_parts']));
      })
      .catch(e => {
        alert(e);
      });
  };
  
  useEffect(() => {
    let isCancelled = false;
    cleanImages();
    if (Visible && state.errorMessage != '') {
      setVisible(!Visible);
    } else {
      setVisible(false);
    }
    return () => {
      isCancelled = true;
    };
  }, [state]);

  if (Visible) {
    return (
      <Spinner
        size='large'
        visible={Visible}
        color='#D32345'
        textContent={'Un momento por favor'}
        textStyle={styles.spinnerTextStyle}
      />
    );
  }

  return (
    <View style={{ flex: 1, backgroundColor: '#333'}}>
      {
        scannerVisible === true
        ? <React.Fragment>
            <DocumentScanner
              ref={pdfScannerElement}
              style={styles.scanner}
              onPictureTaken={(data) => scanner(data)}
              overlayColor='rgba(211,35,69, 0.7)'
              enableTorch={false}
              quality={1.0}
              useBase64={!true}
              saveOnDevice={true}
              detectionCountBeforeCapture={5}
              detectionRefreshRateInMS={50}
              captureMultiple={!true}
            />
                 
            <TouchableOpacity onPress={handleOnPressVisible} style={[styles.crossOnCam]}>
              <Icon name='close' color='#fff' />
            </TouchableOpacity>
            <TouchableOpacity onPress={handleOnPress} style={[styles.multiImagePickerButton, styles.multiImagePickerButtonOnCamp]}>
              <Text style={styles.multiImagePickerButtonText}>TOMAR FOTO</Text>
              <Icon name='camera-alt' color='#fff' />
            </TouchableOpacity>
          </React.Fragment>

        : <>
            {state.error == true ? (
              <InfoModal
                text={`${
                  state.errorMessage !== undefined
                    ? state.errorMessage
                    : 'Error, las imagenes no pueden estar vacias'
                }`}
                isActive={itemModalOpen}
                handleClose={() => setItemModalOpen(false)}
              />
            ) : (
              <InfoModal
                text={`Documento subido exitosamente.`}
                isActive={itemModalOpen}
                handleClose={() => setItemModalOpen(false)}
              />
            )}
            <View style={{ flexDirection: 'column', justifyContent: 'center', alignItems: 'center' }}>
              <View style={styles.Buttonscontainer}>
                <TouchableOpacity onPress={handleOnPressVisible} style={styles.multiImagePickerButton}>
                  <Text style={styles.multiImagePickerButtonText}>TOMAR FOTO</Text>
                  <Icon name='camera-alt' color='#fff' />
                </TouchableOpacity>
                <TouchableOpacity
                  style={styles.multiImagePickerButton}
                  onPress={() => openImagePicker()}
                  >
                  <Text style={styles.multiImagePickerButtonText}>
                    GALERÍA
                  </Text>
                  <Icon name='photo-album' color='#fff' />
                </TouchableOpacity>
              </View>
              <View style={styles.inputContainer}>
                <RNPickerSelect
                  placeholder={{
                    label: 'Seleccione el documento',
                    value: null,
                    color: '#9EA0A4'
                  }}
                  style={{
                    placeholder: {
                      flexDirection: 'column',
                      justifyContent: 'center',
                      alignItems: 'center',
                      color: 'white',
                      marginLeft: 15
                    },
                    inputAndroid: {
                      color: 'white',
                      width: 270,
                      height: 40,
                      flexDirection: 'column',
                      justifyContent: 'center',
                      alignItems: 'center',
                      color: 'white',
                      marginLeft: 15
                    },
                    inputIOS: {
                      color: 'white',
                      width: 270,
                      height: 40,
                      flexDirection: 'column',
                      justifyContent: 'center',
                      alignItems: 'center',
                      color: 'white',
                      marginLeft: 15,
                      marginTop: 15
                    },
                    iconContainer: {
                      top: 10,
                      left: 270,
                    },
                  }}
                  useNativeAndroidPickerStyle={false}
                  Icon={() => {
                    return <Ionicons name='md-arrow-dropdown' size={20} color='#FFF' />
                  }}
                  onValueChange={newvKindDocument => setvKindDocument(newvKindDocument)}
                  items={
                      [
                        { label: 'Identificación oficial', value: 'D1' },
                        { label: 'Comprobante de domicilio', value: 'D2' },
                        { label: 'Recibo de pensión', value: 'D3' },
                        { label: 'Tarjeta bancaria', value: 'D4' },
                        { label: 'EDO CTA BAN O DET MOV', value: 'D5' },
                        { label: 'Estado de cuenta bancario', value: 'D7' },
                        { label: 'Comp clabe interbancaria', value: 'D8' },
                        { label: 'Opcional 1', value: 'OP1' },
                        { label: 'Opcional 2', value: 'OP2' }
                      ]
                    }
                    />
                </View>
                <Formik
                  enableReinitialize={true}
                  initialValues={{ 
                  fComment: '',
                  }}
                  onSubmit={(values, actions) => {
                    const uComment = values.fComment
                    setvComment(uComment.toUpperCase());
                    CheckTextInput()
                    setTimeout(() => {
                      actions.setSubmitting(false)
                    }, 2000);
                  }}
                  validationSchema={validationSchema}
                  >
                  {({ handleChange, handleBlur, handleSubmit, values, isSubmitting, errors }) => (
                    <>
                    <View style={styles.inputContainer}>
                      <TextInput
                        style={styles.inputs}
                        placeholder="Comentario"
                        placeholderTextColor="white"
                        // onChangeText={newvComment => setvComment(newvComment.toUpperCase())}
                        // value={vComment}
                        underlineColorAndroid="transparent"
                        autoCorrect={false}
                        autoCapitalize="characters"

                        onChangeText={handleChange('fComment')}
                        onBlur={handleBlur('fComment')}
                        value={values.fComment}
                      />
                    </View>
                    {
                      errors.fComment
                      ? <Text style={styles.ErrorM}>{errors.fComment}</Text>
                      : null
                    }
                    <View style={styles.buttonContainer2}>
                      <TouchableOpacity style={styles.sendmultiImagePickerButton} onPress={handleSubmit} title="Submit" disabled={isSubmitting ? true: false}>          
                        {
                        isSubmitting 
                          ? <ActivityIndicator color='#fff' />
                          : <Text style={styles.BTNText}>CARGAR Y CONTINUAR</Text>
                        }
                      </TouchableOpacity>
                    </View>
                    </>
                  )}
                </Formik>
            </View>
            {
            images.length >= 1 
              ? <>    
                {/* <TouchableOpacity
                  style={styles.sendmultiImagePickerButton}
                  onPress={() => upload()}>
                    <Text style={styles.multiImagePickerButtonText}>
                      CARGAR Y CONTINUAR
                    </Text>
                </TouchableOpacity> */}
                <TouchableOpacity
                  style={styles.clearmultiImagePickerButton}
                  onPress={() => cleanImages()}>
                  <Text style={styles.multiImagePickerButtonText}>
                    {/* {images.length >= 1
                        ? 'REEMPLACE FOTOS'
                        : 'TOMAR FOTO'} */}
                    ELIMINAR IMÁGENES
                  </Text>
                </TouchableOpacity>
                </>
              : null
            }
            <ScrollView>
            {
            images
              ? images.map((item, i) => {
                  return (
                    <View key={i} style={styles.imagePreview}>
                       {/* <TouchableOpacity onPress={() => console.log('item removed')} style={[styles.crossOnCam, { height: 20, width: 20, zIndex: 100, bottom: 165, left: 250 }]}>
                          <Icon name='close' color='#fff' size={15}/>
                        </TouchableOpacity> */}
                      <Image style={styles.image} source={{uri: item[0].path}} />
                      
                    </View>
                  );
                })
              : null
            }
            </ScrollView>
          </>
      }
    </View>
  )
}

const styles = StyleSheet.create({
  spinnerTextStyle: {
    color: '#D32345',
  },
  scanner: {
    flex: 1,
    aspectRatio: undefined
  },
  Buttonscontainer: {
    flexDirection: 'row',
    backgroundColor: '#333',
    justifyContent: 'center',
  },
  multiImagePickerView: {
    flex: 1,
    flexDirection: 'column',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#333333',
  },
  multiImagePickerButton: {
    height: 45,
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 5,
    width: 142,
    marginHorizontal: 10,
    borderRadius: 30,
    backgroundColor: '#D32345',
  },
  crossOnCam: {
    alignSelf: 'center',
    position: 'absolute',
    bottom: 105,
    height: 30,
    width: 30,
    borderRadius: 15,
    borderWidth: 1,
    borderColor: 'white',
    justifyContent: 'center',
    alignItems: 'center'
  },
  multiImagePickerButtonOnCamp: {
    alignSelf: 'center',
    position: 'absolute',
    bottom: 32,
  },
  multiImagePickerButtonText: {
    color: 'white',
    fontWeight: '700',
    fontSize: 14,
  },
  sendmultiImagePickerButton: {
    height: 45,
    // flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    marginVertical: 5,
    width: 300,
    marginHorizontal: 10,
    borderRadius: 30,
    backgroundColor: '#D32345',
  },
  clearmultiImagePickerButton: {
    height: 45,
    // flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    marginBottom: 5,
    width: 300,
    marginHorizontal: 10,
    borderRadius: 30,
    backgroundColor: '#D32345',
  },
  imagePreview: {
    width: '75%',
    height: 190,
    marginBottom: 5,
    // flex: 1,
    flexDirection: 'column',
    display: 'flex',
    justifyContent: 'center',
    alignSelf: 'center',
    alignContent: 'center',
    borderRadius: 7,
    borderColor: '#4c4c4c',
    backgroundColor: '#4c4c4c',
    borderWidth: 1.5,
  },
  image: {
    width: '100%',
    height: '100%',
    borderRadius: 7,
  },
  inputContainer: {
    backgroundColor: '#5e5e5e',
    borderRadius: 30,
    width: 300,
    height: 45,
    marginVertical: 5,
    flexDirection: 'row',
    alignItems: 'center',
  },
  BTNLoad: {
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    marginBottom: 20,
    width: 200,
    borderRadius: 30,
    height: 45,
    backgroundColor: '#D32345',
  },
  BTNText: {
    color: 'white',
    fontWeight: 'bold',
  },
  inputs: {
    height: 45,
    marginLeft: 16,
    color: 'white',
    flex: 1,
  },
})

export default withNavigationFocus(IrregularForm);