import React, {useState, useEffect} from 'react';
import ImagePicker from 'react-native-image-crop-picker';
import {
  View,
  Text,
  TouchableOpacity,
  StyleSheet,
  Alert,
  Image,
  ScrollView,
  Dimensions
} from 'react-native';
import {Icon} from 'react-native-elements';
import Spinner from 'react-native-loading-spinner-overlay';
import {useModal} from './useModal';
import InfoModal from './InfoModal';

const {width} = Dimensions.get('window');
let data = new FormData();

const MultiImagePicker = ({state, onSubmit}) => {
  console.log('MultiImagePicker');
  console.log(state);

  const [images, setimages] = useState([]);
  const [items, setitems] = useState([]);
  const [itemModalOpen, setItemModalOpen, toggleModal] = useModal();
  const [Visible, setVisible] = useState(false);

  const openImagePicker = () => {
    ImagePicker.openPicker({
      multiple: true,
      includeBase64: true,
      waitAnimationEnd: true,
      includeExif: true,
      mediaType: 'photo',
      // maxFiles: 2,
    })
      .then(images => {
        images.map((images, index) => {
          data.append({
            uri: images.data,
            path: images.path,
          });

          setimages(data['_parts']);
        });
      })
      .catch(e => Alert.alert(`Error al cargar imágenes: ${e}`));
  };

  const openPhotoPicker = () => {
    ImagePicker.openCamera({
      multiple: false,
      includeBase64: true,
      waitAnimationEnd: true,
    })
      .then((images, index) => {
        data.append({
          uri: images.data,
          path: images.path,
        });

        setimages(data['_parts']);
      })
      .catch(e => Alert.alert(`${e}`));
  };

  useEffect(() => {
    setitems(images);
  }, [images]);

  const upload = () => {
    var newArray = [];
    data['_parts'].forEach(element => {
      const itemJSON = element[0];
      newArray = newArray.concat(itemJSON);
    });

    setVisible(true);
    onSubmit({
      vDocumentB64: newArray,
    });
    toggleModal(true);
  };

  const cleanImages = () => {
    ImagePicker.clean()
      .then(() => {
        console.log('removed all tmp images from tmp directory');
        setimages([]);
        data = new FormData();
        console.log(JSON.stringify(data['_parts']));
      })
      .catch(e => {
        alert(e);
      });
  };

  useEffect(() => {
    let isCancelled = false;
    cleanImages();
    if (Visible && state.errorMessage != '') {
      setVisible(!Visible);
    } else {
      setVisible(false);
    }
    return () => {
      isCancelled = true;
    };
  }, [state]);

  if (Visible) {
    return (
      <Spinner
        size="large"
        visible={Visible}
        color="#D32345"
        textContent={'Un momento por favor'}
        textStyle={styles.spinnerTextStyle}
      />
    );
  }

  return (
    <View style={styles.container}>
      {state.error == true ? (
        <InfoModal
          text={`${
            state.errorMessage !== undefined
              ? state.errorMessage
              : 'Error, las imagenes no pueden estar vacias'
          }`}
          isActive={itemModalOpen}
          handleClose={() => setItemModalOpen(false)}
        />
      ) : (
        <InfoModal
          text={`Documento subido exitosamente.`}
          isActive={itemModalOpen}
          handleClose={() => setItemModalOpen(false)}
        />
      )}
      <View style={styles.Buttonscontainer}>
        <TouchableOpacity
          style={styles.multiImagePickerButton}
          onPress={() => openPhotoPicker()}>
          <Text style={styles.multiImagePickerButtonText}>
            {/* {images.length >= 1
              ? 'REEMPLACE FOTOS'
              : 'TOMAR FOTO'} */}
            TOMAR FOTO
          </Text>
          <Icon name="camera-alt" color="#fff" />
        </TouchableOpacity>

        <TouchableOpacity
          style={styles.multiImagePickerButton}
          onPress={() => openImagePicker()}>
          <Text style={styles.multiImagePickerButtonText}>
            {/* {images.length >= 1
              ? 'REEMPLACE'
              : 'GALERÍA'} */}
            GALERÍA
          </Text>
          <Icon name="photo-album" color="#fff" />
        </TouchableOpacity>
      </View>
      {/* <ScrollView> */}
      {images.length >= 1 ? (
        <>
          <TouchableOpacity
            style={styles.sendmultiImagePickerButton}
            onPress={() => upload()}>
            <Text style={styles.multiImagePickerButtonText}>
              CARGAR Y CONTINUAR
            </Text>
          </TouchableOpacity>

          <TouchableOpacity
            style={styles.clearmultiImagePickerButton}
            onPress={() => cleanImages()}>
            <Text style={styles.multiImagePickerButtonText}>
              ELIMINAR IMÁGENES
            </Text>
          </TouchableOpacity>
          </>
      ) : null}
      {/* </ScrollView> */}

      <ScrollView>  
        {images
          ? items.map((item, i) => {
              console.log(item[0].path);
              return (
                <View key={i} style={styles.imagePreview}>
                  <Image style={styles.image} source={{uri: item[0].path}} />
                </View>
              );
            })
          : null
        }
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  spinnerTextStyle: {
    color: '#D32345',
  },
  container: {
    flex: 1,
    flexDirection: 'column',
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginVertical: 10,
    backgroundColor: '#333',
  },
  Buttonscontainer: {
    flex: 1,
    flexDirection: 'row',
  },
  multiImagePickerView: {
    flex: 1,
    flexDirection: 'column',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#333333',
  },
  multiImagePickerButton: {
    height: 45,
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 20,
    width: 142,
    marginHorizontal: 10,
    borderRadius: 30,
    backgroundColor: '#D32345',
  },
  clearmultiImagePickerButton: {
    height: 45,
    // flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginVertical: 5,
    width: 300,
    marginHorizontal: 10,
    borderRadius: 30,
    backgroundColor: '#D32345',
  },
  sendmultiImagePickerButton: {
    height: 45,
    // flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    width: 300,
    marginVertical: 5,
    marginHorizontal: 10,
    borderRadius: 30,
    backgroundColor: '#D32345',
  },
  multiImagePickerButtonText: {
    color: 'white',
    fontWeight: '700',
    fontSize: 14,
  },
  imagePreview: {
    width: '80%',
    height: 200,
    marginBottom: 10,
    // flex: 1,
    flexDirection: 'column',
    display: 'flex',
    justifyContent: 'center',
    alignSelf: 'center',
    alignItems: 'center',
    borderRadius: 7,
    borderColor: '#4c4c4c',
    backgroundColor: '#4c4c4c',
    borderWidth: 1.5,
  },
  image: {
    width: width -40,
    height: 200,
    borderRadius: 7,
    resizeMode: 'cover'
  },
});

export default MultiImagePicker;
