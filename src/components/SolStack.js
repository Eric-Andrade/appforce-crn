import React from 'react';
import { Platform } from 'react-native';
import { createMaterialTopTabNavigator } from 'react-navigation-tabs';
import Sol1Form from './Sol1Form';
import Sol2Form from './Sol2Form';
import Sol3Form from './Sol3Form';
import Sol4Form from './Sol4Form';

export default AppNavigator = createMaterialTopTabNavigator(
  {
    Sol1: {
      screen: Sol1Form,
      navigationOptions: {
        tabBarLabel: 'Consultas Aprobadas'
      }
    },  
    Sol2: {
      screen: Sol2Form,
      navigationOptions: {
        tabBarLabel: 'Enviadas'
      }
    },  
    Sol3: {
      screen: Sol3Form,
      navigationOptions: {
        tabBarLabel: 'Irregularidades'
      }
    },  
    Sol4: {
      screen: Sol4Form,
      navigationOptions: {
        tabBarLabel: 'Rechazadas'
      }
    }
  },  
  {
    tabBarPosition: 'top',
    initialRouteName: 'Sol1',
    tabBarOptions: {  
      activeTintColor: 'white',
      inactiveTintColor: '#999',
      showIcon: !true,
      showLabel: true,
      upperCaseLabel: false,
      pressColor: '#555',
      style: {  
        backgroundColor:'#333333',
      },
      tabStyle: {
        // backgroundColor:'#ff0000',
        // width: 105,
      },
      labelStyle: {
        fontSize: Platform.OS === 'ios' ? 8 : 9,
      },
      indicatorStyle: {
        borderBottomColor: '#D32345',
        borderBottomWidth: 3,
      }
    },  
  } 
);