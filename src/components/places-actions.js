import * as FileSystem from 'expo-file-system';

export const ADD_IMG = 'ADD_IMG';

export const addImg = image => {
  return async dispatch => {
    const fileName = image.split('/').pop();
    const newPath = FileSystem.documentDirectory + fileName;
    try {
      await FileSystem.moveAsync({
        from: image,
        to: newPath,
      });
    } catch (err) {
      console.log(err);
      throw err;
    }
    dispatch({type: ADD_IMG, placeData: {image: newPath}});
  };
};
