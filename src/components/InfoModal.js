import React from 'react';
import {
  Modal,
  Text,
  TouchableOpacity,
  TouchableHighlight,
  View,
  StyleSheet,
} from 'react-native';

const InfoModal = ({isActive, children, text, handleClose}) => {
  return (
    <TouchableOpacity onPressOut={handleClose}>
      <Modal animationType={'fade'} transparent={true} visible={isActive}>
        <View style={styles.popupOverlay}>
          <View style={styles.popup}>
            <View style={styles.popupContent}>
              <Text style={styles.mod}>{text}</Text>
              {/* <Text>{state.errorMessage}</Text> */}
              {/* {children} */}
              {/* {isActive}  */}
            </View>

            <View style={styles.popupButtons}>
              <TouchableOpacity
                onPress={handleClose}
                style={[styles.buttonContainer, styles.loginButton]}>
                <Text style={styles.txtClose}>OK</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </Modal>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: '#333333',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
  message: {
    margin: 24,
    color: 'white',
    fontSize: 20,
    fontWeight: 'bold',
  },
  popup: {
    backgroundColor: '#3d3d3d',
    marginTop: 150,
    marginHorizontal: 20,
    borderRadius: 7,
  },
  popupOverlay: {
    backgroundColor: '#00000057',
    flex: 1,
    marginTop: 30,
  },
  popupContent: {
    justifyContent: 'center',
    margin: 5,
    height: 180,
    alignItems: 'center',
  },
  mod: {
    fontSize: 18,
    // fontWeight: 'bold',
    textAlign: 'center',
    color: 'white',
    marginTop: 3,
    marginBottom: 0,
    marginLeft: 10,
  },
  popupHeader: {
    marginBottom: 45,
  },
  popupButtons: {
    marginTop: 15,
    flexDirection: 'row',
    borderTopWidth: 1,
    borderColor: '#eee',
    justifyContent: 'center',
  },
  popupButton: {
    flex: 1,
    marginVertical: 16,
  },
  btnClose: {
    height: 20,
    backgroundColor: '#D32345',
    padding: 20,
  },
  modalInfo: {
    textAlign: 'left',
  },
  txtClose: {
    justifyContent: 'center',
    color: 'white',
    textAlign: 'center',
  },
  buttonContainer: {
    height: 45,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 20,
    width: 300,
    borderRadius: 30,
    backgroundColor: 'transparent',
  },
  loginButton: {
    marginTop: 10,
    backgroundColor: '#D32345',
  },
});

export default InfoModal;
