import React, { useState, useEffect } from 'react';
import { FlatList, Animated, StyleSheet, Text, View, TouchableOpacity, ActivityIndicator } from 'react-native';
import ForceApi from '../api/force';
import {navigate} from '../navigationRef';
import NoticiasPost from './NoticiasPost';
import {withNavigationFocus} from 'react-navigation';

const NoticiasList = ({isFocused}) => {
    const [ animationPost ] = useState(new Animated.Value(0));
    const [ posts, setposts ] = useState(null);
    let delayAnimation = 1000;

    useEffect(() => {
        if (isFocused) {
            console.warn('Estoy en NoticiasList');
            setposts(null);
            getData();
        }

        async function getData() {
            const {data} = await ForceApi.post(`/ApplicationNotificationsController.php`);
            await setposts(data.publication); //temporal, hasta que la data de la api se regule.
            if (data.publication.length > 0) {
                Animated.spring(animationPost, {
                    toValue: 1,
                    tension: 5,
                    useNativeDriver: true
                }).start();
            }
        }
        getData();
        
    }, [isFocused]);
    
    const _onPress = (item) => {
        navigate('Post', {
            itemTitle: item.publication_title
        })
    }

    if (posts === null) {
        return (
            <View style={styles.loadingView}>
                <ActivityIndicator size='small' color='#D32345' style={{marginVertical: 5}}/>
                <Text style={styles.loadingText}>Buscando noticias...</Text>
            </View>
        );
    }

    return (
        <View style={styles.container}>
        {
            posts 
            ?   <FlatList
                    showsVerticalScrollIndicator={false}
                    data={posts}
                    keyExtractor={item => item.id}
                    initialNumToRender={5}
                    style={{ height: '100%'}}
                    renderItem={({ item }) => {
                        delayAnimation = delayAnimation + 1000;
                        const translateY = animationPost.interpolate({
                            inputRange: [0, 1],
                            outputRange: [delayAnimation, 1],
                            extrapolate: 'clamp'
                        })
                        return (
                            <Animated.View key={item.id} style={[styles.tarjeta, { transform: [{ translateY }] } ]}>
                                {/* <TouchableOpacity
                                    onPress={() => _onPress(item)}> */}
                                    <NoticiasPost item={item} />
                                {/* </TouchableOpacity> */}
                            </Animated.View>
                        );
                    }}
                    initialNumToRender={6}
                    // refreshControl={
                    //     <RefreshControl
                    //         refreshing={fetching}
                    //         onRefresh={() => handleRefreshing()}
                    //         // title="Pull to refresh"
                    //         tintColor={themeContext.EXITUS_COLOR}
                    //         titleColor={themeContext.EXITUS_COLOR}
                    //         colors={[themeContext.EXITUS_COLOR]} 
                    //     />
                    // }
                />
            :   <View style={styles.emptyListView}>
                    <Text style={styles.emptyListText}>
                        Aún no se encuentran publicaciones.
                    </Text>
                </View>
        }
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
    },
    tarjeta: {
        shadowOpacity: 0.37,
        shadowRadius: 7.49,
        elevation: 5,
        marginVertical: 5,
        marginHorizontal: 30,
        flexBasis: '46%',
        flexDirection: 'column',
        backgroundColor: '#484848',
        borderRadius: 10,
    },
    solicitudview: {
        backgroundColor: '#6d6d6d',
        color: 'white',
        height: 40,
        fontSize: 20,
        // borderRadius:10,
        borderTopLeftRadius: 10,
        borderTopRightRadius: 10,
        borderBottomLeftRadius: 10,
        borderBottomRightRadius: 10,
        flexDirection: 'column',
        justifyContent: 'center',
    },
    clave: {
        fontSize: 14,
        textAlign: 'left',
        color: '#9e9e9e',
        marginTop: 3,
        marginTop: 5,
        marginHorizontal: 15,
        fontWeight: '500',
    },
    emptyListView: {
        flex: 1,
        flexDirection: 'column',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
    },
    emptyListText: {
        color: 'white',
        fontSize: 14,
        fontWeight: '600',
    },
    spinnerTextStyle: {
        color: '#fff',
        marginTop: 50
    },
    loadingView: {
      flex: 1,
      flexDirection: 'column',
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: '#333'
    },
    loadingText: {
      color: 'white',
      fontSize: 14,
      fontWeight: '600',
    },
});

export default withNavigationFocus(NoticiasList);