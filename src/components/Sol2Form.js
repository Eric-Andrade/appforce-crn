import React, {useState, useEffect, useContext} from 'react';
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableOpacity,
  Image,
  ScrollView,
  Picker,
  FlatList,
  ActivityIndicator
} from 'react-native';
import {withNavigationFocus} from 'react-navigation';
import ForceApi from '../api/force';
import Spinner from 'react-native-loading-spinner-overlay';
import {useModal} from './useModal';
import InfoModal from './InfoModal';
import AsyncStorage from '@react-native-community/async-storage';

const Sol2Form = ({isFocused, onSubmit}) => {
  const [request, setrequest] = useState(null);
  const [itemModalOpen, setItemModalOpen, toggleModal] = useModal();
  const [Visible, setVisible] = useState(false);
  const [message, setmessage] = useState('');
  const [success, setsuccess] = useState('');
  const [status, setstatus] = useState('');

  async function getList() {
    const vKindSearch = 3;
    const vCreationUser = await AsyncStorage.getItem('id');
    const {data} = await ForceApi.post(`/GetRequestController.php`, {
      vCreationUser,
      vKindSearch,
    });
    setrequest(data.request === undefined ? [] : data.request);
  }

  async function Reenvio({algo}) {
    const vSolicitudeId = algo;
    // const vSolicitudeId = 6;
    setVisible(true)
    const {data} = await ForceApi.post(`/ElectronicSignatureController.php`, {
      vSolicitudeId,
    });
    console.log('adadadadadaa', vSolicitudeId, algo);
    console.log(data);
    
    setmessage(data.message);
    setsuccess(data.success);
    setVisible(false)
    toggleModal(true);
    // setTimeout(() => {
    //   toggleModal(true);
    // }, 1000);
  }

  useEffect(() => {
    if (isFocused) {
      setrequest(null);
      getList();
    }
  }, [isFocused]);

  if (request === null) {
    return (
      <View style={styles.loadingView}>
        <ActivityIndicator size='small' color='#D32345' style={{marginVertical: 5}}/>
        <Text style={styles.loadingText}>Buscando solicitudes enviadas...</Text>
      </View>
    );
    // return (
    //   <Spinner
    //     size='large'
    //     visible={true}
    //     color='#D32345'
          // overlayColor='rgba(0, 0, 0, 0.5)'
    //     textContent={'Buscando solicitudes enviadas...'}
    //     textStyle={styles.spinnerTextStyle}
    //   />
    // );
  }

  return (
    <View style={{ backgroundColor: '#333333', flex: 1 }}>
      {request.length >= 1 ? (
        <View style={styles.Lcontainer}>
          <InfoModal
            text={`${success == true ? message : 'No se pudo enviar la solicitud'}`}
            isActive={itemModalOpen}
            handleClose={() => setItemModalOpen(false)}
          />
          <Spinner
            size='large'
            visible={Visible}
            color='#fff'
            textContent={'Un momento por favor'}
            textStyle={styles.spinnerTextStyle}
          />
          <FlatList
            columnWrapperStyle={styles.listContainer}
            data={request}
            keyExtractor={item => {
              return item.solicitude_id;
            }}
            renderItem={({item}) => {
              return (
                <View style={styles.tarjeta}>
                  <View style={styles.solicitudview}>
                    <Text style={styles.solicitud}>
                      Id de Zell: {item.zell_id}
                    </Text>
                  </View>
                  <View style={styles.card}>
                    <Text style={styles.clave}>Nombre: {item.full_name}</Text>
                    <Text style={styles.clave}>Status: {item.zell_status}</Text>
                  </View>

                  
                  {
                    item.zell_status === 'Pre Autorizada FE' ?
                  (<TouchableOpacity
                    style={[styles.buttonContainer, styles.loginButton]}
                    onPress={() => Reenvio({algo: item.solicitude_id})}>
                    <Text style={styles.loginText}>
                      REENVIAR FIRMA ELECTRONICA
                    </Text>
                  </TouchableOpacity>) : null}


                </View>
              );
            }}
          />
        </View>
      ) : (
        <View style={styles.emptyListView}>
          <Text style={styles.emptyListText}>
            Sin solicitudes enviadas por el momento.
          </Text>
        </View>
      )}
    </View>
  );
};

Sol2Form.navigationOptions = {
  headerShown: false,
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: '#333333',
  },
  logo: {
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    marginTop: 40,
    marginBottom: 20,
    marginLeft: 10,
  },
  head: {
    backgroundColor: '#000',
    borderRadius: 20,
  },
  textIniciar: {
    marginTop: 30,
    color: 'white',
    width: 300,
    height: 45,
    marginBottom: 5,
    fontSize: 20,
    fontWeight: 'bold',
    textAlign: 'left',
    marginLeft: 20,
  },
  buttonContainer: {
    flex: 1,
    height: 35,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 20,
    borderRadius: 30,
    backgroundColor: 'transparent',
    marginVertical: 10,
    marginHorizontal: 30,
    flexBasis: '46%',
  },
  inputIcon: {
    marginRight: 20,
    justifyContent: 'center',
  },
  loginButton: {
    backgroundColor: '#D32345',
  },
  loginText: {
    color: 'white',
    fontWeight: 'bold',
  },
  Lcontainer: {
    backgroundColor: '#333333',
  },
  card: {
    backgroundColor: '#484848',
    flexBasis: '46%',
    flexDirection: 'column',
    minHeight: 100,
    height: '100%',
    borderRadius: 8,
  },
  solicitud: {
    fontSize: 17,
    textAlign: 'left',
    color: 'white',
    fontWeight: '500',
    marginHorizontal: 15,
    marginTop: 5,
  },
  solicitudview: {
    backgroundColor: '#6d6d6d',
    color: 'white',
    height: 40,
    fontSize: 20,
    // borderRadius:10,
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
    borderBottomLeftRadius: 0,
    borderBottomRightRadius: 0,
    flexDirection: 'column',
    justifyContent: 'center',
  },
  ocointainer: {
    marginTop: 30,
    marginBottom: 20,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  pickcontainer: {
    flexDirection: 'row',
    borderRadius: 10,
    borderWidth: 1,
    borderColor: '#696969',
    backgroundColor: '#696969',
    flexBasis: '90%',
  },
  pick: {
    backgroundColor: '#696969',
    color: 'white',
    height: 40,
    fontSize: 20,
    borderRadius: 50,
    flexBasis: '90%',
    marginLeft: 15,
  },
  tarjeta: {
    shadowOpacity: 0.37,
    shadowRadius: 7.49,
    elevation: 12,
    marginVertical: 10,
    marginHorizontal: 30,
    flexBasis: '46%',
    flexDirection: 'column',
    backgroundColor: '#484848',
    borderRadius: 10,
  },
  clave: {
    fontSize: 14,
    textAlign: 'left',
    color: '#9e9e9e',
    marginBottom: 0,
    marginTop: 5,
    marginHorizontal: 15,
    fontWeight: '500',
  },
  emptyListView: {
    flex: 1,
    flexDirection: 'column',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
  emptyListText: {
    color: 'white',
    fontSize: 14,
    fontWeight: '600',
  },
  spinnerTextStyle: {
    color: '#fff',
    marginTop: 50
  },
  loadingView: {
    flex: 1,
    flexDirection: 'column',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#333'
  },
  loadingText: {
    color: 'white',
    fontSize: 14,
    fontWeight: '600',
  },
});

// export default Sol2Form;
export default withNavigationFocus(Sol2Form);
