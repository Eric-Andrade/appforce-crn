import React, {useState, useEffect, useContext} from 'react';
import {
  View,
  TouchableOpacity,
  Image,
  Text,
  StyleSheet,
  TextInput,
  ScrollView,
  ActivityIndicator
} from 'react-native';
import ImagePicker from 'react-native-image-crop-picker';
import { Icon } from 'react-native-elements';
import { useModal } from './useModal';
import InfoModal from './InfoModal';
import Spinner from 'react-native-loading-spinner-overlay';
import { Formik } from 'formik';
import * as yup from 'yup';
import MitekFunctionality from './Mitek';

const validationSchema = yup.object().shape({
  fCellphone: yup
          .string()
          .matches(/^[0-9]*$/, 'Ingrese únicamente números')
          .min(10, 'Celular debe tener 10 dígitos')
          .max(10, 'Celular debe tener 10 dígitos')
          .typeError('Ingrese únicamente números')
          .required('Este campo no puede estar vacío'),
  fPhone: yup
          .string()
          .matches(/^[0-9]*$/, 'Ingrese únicamente números')
          // .min(10, 'Teléfono debe tener 10 dígitos')
          .max(10, 'Teléfono debe tener 10 dígitos')
          .typeError('Ingrese únicamente números'),
          // .required('Este campo no puede estar vacío'),
  fEmail: yup
          .string()
          // .matches(/^[A-Za-z0-9 ]*$/, 'No ingrese caracteres especiales ni con tildes')
          .email('Ingrese un email válido'),
});

const OcrImg = ({state, onSubmit, onImageTaken}) => {
  console.log('..................OcrImg state..................: ', state);

  const [itemModalOpen, setItemModalOpen, toggleModal] = useModal();
  const [pickedImageFront, setvpickedImageFront] = useState();
  const [vCredentialFront, setvCredentialFront] = useState();
  const [pickedImageBack, setvpickedImageBack] = useState();
  const [vCredentialBack, setvCredentialBack] = useState();
  const [vCellphone, setvCellphone] = useState('');
  const [vEmail, setvEmail] = useState('');
  const [isLoading, setisLoading] = useState(false);
  const [Visible, setVisible] = useState(false);

  const [vPhone, setvPhone] = useState('');
  const [cCellphone, setcCellphone] = useState(false);
  const [cEmail, setcEmail] = useState(false);
  const [stateError, setstateError] = useState();

  // useEffect(() => {
  //   if (isLoading != false) {
  //     setInterval(() => {
  //       setisLoading(false);
  //     }, 3000);
  //   }
  // }, [isLoading]);

  enviar = () => {
    if (vCellphone != '') {
      setisLoading(true);
      onSubmit({vCredentialFront, vCredentialBack, vCellphone, vEmail, vPhone});
      setVisible(true);
      setisLoading(false);
      toggleModal(true);
    }
  };

  const openImageFrontPicker = () => {
    ImagePicker.openPicker({
      multiple: false,
      includeBase64: true,
      waitAnimationEnd: true,
      includeExif: true,
      mediaType: 'photo',
      // maxFiles: 2,
    })
      .then(image => {
        setvpickedImageFront(image.path);
        setvCredentialFront(image.data);
      })
      .catch(e => Alert.alert(`Error al cargar imagen frontal: ${e}`));
  };

  const openImageBackPicker = () => {
    ImagePicker.openPicker({
      multiple: false,
      includeBase64: true,
      waitAnimationEnd: true,
      includeExif: true,
      mediaType: 'photo',
      // maxFiles: 2,
    })
      .then(image => {
        setvpickedImageBack(image.path);
        setvCredentialBack(image.data);
      })
      .catch(e => Alert.alert(`Error al cargar imagen frontal: ${e}`));
  };


  useEffect(() => {
    let isCancelled = false;
    if (Visible && state.errorMessage != '') {
      setVisible(!Visible);
    } else {
      setVisible(false);
    }
    return () => {
      isCancelled = true;
    };
  }, [state]);

  const openPhotoFrontPicker = () => {
    MitekFunctionality.launchMitek(
      (result) => {
        var image = `data:image/gif;base64,${result}`;
  
        setvpickedImageFront(image);
        setvCredentialFront(result);
      },
      (errorMsg) => {
        alert(errorMsg);
      }
    ); 
  };

  const openPhotoBackPicker = () => {
    MitekFunctionality.launchMitek(
      (result) => {
        var image = `data:image/gif;base64,${result}`;
  
        setvpickedImageBack(image);
        setvCredentialBack(result);
      },
      (errorMsg) => {
        alert(errorMsg);
      }
    ); 
  };

  if (Visible) {
    return (
      <Spinner
        size="large"
        visible={Visible}
        color="#D32345"
        textContent={'Este proceso puede tardar más de un minuto.'}
        textStyle={styles.spinnerTextStyle}
      />
    );
  }

  return (
    <ScrollView>
      {state.error == true ? (
        <InfoModal
          text={`${state.errorMessage}`}
          isActive={itemModalOpen}
          handleClose={() => setItemModalOpen(false)}
        />
      ) : null}
      <View style={styles.imagePicker}>
        <View style={styles.imagePreview}>
          {!pickedImageFront ? (
            <Text style={styles.DocText}>Imagen Frontal</Text>
          ) : (
            <Image style={styles.image} source={{uri: pickedImageFront}} />
          )}
        </View>

        <View style={styles.buttonContainer2}>
          <TouchableOpacity
            style={styles.BTN}
            onPress={() => openPhotoFrontPicker()}>
            <Text style={styles.BTNText}>TOMAR FOTO</Text>
            <Icon name="camera-alt" color="#fff" />
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.BTN}
            onPress={() => openImageFrontPicker()}>
            <Text style={styles.BTNText}>GALERÍA</Text>
            <Icon name="photo-album" color="#fff" />
          </TouchableOpacity>
        </View>

        <View style={styles.imagePreview}>
          {!pickedImageBack ? (
            <Text style={styles.DocText}>Imagen Trasera</Text>
          ) : (
            <Image style={styles.image} source={{uri: pickedImageBack}} />
          )}
        </View>

        <View style={styles.buttonContainer2}>
          <TouchableOpacity
            style={styles.BTN}
            onPress={() => openPhotoBackPicker()}>
            <Text style={styles.BTNText}>TOMAR FOTO</Text>
            <Icon name="camera-alt" color="#fff" />
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.BTN}
            onPress={() => openImageBackPicker()}>
            <Text style={styles.BTNText}>GALERÍA</Text>
            <Icon name="photo-album" color="#fff" />
          </TouchableOpacity>
        </View>
        <Formik
          enableReinitialize={true}
          initialValues={{
            fCellphone: '',
            fPhone: '',
            fEmail: '',
          }}
          onSubmit={(values, actions) => {
          // alert(JSON.stringify(values))
            const uEmail = values.fEmail;  
            setvCellphone(values.fCellphone);
            setvPhone(values.fPhone);
            setvEmail(uEmail.toUpperCase());
            enviar();
            setTimeout(() => {
                actions.setSubmitting(false);
            }, 2000);
          }}
          validationSchema={validationSchema}
          >
          {({ handleChange, handleBlur, handleSubmit, values, isSubmitting, errors }) => (
          <>
            <View style={styles.imagePicker2}>
              <View style={styles.inputContainer}>
                <TextInput
                  style={styles.inputs}
                  placeholder="Teléfono celular"
                  placeholderTextColor="white"
                  underlineColorAndroid="transparent"
                  // onChangeText={newvCellphone =>
                  //   setvCellphone(newvCellphone.toUpperCase())
                  // }
                  // value={vCellphone}
                  autoCorrect={false}
                  autoCapitalize="characters"
                  keyboardType={'numeric'}
                  maxLength={10}
                  
                  onChangeText={handleChange('fCellphone')}
                  // onBlur={handleBlur('fCellphone')}
                  onBlur={() => setvCellphone(values.fCellphone)}
                  value={values.fCellphone}
                />
              </View>
              {
                errors.fCellphone
                ? <Text style={styles.ErrorM}>{errors.fCellphone}</Text>
                : null
              }
              {/* {cCellphone == true ? (
                <Text style={styles.ErrorM}>Este campo no puede estar vacío</Text>
              ) : null} */}
              <View style={styles.inputContainer}>
                <TextInput
                  style={styles.inputs}
                  placeholder="Teléfono fijo"
                  placeholderTextColor="white"
                  underlineColorAndroid="transparent"
                  // onChangeText={newPhone => setvPhone(newPhone.toUpperCase())}
                  // value={vPhone}
                  autoCorrect={false}
                  keyboardType={'numeric'}
                  maxLength={10}
                  
                  onChangeText={handleChange('fPhone')}
                  // onBlur={handleBlur('fPhone')}
                  onBlur={() => setvPhone(values.fPhone)}
                  value={values.fPhone}
                />
              </View>
              {
                errors.fPhone
                ? <Text style={styles.ErrorM}>{errors.fPhone}</Text>
                : null
              }
              <View style={styles.inputContainer}>
                <TextInput
                  style={styles.inputs}
                  placeholder="Correo electrónico"
                  placeholderTextColor="white"
                  underlineColorAndroid="transparent"
                  // onChangeText={newvEmail => setvEmail(newvEmail)}
                  // value={vEmail}
                  autoCorrect={false}
                  keyboardType='email-address'
                  
                  onChangeText={handleChange('fEmail')}
                  // onBlur={handleBlur('fEmail')}
                  onBlur={() => setvEmail(values.fEmail)}
                  value={values.fEmail}
                />
              </View>
              {
                errors.fEmail
                ? <Text style={styles.ErrorM}>{errors.fEmail}</Text>
                : null
              }
              <View style={styles.buttonContainer2}>
                <TouchableOpacity style={styles.BTNLoad} onPress={handleSubmit} title="Submit" disabled={isSubmitting ? true: false}>          
                  {
                  isSubmitting 
                  ? <ActivityIndicator color='#fff' />
                  : <Text style={styles.BTNText}>CARGAR Y CONTINUAR</Text>
                  }
                </TouchableOpacity>
              </View>
            </View>
          </>
          )}
        </Formik>
      </View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  spinnerTextStyle: {
    color: '#D32345',
    fontSize: 14,
  },
  imagePicker: {
    alignContent: 'center',
    justifyContent: 'center',
  },
  imagePicker2: {
    alignContent: 'center',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  imagePreview: {
    width: '80%',
    height: 200,
    marginBottom: 20,
    justifyContent: 'center',
    alignItems: 'center',
    alignContent: 'center',
    // borderColor: '#ccc',
    borderRadius: 7,
    borderColor: '#4c4c4c',
    backgroundColor: '#4c4c4c',
    borderWidth: 1,
    marginLeft: 45,
  },
  image: {
    width: '100%',
    height: '100%',
  },
  BTN: {
    marginLeft: 20,
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 20,
    width: 142,
    borderRadius: 30,
    height: 45,
    backgroundColor: '#D32345',
  },
  BTNLoad: {
    // marginLeft: '28%',
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 20,
    width: 200,
    borderRadius: 30,
    height: 45,
    backgroundColor: '#D32345',
  },
  BTNText: {
    color: 'white',
    fontWeight: 'bold',
  },
  buttonContainer2: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  inputContainer: {
    backgroundColor: '#5e5e5e',
    borderRadius: 30,
    width: 300,
    height: 45,
    marginBottom: 20,
    flexDirection: 'row',
    alignItems: 'center',
  },
  inputs: {
    height: 45,
    marginLeft: 16,
    color: 'white',
    flex: 1,
  },
  ErrorM: {
    color: '#D32345',
    fontWeight: 'bold',
    marginBottom: 15,
  },
  DocText: {
    color: '#b3b4b5',
  },
});

export default OcrImg;
/*
const takeImageHandler = async () => {
    const hasPermissions = await verifyPermissions();
    if (!hasPermissions){
        return;
    }
   ImagePicker.launchCameraAsync();
};*/
