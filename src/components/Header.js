import React from 'react';
import { Image, View, StyleSheet, StatusBar, Text, Platform } from 'react-native';

const Header = () => {
    return (
        <>
            <View style={styles.head}>
                {/* <StatusBar backgroundColor='black' barStyle={Platform.OS === 'ios' ? 'dark-content' : 'light-content' } /> */}
                <Image
                    style={styles.logo}
                    source={require('../../assets/Logo-Grande.png')}
                />                
                {/* <Text style={styles.versionNumber}>v2.0.6</Text> */}
            </View>
        </>
    )
}

const styles = StyleSheet.create({
    head: {
        backgroundColor: '#000',
        // borderRadius:20,
        borderTopLeftRadius: 0,
        borderTopRightRadius: 0,
        // borderBottomLeftRadius: 20,
        // borderBottomRightRadius: 20,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'flex-end',
    },
    logo: {
        height: 70,
        width: 110,
        justifyContent: 'flex-start',
        alignItems: 'flex-start',
        marginTop: 40,
        marginBottom: 20,
        marginLeft: 16,
    },
    versionNumber: {
      color: '#b3b4b5',
      marginBottom: 10,
      fontSize: 12,
      marginHorizontal: 10,
    },
})

export default Header;