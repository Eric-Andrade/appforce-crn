import React, {useState, useEffect} from 'react';
import ImagePicker from 'react-native-image-crop-picker';
import {
  View,
  Text,
  TouchableOpacity,
  StyleSheet,
  Alert,
  Image,
  ScrollView,
  NativeModules,
  Platform, 
  SafeAreaView
} from 'react-native';
import {Icon} from 'react-native-elements';
import Spinner from 'react-native-loading-spinner-overlay';
import {useModal} from './useModal';
import InfoModal from './InfoModal';

let data = new FormData();

const OneImagePicker = ({state, onSubmit}) => {
  console.log('OneImagePicker');
  console.log(state);

  const [pickedImage, setvpickedImage] = useState();
  const [vDocumentB64, setvDocumentB64] = useState();
  const [images, setimages] = useState([]);
  const [items, setitems] = useState([]);
  const [itemModalOpen, setItemModalOpen, toggleModal] = useModal();
  const [Visible, setVisible] = useState(false);

  const openImagePicker = () => {
    ImagePicker.openPicker({
      multiple: false,
      includeBase64: true,
      waitAnimationEnd: true,
      includeExif: true,
      mediaType: 'photo',
      // maxFiles: 2,
    })
      .then(image => {
        setvpickedImage(image.path);
        setvDocumentB64([
          {
            uri: image.data,
          },
        ]);
      })
      .catch(e => Alert.alert(`Error al cargar imágenes: ${e}`));
  };

  const openPhotoPicker = () => {
    if(Platform.OS ==='android'){
      
      //Esta es una promesa que regresa la iamgen en base64
      NativeModules.Facial.startFacial().then(res => {
          // console.log(`data:image/jpeg;base64,${res}`)
          let value = JSON.parse(res)
          let imageLoad = value.image
          let imageBase64 = imageLoad.split(',')
          console.log('imageBase64: ',imageBase64[1]);
          
          // setimageSelfie(imageLoad)
          setvpickedImage(imageLoad);
          setvDocumentB64([
            {
              uri: imageBase64[1],
            },
          ]);
          console.log('vDocumentB64: ',vDocumentB64)
      }).catch(err => console.log('err',err));
    } else {
      //esto es apenas la promesa para poder utilizarlo en IOS
    //   NativeModules.Facial.startFacial().then(res => {
    //     // console.log(`data:image/jpeg;base64,${res}`)
    //     let value = JSON.parse(res)
    //     let imageLoad = value.image
    //     let imageBase64 = imageLoad.split(',')
    //     console.log('imageBase64: ',imageBase64[1]);
        
    //     // setimageSelfie(imageLoad)
    //     setvpickedImage(imageLoad);
    //     setvDocumentB64([
    //       {
    //         uri: imageBase64[1],
    //       },
    //     ]);
    //     console.log('vDocumentB64: ',vDocumentB64)
    // }).catch(err => console.log('err',err));


        ImagePicker.openCamera({
        multiple: false,
        includeBase64: true,
        waitAnimationEnd: true,
      })
        .then(image => {
          setvpickedImage(image.path);
          setvDocumentB64([
            {
              uri: image.data,
            },
          ]);
        })
        .catch(e => Alert.alert(`${e}`));

    }
  }

  useEffect(() => {
    setitems(images);
  }, [images]);

  const upload = () => {
    setVisible(true);
    onSubmit({
      vDocumentB64: vDocumentB64,
    });
    toggleModal(true);
  };

  const cleanImages = () => {
    ImagePicker.clean()
      .then(() => {
        console.log('removed all tmp images from tmp directory');
        setimages([]);
        data = new FormData();
        console.log(JSON.stringify(data['_parts']));
      })
      .catch(e => {
        alert(e);
      });
  };

  useEffect(() => {
    let isCancelled = false;
    if (Visible && state.errorMessage != '') {
      setVisible(!Visible);
    } else {
      setVisible(false);
    }
    return () => {
      isCancelled = true;
    };
  }, [state]);

  // useEffect(() => {
  // if (Visible) {
  //     setTimeout(() => {
  //     // toggleModal(false)
  //     setVisible(false);
  //     }, 100000);

  // }
  // }, [Visible]);

  if (Visible) {
    return (
      <Spinner
        size="large"
        visible={Visible}
        color="#D32345"
        textContent={'Un momento por favor'}
        textStyle={styles.spinnerTextStyle}
      />
    );
  }

  return (
    <View style={styles.container}>
      {state.error == true ? (
        <InfoModal
          text={`${
            state.errorMessage !== undefined
              ? state.errorMessage
              : 'Error, las imagenes no pueden estar vacias'
          }`}
          isActive={itemModalOpen}
          handleClose={() => setItemModalOpen(false)}
        />
      ) : null}
      <View style={styles.imagePreview}>
        {!pickedImage ? (
          <Text>No hay imagen</Text>
        ) : (
          <Image style={styles.image} source={{uri: pickedImage}} />
        )}
      </View>
      <View style={styles.Buttonscontainer}>
        <TouchableOpacity
          style={styles.multiImagePickerButton}
          onPress={() => openPhotoPicker()}>
          <Text style={styles.multiImagePickerButtonText}>
            TOMAR FOTO
          </Text>
          <Icon name="camera-alt" color="#fff" />
        </TouchableOpacity>
        {/* <TouchableOpacity
          style={styles.multiImagePickerButton}
          onPress={() => openPhotoPicker()}>
          <Text style={styles.multiImagePickerButtonText}>
            TOMAR FOTO
          </Text>
          <Icon name="camera-alt" color="#fff" />
        </TouchableOpacity> */}

        <TouchableOpacity
          style={styles.multiImagePickerButton}
          onPress={() => openImagePicker()}>
          <Text style={styles.multiImagePickerButtonText}>
            {/* {images.length >= 1
              ? 'REEMPLACE'
              : 'GALERÍA'} */}
            GALERÍA
          </Text>
          <Icon name="photo-album" color="#fff" />
        </TouchableOpacity>
      </View>
      {/* <ScrollView> */}
      {images.length >= 1 ? (
        <TouchableOpacity
          style={styles.clearmultiImagePickerButton}
          onPress={() => cleanImages()}>
          <Text style={styles.multiImagePickerButtonText}>
            {/* {images.length >= 1
                ? 'REEMPLACE FOTOS'
                : 'TOMAR FOTO'} */}
            ELIMINAR IMÁGENES
          </Text>
        </TouchableOpacity>
      ) : null}
      {/* </ScrollView> */}

      <TouchableOpacity
        style={styles.sendmultiImagePickerButton}
        onPress={() => upload()}>
        <Text style={styles.multiImagePickerButtonText}>
          CARGAR Y CONTINUAR
        </Text>
      </TouchableOpacity>

      {/* {
      items.length >= 1
        ? items.map((item, i) => {
            return (
              <View key={i} style={styles.imagePreview}>
                <Image style={styles.image} source={{uri: item[0].uri}} />
              </View>
            );
          })
        : null
      } */}
    </View>
  );
};

const styles = StyleSheet.create({
  spinnerTextStyle: {
    color: '#D32345',
  },
  container: {
    flexDirection: 'column',
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginVertical: 10,
    backgroundColor: '#333',
  },
  Buttonscontainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  multiImagePickerView: {
    flex: 1,
    flexDirection: 'column',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#333333',
  },
  multiImagePickerButton: {
    height: 45,
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 20,
    width: 142,
    marginHorizontal: 10,
    borderRadius: 30,
    backgroundColor: '#D32345',
  },
  clearmultiImagePickerButton: {
    height: 45,
    // flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 20,
    width: 300,
    marginHorizontal: 10,
    borderRadius: 30,
    backgroundColor: '#D32345',
  },
  sendmultiImagePickerButton: {
    height: 45,
    // flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 20,
    width: 300,
    marginHorizontal: 10,
    borderRadius: 30,
    backgroundColor: '#D32345',
  },
  multiImagePickerButtonText: {
    color: 'white',
    fontWeight: '700',
    fontSize: 14,
  },
  imagePreview: {
    width: '80%',
    height: 200,
    marginBottom: 20,
    justifyContent: 'center',
    alignItems: 'center',
    alignContent: 'center',
    // borderColor: '#ccc',
    borderRadius: 7,
    borderColor: '#4c4c4c',
    backgroundColor: '#4c4c4c',
    borderWidth: 1,
    // marginLeft: 45
  },
  image: {
    width: '100%',
    height: '100%',
    borderRadius: 7,
  },
});

export default OneImagePicker;
