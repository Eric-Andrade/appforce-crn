import React, {useState, useEffect, useContext} from 'react';
import {ActivityIndicator, SafeAreaView, Text } from 'react-native';
import NetInfo from '@react-native-community/netinfo';

const ETANetInfo = () => {
  const [isInternetReachable, setisInternetReachable] = useState(true);

  useEffect(() => {
    const unsubscribe = NetInfo.addEventListener((state) => {
      setisInternetReachable(state.isInternetReachable);
      console.log('isInternetReachable: ', state.isInternetReachable);
    });

    return () => {
      unsubscribe();
    };
  }, []);

  if (isInternetReachable) {
    return null;
  }

  return (
    <SafeAreaView style={{
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#D32345',
        paddingVertical: 10,
    }}>
      <Text
        style={{
            fontSize:14,
            fontWeight:'400',
            color:'white',
            textAlign:'center'
        }}
        >
        Sin internet, reconectando {'  '}
      </Text>
      <ActivityIndicator
        size='small'
        color='white'
      />
    </SafeAreaView>
  );
};

export default ETANetInfo;
