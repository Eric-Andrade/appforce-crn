import React, {useState, useEffect, useRef} from 'react';
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableOpacity,
  Image,
  ActivityIndicator,
  Keyboard,
  TouchableWithoutFeedback,
  KeyboardAvoidingView,
  Platform
} from 'react-native';
import Spinner from 'react-native-loading-spinner-overlay';
import {useModal} from './useModal';
import InfoModal from './InfoModal';
import Entypo from 'react-native-vector-icons/Entypo';
import { Formik } from 'formik';
import * as yup from 'yup';

const validationSchema = yup.object().shape({
  fCellphone: yup
      .string()
      .matches(/^[0-9]*$/, 'Ingrese únicamente números')
      .min(10, 'Teléfono celular debe tener 10 dígitos')
      .max(10, 'Teléfono celular debe tener 10 dígitos')
      .typeError('Ingrese únicamente números')
      .required('Este campo no puede estar vacío'),
  fPassword: yup
      .string()
      // .matches(/^[A-Za-z0-9]*$/, 'No ingrese caracteres especiales ni con tildes')
      .required('Este campo no puede estar vacío')
      .uppercase(),
})

export default function AuthForm({state, errorMessage, onSubmit}) {
  const [itemModalOpen, setItemModalOpen, toggleModal] = useModal();
  const [vCellphone, setvCellphone] = useState('');
  const [vPassword, setvPassword] = useState('');
  const [toogleEye, settoogleEye] = useState(true);
  const [secureTextEntry, setSecureTextEntry] = useState(true);
  const [isLoading, setisLoading] = useState(false);
  const [cellError, setcellError] = useState(false);
  const [passError, setpassError] = useState(false);
  const [Disable] = useState(false);
  const [Visible, setVisible] = useState(false);
  const [stateError, setstateError] = useState(false);

  const onPassPress = () => {
    setSecureTextEntry(!secureTextEntry);
    settoogleEye(!toogleEye)
  };

  const check = () => {
    setstateError(!stateError);
    this.CheckTextInput();
  };

  toogle = () => {
    toggleModal(true);
  };

  CheckTextInput = () => {
    if (vCellphone != '') {
      setcellError(false);
    } else {
      setcellError(true);
    }

    if (vPassword != '') {
      setpassError(false);
    } else {
      setpassError(true);
    }

    if (vCellphone != '' && vPassword != '') {
      console.log('entro');
      setisLoading(true);
      setcellError(false);
      setpassError(false);
      onSubmit({vCellphone, vPassword});
      setVisible(true);
      toggleModal(true);
    }
  };

  useEffect(() => {
    let isCancelled = false;
    if (Visible && state.errorMessage != '') {
      setVisible(!Visible);
    } else {
      setVisible(false);
    }
    return () => {
      isCancelled = true;
    };
  }, [state]);

  if (Visible) {
    return (
      <Spinner
        size="large"
        visible={Visible}
        color="#D32345"
        textContent={'Un momento por favor'}
        textStyle={styles.spinnerTextStyle}
      />
    );
  }

  return (
    <KeyboardAvoidingView behavior={ Platform.OS === 'ios' ? 'padding' : 'height' }>
      <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()} style={{ flex: 1 }}>
      <View style={styles.container}>
          {state.error == true ? (
            <InfoModal
              text={`${state.errorMessage}`}
              isActive={itemModalOpen}
              handleClose={() => setItemModalOpen(false)}
            />
          ) : null}

          <Image
            style={styles.logo}
            source={require('../../assets/Logo-Grande.png')}
          />
          <Text style={styles.textIniciar}>Iniciar sesión</Text>
          <Text style={styles.textIniciar} />
          <Formik
            enableReinitialize={true}
            initialValues={{ 
              fCellphone: '',
              fPassword: ''
            }}
            onSubmit={(values, actions) => {
            // alert(JSON.stringify(values))
              const uPassword = values.fPassword
              setvCellphone(values.fCellphone);
              setvPassword(values.fPassword);
            
            CheckTextInput()
            setTimeout(() => {
                actions.setSubmitting(false)
            }, 2000);
            }}
            validationSchema={validationSchema}
            >
            {({ handleChange, handleBlur, handleSubmit, values, isSubmitting, errors }) => (
            <>
              <View style={styles.inputContainer}>
                <TextInput
                  style={styles.inputs}
                  placeholder="Teléfono celular"
                  placeholderTextColor="white"
                  underlineColorAndroid="transparent"
                  // onChangeText={newvCellphone => setvCellphone(newvCellphone)}
                  // value={vCellphone}
                  keyboardType='phone-pad'
                  autoCorrect={false}
                  autoCompleteType="off"
                  maxLength={10}
                  
                  onChangeText={handleChange('fCellphone')}
                  onBlur={handleBlur('fCellphone')}
                  value={values.fCellphone}

                />
              </View>
              {
                errors.fCellphone
                ? <Text style={styles.ErrorM}>{errors.fCellphone}</Text>
                : null
              }
              {/* {cellError == true ? (
                <Text style={styles.ErrorM}>Este campo no puede estar vacío</Text>
              ) : null} */}
              <View style={styles.inputContainer}>
                <TextInput
                  style={styles.inputs}
                  placeholder="Contraseña"
                  placeholderTextColor="white"
                  secureTextEntry={secureTextEntry}
                  underlineColorAndroid="transparent"
                  // onChangeText={newvPassword => setvPassword(newvPassword)}
                  // value={vPassword}
                  autoCorrect={false}
                  autoCompleteType="off"
                  
                  onChangeText={handleChange('fPassword')}
                  onBlur={handleBlur('fPassword')}
                  value={values.fPassword}
                />

                <TouchableOpacity onPress={() => onPassPress()}>
                  <Entypo style={{ color: '#cccccc', marginRight: 18 }} name={ toogleEye ? 'eye' : 'eye-with-line'} size={20} />
                  {/* <Image
                    style={styles.inputIcon}
                    source={require('../../assets/Visualización.png')}
                  /> */}
                </TouchableOpacity>
              </View>
              {
                errors.fPassword
                ? <Text style={styles.ErrorM}>{errors.fPassword}</Text>
                : null
              }  
              <View style={styles.buttonContainer2}>
                <TouchableOpacity style={[styles.buttonContainer, styles.loginButton]} onPress={handleSubmit} title="Submit" disabled={isSubmitting ? true: false}>          
                {
                isSubmitting 
                    ? <ActivityIndicator color='#fff' />
                    : <Text style={styles.loginText}>INGRESAR</Text>
                }
                </TouchableOpacity>
              </View>
          </>
          )}
        </Formik>
          {/* {passError == true ? (
            <Text style={styles.ErrorM}>Este campo no puede estar vacío</Text>
          ) : null} */}
    {/*     
          <TouchableOpacity
            style={[styles.buttonContainer, styles.loginButton]}
            disabled={Disable}
            // onPress={() => check()}
            // onPress={handleSubmit(onSubmitHook)}
            >
            <Text style={styles.loginText}>INGRESAR</Text>
          </TouchableOpacity>
        </View> */}
      </View> 
      </TouchableWithoutFeedback>
    </KeyboardAvoidingView>
  );
}

const styles = StyleSheet.create({
  buttonContainer2: {
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  spinnerTextStyle: {
    color: '#D32345',
  },
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    // backgroundColor: '#333333',
  },
  logo: {
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 60,
  },
  textIniciar: {
    color: 'white',
    width: 300,
    height: 45,
    marginBottom: 20,
    fontSize: 20,
    fontWeight: 'bold',
    textAlign: 'center',
  },
  inputContainer: {
    backgroundColor: '#5e5e5e',
    borderRadius: 30,
    width: 300,
    height: 45,
    marginBottom: 20,
    flexDirection: 'row',
    alignItems: 'center',
  },
  inputs: {
    height: 45,
    marginLeft: 16,
    color: 'white',
    flex: 1,
  },
  inputIcon: {
    marginRight: 15,
    justifyContent: 'center',
  },
  buttonContainer: {
    height: 45,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 20,
    width: 300,
    borderRadius: 30,
    backgroundColor: 'transparent',
  },
  frgt: {
    height: 15,
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
    marginVertical: 20,
    width: 300,
    backgroundColor: 'transparent',
  },
  loginButton: {
    marginTop: 10,
    backgroundColor: '#D32345',
  },
  loginText: {
    color: 'white',
    fontWeight: 'bold',
  },
  textByRegister: {
    color: '#808080',
    fontWeight: 'bold',
    textAlign: 'center',
  },
  errorMessage: {
    color: 'white',
    fontWeight: 'bold',
    textAlign: 'center',
  },
  ErrorM: {
    color: '#D32345',
    fontWeight: 'bold',
    marginBottom: 15,
  },
  popup: {
    backgroundColor: '#3d3d3d',
    marginTop: 150,
    marginHorizontal: 20,
    borderRadius: 7,
  },
  popupOverlay: {
    backgroundColor: '#00000057',
    flex: 1,
    marginTop: 30,
  },
  popupContent: {
    justifyContent: 'center',

    margin: 5,
    height: 180,
    alignItems: 'center',
  },
  mod: {
    fontSize: 20,
    fontWeight: 'bold',
    textAlign: 'left',
    color: 'white',
    marginTop: 3,
    marginBottom: 0,
    marginLeft: 10,
  },
  popupHeader: {
    marginBottom: 45,
  },
  popupButtons: {
    marginTop: 15,
    flexDirection: 'row',
    borderTopWidth: 1,
    borderColor: '#eee',
    justifyContent: 'center',
  },
  popupButton: {
    flex: 1,
    marginVertical: 16,
  },
  btnClose: {
    height: 20,
    backgroundColor: '#D32345',
    padding: 20,
  },
  modalInfo: {
    textAlign: 'left',
  },
  txtClose: {
    justifyContent: 'center',
    color: 'white',
    textAlign: 'center',
  },
  validationText: {
    color: '#D32345',
    fontWeight: 'bold',
    marginBottom: 15,
    fontSize: 13,
  }
});
