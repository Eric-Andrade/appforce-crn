import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Image,
  FlatList,
  Modal,
  Picker,
  ScrollView,
} from 'react-native';
import SegmentedControlTab from 'react-native-segmented-control-tab';
import Header from './Header'

export default class Solicitudes extends Component {

  constructor() {
    super();
    this.state = {
      checked: false,
      choosenLabel: '', 
      choosenindex: '',
      modalVisible:false,
      userSelected:[],
      selectedIndex: 0,
      //Default selected Tab Index for single select SegmentedControlTab
      selectedIndices: [0],
      //Default selected Tab Indexes for multi select SegmentedControlTab
      customStyleIndex: 0,
      //Default selected Tab Indexes for cusatom SegmentedControlTab
      data: [
        {id:'1',  solicitud:"EXI-508890", clave:"CEO", cliente:"Estrada y Sanchez Elvia Isabel", motivo:"07FE General Fe", estado:"Falta de información" },
        {id:'2',  solicitud:"EXI-508890", clave:"CEO", cliente:"Estrada y Sanchez Elvia Isabel", motivo:"07FE General Fe", estado:"Falta de información" },
        {id:'3',  solicitud:"EXI-508890", clave:"CEO", cliente:"Estrada y Sanchez Elvia Isabel", motivo:"07FE General Fe", estado:"Falta de información" },
        {id:'4',  solicitud:"EXI-508890", clave:"CEO", cliente:"Estrada y Sanchez Elvia Isabel", motivo:"07FE General Fe", estado:"Falta de información" },
        {id:'5',  solicitud:"EXI-508890", clave:"CEO", cliente:"Estrada y Sanchez Elvia Isabel", motivo:"07FE General Fe", estado:"Falta de información" },
        {id:'6',  solicitud:"EXI-508890", clave:"CEO", cliente:"Estrada y Sanchez Elvia Isabel", motivo:"07FE General Fe", estado:"Falta de información" },
        {id:'7',  solicitud:"EXI-508890", clave:"CEO", cliente:"Estrada y Sanchez Elvia Isabel", motivo:"07FE General Fe", estado:"Falta de información" },
        {id:'8',  solicitud:"EXI-508890", clave:"CEO", cliente:"Estrada y Sanchez Elvia Isabel", motivo:"07FE General Fe", estado:"Falta de información" },
        {id:'9',  solicitud:"EXI-508890", clave:"CEO", cliente:"Estrada y Sanchez Elvia Isabel", motivo:"07FE General Fe", estado:"Falta de información" },
        {id:'10', solicitud:"EXI-508890", clave:"CEO", cliente:"Estrada y Sanchez Elvia Isabel", motivo:"07FE General Fe", estado:"Falta de información" },
        {id:'11', solicitud:"EXI-508890", clave:"CEO", cliente:"Estrada y Sanchez Elvia Isabel", motivo:"07FE General Fe", estado:"Falta de información" },
        {id:'12', solicitud:"EXI-508890", clave:"CEO", cliente:"Estrada y Sanchez Elvia Isabel", motivo:"07FE General Fe", estado:"Falta de información" },
        {id:'13', solicitud:"EXI-508890", clave:"CEO", cliente:"Estrada y Sanchez Elvia Isabel", motivo:"07FE General Fe", estado:"Falta de información" },
        {id:'14', solicitud:"EXI-508890", clave:"CEO", cliente:"Estrada y Sanchez Elvia Isabel", motivo:"07FE General Fe", estado:"Falta de información" },
      ]
    };
  }

  clickEventListener = (item) => {
    this.setState({userSelected: item}, () =>{
      this.setModalVisible(true);
    });
  }

  setModalVisible(visible) {
    this.setState({modalVisible: visible});
  }

  handleSingleIndexSelect = (index: number) => {
    //handle tab selection for single Tab Selection SegmentedControlTab
    this.setState(prevState => ({ ...prevState, selectedIndex: index }));
  };

  handleMultipleIndexSelect = (index: number) => {
    //handle tab selection for multi Tab Selection SegmentedControlTab
    const { selectedIndices } = this.state;
    if (selectedIndices.includes(index)) {
      //if included in the selected array then remove
      this.setState(prevState => ({
        ...prevState,
        selectedIndices: selectedIndices.filter(i => i !== index),
      }));
    } else {
      //if not included in the selected array then add
      this.setState(prevState => ({
        ...prevState,
        selectedIndices: [...selectedIndices, index],
      }));
    }
  };
 
  handleCustomIndexSelect = (index: number) => {
    //handle tab selection for custom Tab Selection SegmentedControlTab
    this.setState(prevState => ({ ...prevState, customStyleIndex: index }));
  };

  render() {
    const { selectedIndex, selectedIndices, customStyleIndex } = this.state;

    return (
      <View style={styles.container}>

        <Header />
          <Text style={styles.textIniciar}>Solicitudes</Text>
        </View>
        <View>
        <SegmentedControlTab
          values={['Incompletas', 'Aprobadas','Rechazadas','Enviadas']}
          selectedIndex={customStyleIndex}
          onTabPress={this.handleCustomIndexSelect}
          borderRadius={0}
          tabsContainerStyle={{ height: 50, backgroundColor: '#333333' }}
          tabStyle={{
            backgroundColor: '#333333',
            borderWidth: 0,
            borderColor: 'transparent',
            decoration: 'underline',
            
          }}
          activeTabStyle={{ backgroundColor: '#333333', marginTop: 2 }}
          tabTextStyle={{ color: 'white', fontWeight: 'bold' }}
          activeTabTextStyle={{ borderColor: '#D32345',  borderBottomWidth: 3}}
        /> 
       
        {customStyleIndex === 0 && (
          <View>
          <View style={styles.ocointainer}>

<View style={styles.pickcontainer}>
  <Picker style={styles.pick} selectedValue={this.state.choosenLabel}
    onValueChange={
    (itemValue, itemIndex) => this.setState({
        choosenLabel: itemValue, 
        choosenindex:itemIndex})
    }>          
      <Picker.Item label = "Seleccione la opción" value = "word1" />
      <Picker.Item label = "Falta de información" value = "word2" />
      <Picker.Item label = "Mal redactada" value = "word3" />
      <Picker.Item label = "Lorem impsum" value = "word4" />
      <Picker.Item label = "Dolor sit amet " value = "word5" />
      <Picker.Item label = "Sed ut perspiciatis " value = "word6" />
  </Picker>

  <TouchableOpacity style={styles.inputIcon}  >
    <Image  source={require('../../assets/Flecha.png')}/>
  </TouchableOpacity>
  
</View>
</View>

          <View style={styles.Lcontainer}>
            <FlatList 
            columnWrapperStyle={styles.listContainer}
            data={this.state.data}
            keyExtractor= {(item) => {
              return item.id;
            }}
          renderItem={({item}) => {
          return (
            <TouchableOpacity onPress={() => {this.clickEventListener(item)}}>
            
              <View style={styles.tarjeta }>
                <View style={styles.solicitudview }>
                  <Text style={styles.solicitud }>Solicitud:{item.solicitud}</Text></View>
                  <View style={styles.card }>
                  <Text style={styles.clave}>Clave Zell:{item.clave}</Text>
                  <Text style={styles.clave}>Cliente:{item.cliente}</Text>
                  <Text style={styles.clave}>Motivo:{item.motivo}</Text>
                  <Text style={styles.clave}>Estado:{item.estado}</Text>
                </View>

                <View >
                <TouchableOpacity  style={[styles.buttonContainer, styles.loginButton]} onPress={() => this.onClickListener('login')}>
                  <Text style={styles.loginText}>COMPLETAR SOLICITUD</Text>
                </TouchableOpacity></View>
              </View>
                
            </TouchableOpacity>
          )}}/>

      </View>
      </View>
        )}
        {customStyleIndex === 1 && (
          <View style={styles.Lcontainer}>
            <FlatList 
            columnWrapperStyle={styles.listContainer}
            data={this.state.data}
            keyExtractor= {(item) => {
              return item.id;
            }}
          renderItem={({item}) => {
          return (
          
              <View style={styles.tarjeta }>
                <View style={styles.solicitudview }>
                  <Text style={styles.solicitud }>Solicitud:{item.solicitud}</Text></View>
                  <View style={styles.card }>
                  <Text style={styles.clave}>Clave Zell:{item.clave}</Text>
                  <Text style={styles.clave}>Cliente:{item.cliente}</Text>
                  <Text style={styles.clave}>Motivo:{item.motivo}</Text>
                  <Text style={styles.clave}>Estado:{item.estado}</Text>
                </View>

               
              </View>
          )}}/>

        <Modal
          animationType={'fade'}
          transparent={true}
          onRequestClose={() => this.setModalVisible(false)}
          visible={this.state.modalVisible}>

          <View style={styles.popupOverlay}>
            <View style={styles.popup}>
              <View style={styles.popupContent}>
                <ScrollView contentContainerStyle={styles.modalInfo}>
                  <Text style={styles.solicitud}>Solicitud:{this.state.userSelected.solicitud}</Text>
                  <Text style={styles.mod}>Clave Zell:{this.state.userSelected.clave}</Text>
                  <Text style={styles.mod}>Cliente:{this.state.userSelected.cliente}</Text>
                  <Text style={styles.mod}>Motivo:{this.state.userSelected.motivo}</Text>
                  <Text style={styles.mod}>Estado{this.state.userSelected.estado}</Text>
                  <Text style={styles.about}>{this.state.userSelected.about}</Text>
                </ScrollView>
              </View>
            </View>
          </View>
        </Modal>
      </View>
    
        )}
        {customStyleIndex === 2 && (
          <View style={styles.Lcontainer}>
            <FlatList 
            columnWrapperStyle={styles.listContainer}
            data={this.state.data}
            keyExtractor= {(item) => {
              return item.id;
            }}
          renderItem={({item}) => {
          return (
            <TouchableOpacity onPress={() => {this.clickEventListener(item)}}>
            
              <View style={styles.tarjeta }>
                <View style={styles.solicitudview }>
                  <Text style={styles.solicitud }>Solicitud:{item.solicitud}</Text></View>
                  <View style={styles.card }>
                  <Text style={styles.clave}>Clave Zell:{item.clave}</Text>
                  <Text style={styles.clave}>Cliente:{item.cliente}</Text>
                  <Text style={styles.clave}>Motivo:{item.motivo}</Text>
                  <Text style={styles.clave}>Estado:{item.estado}</Text>
                </View>

                <View >
                <TouchableOpacity  style={[styles.buttonContainer, styles.loginButton]} onPress={() => this.onClickListener('login')}>
                  <Text style={styles.loginText}>COMPLETAR SOLICITUD</Text>
                </TouchableOpacity></View>
              </View>
                
            </TouchableOpacity>
          )}}/>

        <Modal
          animationType={'fade'}
          transparent={true}
          onRequestClose={() => this.setModalVisible(false)}
          visible={this.state.modalVisible}>

          <View style={styles.popupOverlay}>
            <View style={styles.popup}>
              <View style={styles.popupContent}>
                <ScrollView contentContainerStyle={styles.modalInfo}>
                  <Text style={styles.solicitud}>Solicitud:{this.state.userSelected.solicitud}</Text>
                  <Text style={styles.mod}>Clave Zell:{this.state.userSelected.clave}</Text>
                  <Text style={styles.mod}>Cliente:{this.state.userSelected.cliente}</Text>
                  <Text style={styles.mod}>Motivo:{this.state.userSelected.motivo}</Text>
                  <Text style={styles.mod}>Estado{this.state.userSelected.estado}</Text>
                  <Text style={styles.about}>{this.state.userSelected.about}</Text>
                </ScrollView>
              </View>
              <View style={styles.popupButtons}>
                <TouchableOpacity onPress={() => {this.setModalVisible(false) }} style={styles.btnClose}>
                  <Text style={styles.txtClose}>CERRAR</Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </Modal>
      </View>
        )}
        {customStyleIndex === 3 && (
          <View style={styles.Lcontainer}>
            <FlatList 
            columnWrapperStyle={styles.listContainer}
            data={this.state.data}
            keyExtractor= {(item) => {
              return item.id;
            }}
          renderItem={({item}) => {
          return (
            <TouchableOpacity onPress={() => {this.clickEventListener(item)}}>
            
              <View style={styles.tarjeta }>
                <View style={styles.solicitudview }>
                  <Text style={styles.solicitud }>Solicitud:{item.solicitud}</Text></View>
                  <View style={styles.card }>
                  <Text style={styles.clave}>Clave Zell:{item.clave}</Text>
                  <Text style={styles.clave}>Cliente:{item.cliente}</Text>
                  <Text style={styles.clave}>Motivo:{item.motivo}</Text>
                  <Text style={styles.clave}>Estado:{item.estado}</Text>
                </View>

                <View >
                <TouchableOpacity  style={[styles.buttonContainer, styles.loginButton]} onPress={() => this.onClickListener('login')}>
                  <Text style={styles.loginText}>CARGAR FIRMA AUTÓGRAFA</Text>
                </TouchableOpacity></View>
              </View>
                
            </TouchableOpacity>
          )}}/>

        <Modal
          animationType={'fade'}
          transparent={true}
          onRequestClose={() => this.setModalVisible(false)}
          visible={this.state.modalVisible}>

          <View style={styles.popupOverlay}>
            <View style={styles.popup}>
              <View style={styles.popupContent}>
                <ScrollView contentContainerStyle={styles.modalInfo}>
                  <Text style={styles.solicitud}>Solicitud:{this.state.userSelected.solicitud}</Text>
                  <Text style={styles.mod}>Clave Zell:{this.state.userSelected.clave}</Text>
                  <Text style={styles.mod}>Cliente:{this.state.userSelected.cliente}</Text>
                  <Text style={styles.mod}>Motivo:{this.state.userSelected.motivo}</Text>
                  <Text style={styles.mod}>Estado{this.state.userSelected.estado}</Text>
                  <Text style={styles.about}>{this.state.userSelected.about}</Text>
                </ScrollView>
              </View>
              <View style={styles.popupButtons}>
                <TouchableOpacity onPress={() => {this.setModalVisible(false) }} style={styles.btnClose}>
                  <Text style={styles.txtClose}>CERRAR</Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </Modal>
      </View>
        )}
        </View>
      </View>
    );
  }
}



Solicitudes.navigationOptions = {
  headerShown: false,
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: '#333333',
  },
  logo:{
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    marginTop:40,
    marginBottom:20,
    marginLeft: 10
  },
  head:{
    backgroundColor: '#000',
    // borderRadius:20
    borderTopLeftRadius: 0,
    borderTopRightRadius: 0,
    borderBottomLeftRadius: 20,
    borderBottomRightRadius: 20,
  },
  textIniciar:{
    marginTop:30,
    color: 'white',
    width:300,
    height:45,
    marginBottom:5,
    fontSize: 20,
    fontWeight: 'bold',
    textAlign: 'left',
    marginLeft: 20
  },
  buttonContainer: {
    flex:1,
    height:35,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom:20,
    borderRadius:30,
    backgroundColor:'transparent',
    marginVertical: 10,
    marginHorizontal:20,
    flexBasis: '46%',
  },
  inputIcon:{
    marginRight:20,
    justifyContent: 'center',
  },
  loginButton: {
    backgroundColor: "#D32345",
  },
  loginText: {
    color: 'white',
    fontWeight: 'bold',
  },
  Lcontainer:{
    backgroundColor:"#333333"
  },
  card:{
    backgroundColor:"#3d3d3d",
    flexBasis: '46%',
    flexDirection:'column',
    height: 120,
    borderRadius:8
  },
  solicitud:{
    fontSize:18,
    textAlign: 'left',
    color:"white",
    fontWeight:'bold',
    marginLeft: 10,
    marginTop:5,
  },
  solicitudview:{
    backgroundColor:"#696969",
    color:'white',
    height: 40,
    fontSize: 20,
    borderRadius:10
  },
  ocointainer:{
    marginTop: 30,
    marginBottom:20,
    flexDirection:'row',
    justifyContent:'center',
    alignItems:'center',
  },
  pickcontainer:{
    flexDirection:'row',
    borderRadius: 10, 
    borderWidth: 1, 
    borderColor: '#696969',  
    backgroundColor:"#696969",
    flexBasis: '90%',
  },
  pick:{
    backgroundColor:"#696969",
    color:'white',
    height: 40,
    fontSize: 20,
    borderRadius:50,
    flexBasis: '90%',
    marginLeft:15,
  },
  tarjeta:{
    shadowOpacity: 0.37,
    shadowRadius: 7.49,
    elevation: 12,
    marginVertical: 10,
    marginHorizontal:20,
    flexBasis: '46%',
    flexDirection:'column',
    backgroundColor:"#3d3d3d",
    borderRadius:10
  },
  clave:{
    fontSize:14,
    textAlign: 'left',
    color:"#9e9e9e",
    marginTop:3,
    marginBottom:0,
    marginLeft: 10
  },

 /************ modals ************/
  popup: {
    backgroundColor: '#3d3d3d',
    marginTop: 150,
    marginHorizontal: 20,
    borderRadius: 7,
  },
  popupOverlay: {
    backgroundColor: "#00000057",
    flex: 1,
    marginTop: 30
  },
  popupContent: {
    textAlign: 'left',
    margin: 5,
    height:180,
    alignItems: 'center',
    
  },
  mod:{
    fontSize:20,
    fontWeight: 'bold',
    textAlign: 'left',
    color:"#9e9e9e",
    marginTop:3,
    marginBottom:0,
    marginLeft: 10
  },
  popupHeader: {
    marginBottom: 45
  },
  popupButtons: {
    marginTop: 15,
    flexDirection: 'row',
    borderTopWidth: 1,
    borderColor: "#eee",
    justifyContent:'center'
  },
  popupButton: {
    flex: 1,
    marginVertical: 16
  },
  btnClose:{
    height:20,
    backgroundColor:'#D32345',
    padding:20
  },
  modalInfo:{
    textAlign: 'left',
  },
  txtClose:{
    justifyContent:'center',
    color: 'white',
    textAlign: 'center',
  }
}); 
 