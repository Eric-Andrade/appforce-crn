import React, { useRef, useState, useEffect } from 'react'
import { View, StyleSheet, Text, TouchableOpacity, Image, Platform } from 'react-native'
// import Permissions from 'react-native-permissions';
import DocumentScanner from '@woonivers/react-native-document-scanner'

export default function Scanner() {
  const pdfScannerElement = useRef(null)
  const [data, setData] = useState({})
  const [scannerVisible, setscannerVisible] = useState(!true)

  function handleOnPressRetry() {
    setData({})
  }

  function handleOnPress() {
    pdfScannerElement.current.capture()
  }

  function handleOnPressVisible() {
    setscannerVisible(!scannerVisible)
  }
  
  if (data.croppedImage) {
    console.log('data', data)
    return (<View style={styles.permissions}>
            <Text>Image captured</Text>
          </View>)
  }
  return (
    scannerVisible === true
    ? <React.Fragment>
        <DocumentScanner
          ref={pdfScannerElement}
          style={styles.scanner}
          onPictureTaken={setData}
          overlayColor='rgba(211,35,69, 0.7)'
          enableTorch={false}
          quality={1.0}
          useBase64={true}
          detectionCountBeforeCapture={5}
          detectionRefreshRateInMS={50}
        />
          <TouchableOpacity onPress={handleOnPress} style={styles.multiImagePickerButton}>
            <Text style={styles.multiImagePickerButtonText}>TOMAR FOTO</Text>
          </TouchableOpacity>
      </React.Fragment>
    : <View style={styles.Buttonscontainer}>
        <TouchableOpacity onPress={handleOnPressVisible} style={styles.multiImagePickerButton}>
          <Text style={styles.multiImagePickerButtonText}>TOMAR FOTO</Text>
        </TouchableOpacity>
      </View>
  )
}

const styles = StyleSheet.create({
  scanner: {
    flex: 1,
    aspectRatio: undefined
  },
  Buttonscontainer: {
    flex: 1,
  },
  multiImagePickerButton: {
    alignSelf: "center",
    position: "absolute",
    bottom: 32,
    height: 45,
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 20,
    width: 142,
    marginHorizontal: 10,
    borderRadius: 30,
    backgroundColor: '#D32345',
  },
  multiImagePickerButtonText: {
    color: 'white',
    fontWeight: '700',
    fontSize: 14,
  }
})