import React, {useState, useEffect} from 'react';
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableOpacity,
  Image,
  ScrollView,
  Picker,
  FlatList,
  ActivityIndicator
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import {withNavigationFocus} from 'react-navigation';
import ForceApi from '../api/force';
import Spinner from 'react-native-loading-spinner-overlay';
const Sol4Form = ({isFocused, onSubmit}) => {
  const [request, setrequest] = useState(null);
  const [vResidenceTime, setvResidenceTime] = useState(0);
  const [vCivilStatus, setvCivilStatus] = useState(0);

  async function getList() {
    const vKindSearch = 4;

    const vCreationUser = await AsyncStorage.getItem('id');
    const {data} = await ForceApi.post(`/GetRequestController.php`, {
      vCreationUser,
      vKindSearch,
    });
    setrequest(data.request === undefined ? [] : data.request);
  }

  useEffect(() => {
    if (isFocused) {
      // console.warn('Estoy en Sol4Form');
      setrequest(null);
      getList();
    }
  }, [isFocused]);

  if (request === null) {
    return (
      <View style={styles.loadingView}>
        <ActivityIndicator size='small' color='#D32345' style={{marginVertical: 5}}/>
        <Text style={styles.loadingText}>Buscando solicitudes rechazadas...</Text>
      </View>
    );
    // return (
    //   <Spinner
    //     size="large"
    //     visible={true}
    //     color="#D32345"
    //     textContent={'Buscando solicitudes rechazadas...'}
    //     textStyle={styles.spinnerTextStyle}
    //   />
    // );
  }

  return (
    <View style={{ backgroundColor: '#333333', flex: 1 }}>
      {request.length >= 1 ? (
        <View style={styles.Lcontainer}>
          <FlatList
            columnWrapperStyle={styles.listContainer}
            data={request}
            keyExtractor={item => {
              return item.solicitude_id;
            }}
            renderItem={({item}) => {
              return (
                <View style={styles.tarjeta}>
                  <View style={styles.solicitudview}>
                    <Text style={styles.solicitud}>
                      Id de la Solicitud: {item.solicitude_id}
                    </Text>
                  </View>
                  <View style={styles.card}>
                    <Text style={styles.clave}>Nombre: {item.full_name}</Text>
                  </View>
                </View>
              );
            }}
          />
        </View>
      ) : (
        <View style={styles.emptyListView}>
          <Text style={styles.emptyListText}>
            Sin solicitudes rechazadas por el momento.
          </Text>
        </View>
      )}
    </View>
  );
};

Sol4Form.navigationOptions = {
  headerShown: false,
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: '#333333',
  },
  logo: {
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    marginTop: 40,
    marginBottom: 20,
    marginLeft: 10,
  },
  head: {
    backgroundColor: '#000',
    borderRadius: 20,
  },
  textIniciar: {
    marginTop: 30,
    color: 'white',
    width: 300,
    height: 45,
    marginBottom: 5,
    fontSize: 20,
    fontWeight: 'bold',
    textAlign: 'left',
    marginLeft: 20,
  },
  buttonContainer: {
    flex: 1,
    height: 35,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 20,
    borderRadius: 30,
    backgroundColor: 'transparent',
    marginVertical: 10,
    marginHorizontal: 30,
    flexBasis: '46%',
  },
  inputIcon: {
    marginRight: 20,
    justifyContent: 'center',
  },
  loginButton: {
    backgroundColor: '#D32345',
  },
  loginText: {
    color: 'white',
    fontWeight: 'bold',
  },
  Lcontainer: {
    backgroundColor: '#333333',
  },
  card: {
    backgroundColor: '#484848',
    flexBasis: '46%',
    flexDirection: 'column',
    minHeight: 100,
    height: '100%',
    borderRadius: 8,
  },
  solicitud: {
    fontSize: 17,
    textAlign: 'left',
    color: 'white',
    fontWeight: '500',
    marginHorizontal: 15,
    marginTop: 5,
  },
  solicitudview: {
    backgroundColor: '#6d6d6d',
    color: 'white',
    height: 40,
    fontSize: 20,
    // borderRadius:10,
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
    borderBottomLeftRadius: 0,
    borderBottomRightRadius: 0,
    flexDirection: 'column',
    justifyContent: 'center',
  },
  ocointainer: {
    marginTop: 30,
    marginBottom: 20,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  pickcontainer: {
    flexDirection: 'row',
    borderRadius: 10,
    borderWidth: 1,
    borderColor: '#696969',
    backgroundColor: '#696969',
    flexBasis: '90%',
  },
  pick: {
    backgroundColor: '#696969',
    color: 'white',
    height: 40,
    fontSize: 20,
    borderRadius: 50,
    flexBasis: '90%',
    marginLeft: 15,
  },
  tarjeta: {
    shadowOpacity: 0.37,
    shadowRadius: 7.49,
    elevation: 12,
    marginVertical: 10,
    marginHorizontal: 30,
    flexBasis: '46%',
    flexDirection: 'column',
    backgroundColor: '#484848',
    borderRadius: 10,
  },
  clave: {
    fontSize: 14,
    textAlign: 'left',
    color: '#9e9e9e',
    marginTop: 3,
    marginTop: 5,
    marginHorizontal: 15,
    fontWeight: '500',
  },
  emptyListView: {
    flex: 1,
    flexDirection: 'column',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
  emptyListText: {
    color: 'white',
    fontSize: 14,
    fontWeight: '600',
  },
  spinnerTextStyle: {
    color: '#D32345',
  },
  loadingView: {
    flex: 1,
    flexDirection: 'column',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#333'
  },
  loadingText: {
    color: 'white',
    fontSize: 14,
    fontWeight: '600',
  },
});

// export default Sol4Form;
export default withNavigationFocus(Sol4Form);
