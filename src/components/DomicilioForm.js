import React, {useState, useEffect} from 'react';
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableOpacity,
  Image,
  ScrollView,
  Picker,
  ActivityIndicator,
  Button
} from 'react-native';
import PickerModal from 'react-native-picker-modal-view';
import Ionicons from 'react-native-vector-icons/Ionicons';
import RNPickerSelect from 'react-native-picker-select';
import {useModal} from './useModal';
import InfoModal from './InfoModal';
import Spinner from 'react-native-loading-spinner-overlay';
import ForceApi from '../api/force';
import { Formik } from 'formik';
import * as yup from 'yup';

const validationSchema = yup.object().shape({
  fStreet: yup 
          .string()
          .matches(/^[A-Za-zÑñ0-9 ]*$/, 'No ingrese caracteres especiales ni con tildes')
          .required('Este campo no puede estar vacío')
          .uppercase(),
  fExteriorNumber: yup 
          .string()
          .matches(/^[A-Za-zÑñ0-9 ]*$/, 'No ingrese caracteres especiales ni con tildes')
          .required('Este campo no puede estar vacío')
          .uppercase(),
  fInteriorNumber: yup
          .string()
          .matches(/^[A-Za-zÑñ0-9 ]*$/, 'No ingrese caracteres especiales ni con tildes')
          // .required('Este campo no puede estar vacío')
          .uppercase(),
  fZipCode: yup
          .string()
          .matches(/^[0-9]*$/, 'Ingrese únicamente números')
          .min(5, 'Código postal debe tener 5 dígitos')
          .max(5, 'Código postal debe tener 5 dígitos')
          .typeError('Ingrese únicamente números')
          .required('Este campo no puede estar vacío'),
})

const DomicilioForm = ({state, errorMessage, onSubmit}) => {
  console.log('DomicilioForm state: ', state);
  const [itemModalOpen, setItemModalOpen, toggleModal] = useModal();
  const [vZipCode, setvZipCode] = useState('');
  const [vStreet, setvStreet] = useState('');
  const [vExteriorNumber, setvExteriorNumber] = useState('');
  const [vInteriorNumber, setvInteriorNumber] = useState('');
  const [vState, setvState] = useState('Estado');
  const [vCity, setvCity] = useState('Ciudad');
  const [pSuburb, setpSuburb] = useState([]);
  const [vSuburb, setvSuburb] = useState();

  const [CCalle, setCCalle] = useState(false);
  const [CNum, setCNum] = useState(false);
  const [CCode, setCCode] = useState(false);
  const [CCol, setCCol] = useState(false);
  const [CCiud, setCCiud] = useState(false);
  const [CEstado, setCCEstado] = useState(false);

  const [Visible, setVisible] = useState(false);
  const [isLoading, setisLoading] = useState(false);
  const [selectedItem, setselectedItem] = useState({});
  const [newSuburb, setnewSuburb] = useState([]);

  useEffect(() => {
    if (isLoading != false) {
      setInterval(() => {
        setisLoading(false);
      }, 3000);
    }
  }, [isLoading]);

  CheckTextInput = () => {
    if (
      vStreet != '' &&
      vExteriorNumber != '' &&
      vSuburb != '' &&
      vCity != '' &&
      vState != '' &&
      vZipCode != ''
    ) {
      setisLoading(false);
      setCCalle(false);
      setCNum(false);
      setCCol(false);
      setCCiud(false);
      setCCEstado(false);
      onSubmit({
        vZipCode,
        vStreet,
        vExteriorNumber,
        vInteriorNumber,
        vState,
        vCity,
        vSuburb,
      });
      setVisible(true);
      // setisLoading(false);
      // setVisible(state.error === false ? true : state.error);
      toggleModal(true);
    }

    if (vStreet != '') {
      setCCalle(false);
    } else {
      setCCalle(true);
    }

    if (vExteriorNumber != '') {
      setCNum(false);
    } else {
      setCNum(true);
    }

    if (vZipCode != '') {
      setCCode(false);
    } else {
      setCCode(true);
    }

    if (vSuburb != '') {
      setCCol(false);
    } else {
      setCCol(true);
    }

    if (vCity != '') {
      setCCiud(false);
    } else {
      setCCiud(true);
    }

    if (vState != '') {
      setCCEstado(false);
    } else {
      setCCEstado(true);
    }
  };

  const getAddres = ({vZipCode}) => {
    ForceApi.post(`/GetAddressController.php`, {vZipCode}).then(res => {
      setnewSuburb([])
      setvState(res.data.state);
      setvCity(res.data.city);
      setpSuburb(res.data.suburbs);
      
      var array = res.data.suburbs;
      var arr = [];
      array.forEach((element, index) => {
        for (let [key, value] of Object.entries(element)) {
          arr.push({
            label: value,
            value: value
          });
        }
      });
      
      setnewSuburb(arr)
      
    });
  };

  useEffect(() => {
    if (vZipCode) {
      getAddres({vZipCode});
    }
  }, [vZipCode]);

  useEffect(() => {
    let isCancelled = false;
    if (Visible && state.errorMessage != '') {
      setVisible(!Visible);
    } else {
      setVisible(false);
    }
    return () => {
      isCancelled = true;
    };
  }, [state]);

  if (Visible) {
    return (
      <Spinner
        size="large"
        visible={Visible}
        color="#D32345"
        textContent={'Un momento por favor'}
        textStyle={styles.spinnerTextStyle}
      />
    );
  }

  return (
    <ScrollView>
      {state.error == true ? (
        <InfoModal
          text={`${state.errorMessage}`}
          isActive={itemModalOpen}
          handleClose={() => setItemModalOpen(false)}
        />
      ) : null}
      <Formik
        enableReinitialize={true}
        initialValues={{ 
          fStreet: vStreet,
          fExteriorNumber: vExteriorNumber,
          fInteriorNumber: vInteriorNumber,
          fZipCode: vZipCode
          }}
        onSubmit={(values, actions) => {
        // alert(JSON.stringify(values))
        const _Street = values.fStreet;
        const _ExteriorNumber = values.fExteriorNumber;
        const _InteriorNumber = values.fInteriorNumber;

        setvStreet(_Street.toUpperCase())
        setvExteriorNumber(_ExteriorNumber.toUpperCase())
        setvInteriorNumber(_InteriorNumber.toUpperCase())
        setvZipCode(values.fZipCode)
        CheckTextInput()
        setTimeout(() => {
            actions.setSubmitting(false)
        }, 2000);
        }}
        validationSchema={validationSchema}
        >
        {({ handleChange, handleBlur, handleSubmit, values, isSubmitting, errors }) => (
          <>
            <View style={styles.buttonContainer}>
              <View style={styles.inputContainer}>
                <TextInput
                  style={styles.inputs}
                  placeholder="Código postal"
                  placeholderTextColor="white"
                  underlineColorAndroid="transparent"
                  // onChangeText={newvZipCode => setvZipCode(newvZipCode.toUpperCase())}
                  // value={vZipCode}
                  autoCorrect={false}
                  autoCapitalize="characters"
                  keyboardType={'numeric'}
                  type="text"
                  maxLength={5}
                  
                  onChangeText={handleChange('fZipCode')}
                  onBlur={() => setvZipCode(values.fZipCode)}
                  value={values.fZipCode}
                />
              </View>
              {
                errors.fZipCode
                ? <Text style={styles.ErrorM}>{errors.fZipCode}</Text>
                : null
              }
              {/* {CCode == true ? (
                <Text style={styles.ErrorM}>Este campo no puede estar vacío</Text>
              ) : null} */}
              <View style={styles.inputContainer}>
                <TextInput
                  style={styles.inputs}
                  placeholder="Ingrese la calle"
                  placeholderTextColor="white"
                  underlineColorAndroid="transparent"
                  // onChangeText={newvStreet => setvStreet(newvStreet.toUpperCase())}
                  // value={vStreet}
                  autoCorrect={false}
                  autoCapitalize="characters"
                  
                  onChangeText={handleChange('fStreet')}
                  onBlur={handleBlur('fStreet')}
                  value={values.fStreet}
                />
              </View>
              {
                errors.fStreet
                ? <Text style={styles.ErrorM}>{errors.fStreet}</Text>
                : null
              }
              {/* {CCalle == true ? (
                <Text style={styles.ErrorM}>Este campo no puede estar vacío</Text>
              ) : null} */}
              <View style={styles.buttonContainer2}>
                <View style={styles.SinputContainer}>
                  <TextInput
                    style={styles.inputs}
                    placeholder="Ingrese número exterior"
                    placeholderTextColor="white"
                    underlineColorAndroid="transparent"
                    // onChangeText={newvExteriorNumber =>
                    //   setvExteriorNumber(newvExteriorNumber.toUpperCase())
                    // }
                    // value={vExteriorNumber}
                    autoCorrect={false}
                    autoCapitalize="characters"
                    
                    onChangeText={handleChange('fExteriorNumber')}
                    onBlur={handleBlur('fExteriorNumber')}
                    value={values.fExteriorNumber}
                    />
                </View>
                {
                  errors.fExteriorNumber
                  ? <Text style={styles.ErrorM}>{errors.fExteriorNumber}</Text>
                  : null
                }
                <View style={styles.SinputContainer2}>
                  <TextInput
                    style={styles.inputs}
                    placeholder="Número interior"
                    placeholderTextColor="white"
                    underlineColorAndroid="transparent"
                    // onChangeText={newvInteriorNumber =>
                    //   setvInteriorNumber(newvInteriorNumber.toUpperCase())
                    // }
                    // value={vInteriorNumber}
                    autoCorrect={false}
                    autoCapitalize="characters"
                    onChangeText={handleChange('fInteriorNumber')}
                    onBlur={handleBlur('fInteriorNumber')}
                    value={values.fInteriorNumber}
                  />
                </View>
              </View>
              {
                errors.fInteriorNumber
                ? <Text style={styles.ErrorM}>{errors.fInteriorNumber}</Text>
                : null
              }
              {/* {CNum == true ? (
                <Text style={styles.ErrorM}>
                  El campo de número exterior no puede estar vacío
                </Text>
              ) : null} */}

              <View style={styles.ocointainer}>
                <View style={styles.inputContainer}>
                  <RNPickerSelect
                    placeholder={{
                      label: 'Seleccione una colonia',
                      value: null,
                      color: '#9EA0A4'
                    }}
                    style={{
                      placeholder: {
                        flexDirection: 'column',
                        justifyContent: 'center',
                        alignItems: 'center',
                        color: 'white',
                        marginLeft: 15
                      },
                      inputAndroid: {
                        color: 'white',
                        width: 270,
                        height: 40,
                        flexDirection: 'column',
                        justifyContent: 'center',
                        alignItems: 'center',
                        color: 'white',
                        marginLeft: 15
                      },
                      inputIOS: {
                        color: 'white',
                        width: 270,
                        height: 40,
                        flexDirection: 'column',
                        justifyContent: 'center',
                        alignItems: 'center',
                        color: 'white',
                        marginLeft: 15,
                        marginTop: 15
                      },
                      iconContainer: {
                        top: 10,
                        right: 15,
                      }
                    }}
                    useNativeAndroidPickerStyle={false}
                    Icon={() => {
                      return <Ionicons name='md-arrow-dropdown' size={20} color='#FFF' />
                    }}
                    onValueChange={newpSuburb => setvSuburb(newpSuburb)}
                    items={newSuburb}
                    // items={pSuburb}
                  />

                  {/* <PickerModal
                    renderSelectView={(disabled, selected, showModal) =>
                      <TouchableOpacity disabled={disabled} onPress={showModal} style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', marginHorizontal: 15}}>
                        <Text style={{ color: 'white' }}>{ vSuburb ? vSuburb : 'Seleccione una colonia' }</Text>
                        <Image source={require('../../assets/Flecha.png')} />
                      </TouchableOpacity>
                    }
                    onSelected={newpSuburb => setvSuburb(newpSuburb.Name)}
                    items={newSuburb}
                    sortingLanguage={'tr'}
                    showToTopButton={true}
                    selected={vSuburb}
                    autoGenerateAlphabeticalIndex={true}
                    selectPlaceholderText={'Seleccione una colonia'}
                    onEndReached={() => console.log('list ended...')}
                    searchPlaceholderText={'Search...'}
                    requireSelection={false}
                    autoSort={false}
                  /> */}

                  {/* <Picker
                    style={styles.pick}
                    // mode='dropdown'
                    itemStyle={styles.onePickerItem}
                    selectedValue={vSuburb}
                    placeholderTextColor="white"
                    onValueChange={  => setvSuburb(newpSuburb)}>
                    <Picker.Item label="Seleccione la colonia" value="word1" />
                    {(pSuburb || []).map((item, index) => {
                      return (
                        <Picker.Item
                          key={index}
                          label={`${item[index]}`}
                          value={`${item[index]}`}
                        />
                      );
                    })}
                  </Picker> */}
                  {/* <TouchableOpacity style={styles.inputIcon}>
                    <Image source={require('../../assets/Flecha.png')} />
                  </TouchableOpacity> */}
                </View>
              </View>
              {CCol == true ? (
                <Text style={styles.ErrorM}>Este campo no puede estar vacío</Text>
              ) : null}

              <View style={styles.inputContainer}>
                <TextInput
                  style={styles.inputs}
                  placeholder={vCity}
                  placeholderTextColor="white"
                  underlineColorAndroid="transparent"
                  editable={false}
                  value={vCity}
                  selectTextOnFocus={false}
                  autoCorrect={false}
                  autoCapitalize="characters"
                />
              </View>
              {CCiud == true ? (
                <Text style={styles.ErrorM}>Este campo no puede estar vacío</Text>
              ) : null}
              <View style={styles.inputContainer}>
                <TextInput
                  style={styles.inputs}
                  placeholder={vState}
                  placeholderTextColor="white"
                  underlineColorAndroid="transparent"
                  value={vState}
                  editable={false}
                  selectTextOnFocus={false}
                  autoCorrect={false}
                  autoCapitalize="characters"
                />
              </View>
              {CEstado == true ? (
                <Text style={styles.ErrorM}>Este campo no puede estar vacío</Text>
              ) : null}
            </View>
            <View style={styles.buttonContainer2}>
              <TouchableOpacity style={styles.lightOn} onPress={handleSubmit} title="Submit" disabled={isSubmitting ? true: false}>          
                {
                isSubmitting 
                  ? <ActivityIndicator color='#fff' />
                  : <Text style={styles.loginText}>CONTINUAR</Text>
                }
              </TouchableOpacity>
          </View>
          </>
        )}
      </Formik>
      {/* <View style={styles.buttonContainer2}>
        <TouchableOpacity
          style={styles.lightOn}
          onPress={() => CheckTextInput()}>
          <Text style={styles.loginText}>CONTINUAR</Text>
        </TouchableOpacity>
      </View> */}
    </ScrollView>
  );
};

DomicilioForm.navigationOptions = {
  headerShown: false,
};

const styles = StyleSheet.create({
  spinnerTextStyle: {
    color: '#D32345',
  },
  container: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: '#333333',
  },
  logo: {
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    marginTop: 40,
    marginBottom: 20,
    marginLeft: 10,
  },
  backContainer: {
    flexDirection: 'column',
    marginBottom: 20,
  },
  head: {
    backgroundColor: '#000',
    borderRadius: 20,
  },
  back: {
    marginTop: 2,
    flexDirection: 'column',
    color: 'white',
  },
  textContainer: {
    flexDirection: 'row',
    marginTop: 30,
    marginBottom: 10,
    marginLeft: 10,
  },
  textIniciar: {
    color: 'white',
    width: 300,
    fontSize: 20,
    fontWeight: 'bold',
    textAlign: 'left',
    marginLeft: 25,
  },
  textInit: {
    color: '#b3b4b5',
    textAlign: 'left',
    marginLeft: 25,
  },
  textInit: {
    color: '#b3b4b5',
    textAlign: 'left',
    marginLeft: 25,
  },
  buttonContainer2: {
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  logout: {
    marginTop: 20,
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 20,
    width: 142,
    borderRadius: 30,
    height: 45,
    backgroundColor: '#911830',
  },
  buttonContainer: {
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  lightOn: {
    marginTop: 20,
    marginLeft: 20,
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 20,
    width: 142,
    borderRadius: 30,
    height: 45,
    backgroundColor: '#D32345',
  },
  OUB: {
    marginLeft: 20,
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 20,
    width: 142,
    borderRadius: 30,
    height: 45,
    backgroundColor: '#D32345',
  },
  loginText: {
    color: 'white',
    fontWeight: 'bold',
  },
  inputContainer: {
    backgroundColor: '#5e5e5e',
    borderRadius: 30,
    width: 300,
    height: 45,
    marginBottom: 20,
    flexDirection: 'row',
    alignItems: 'center',
  },

  SinputContainer: {
    backgroundColor: '#5e5e5e',
    borderRadius: 30,
    width: 300,
    height: 45,
    marginBottom: 20,
    flexDirection: 'row',
    alignItems: 'center',
  },

  SinputContainer2: {
    backgroundColor: '#5e5e5e',
    borderRadius: 30,
    width: 300,
    height: 45,
    marginBottom: 20,
    flexDirection: 'row',
    alignItems: 'center',
    marginLeft: 10,
  },
  inputs: {
    height: 45,
    marginLeft: 16,
    color: 'white',
    flex: 1,
  },
  ocointainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  pick: {
    backgroundColor: '#696969',
    color: 'white',
    height: 40,
    fontSize: 20,
    borderRadius: 50,
    flexBasis: '88%',
    marginLeft: 15,
  },
  inputIcon: {
    marginRight: 15,
    justifyContent: 'center',
    height: 10,
    width: 10,
  },
  ErrorM: {
    color: '#D32345',
    fontWeight: 'bold',
    marginBottom: 15,
  },
  onePickerItem: {
    height: 44,
    color: 'red',
  },
});

export default DomicilioForm;

/* <TouchableOpacity style={ styles.logout}  onPress={() => this.props.navigation.navigate('Logout'), console.log(vZipCode, vStreet, vExteriorNumber,
                 vInteriorNumber, vState, vCity, vSuburb)}>
              <Text style={styles.loginText}>GUARDAR</Text>
            </TouchableOpacity>*/
