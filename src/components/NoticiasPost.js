import React, { useState, useRef } from 'react';
import { StyleSheet, Text, View, Image, Dimensions, TouchableOpacity, TouchableWithoutFeedback, Linking } from 'react-native';
import HTML from 'react-native-render-html';
import { IGNORED_TAGS } from 'react-native-render-html/src/HTMLUtils';
import Video from 'react-native-video';
import ProgressBar from 'react-native-progress/Bar';
import Ionicons from 'react-native-vector-icons/Ionicons';

function secondsToTime(time) {
    return ~~(time / 60) + ':' + (time % 60 < 10 ? '0' : '') + time % 60;
}

const NoticiasPost = ({ item }) => {
    const [ pausedstate, setpausedstate ] = useState(true);
    const [ progress, setprogress ] = useState(0);
    const [ duration, setduration ] = useState(0);
    let player = useRef(null);
    
    const openLink = url => {
        Linking.openURL(url).catch(err => console.error('An error occurred', err));
    };
    const handleMainButtonTouch = () => {
        if (progress >= 1) {
            player.seek(0);
        }
        setpausedstate(!pausedstate);
        player.seek(progress);
    } 

    const handleProgressPress = e => {
        console.warn('handleProgressPress pressed');
        
        const position = e.nativeEvent.locationX;
        const progressvar = (position / 250) * duration;
        const isPlaying = !pausedstate;
        
        player.seek(progressvar);
    };

    const handleProgress = progressparam => {
        setprogress(progressparam.currentTime / duration);
    };

    const handleEnd = () => {
        setpausedstate(true);
        setprogress(0);
        // setduration(0);
        player.seek(0)
    };

    const handleLoad = meta => {
        setduration(meta.duration);
    };

    const SwitchComponent = () => {
        switch (item.kind_publication) {
            case '1':
                return <Image style={{ width: '100%', height: 200 }} source={{uri: `data:image/png;base64,${item.pulication}`}} />
            break;
            case '2':
                // return <HTML html={`<iframe width="${Dimensions.get('window').width}" height="300" src="https://www.youtube.com/embed/tgbNymZ7vqY?autoplay=1"</iframe>`} imagesMaxWidth={Dimensions.get('window').width}/>
                return  <TouchableOpacity style={styles.LinkButton} onPress={() => openLink(item.pulication)}><Text style={{ color: '#fff'}}>Visitar link</Text></TouchableOpacity>
            break;
            case '3':
                
                return (
                    <>
                        <View style={{minHeight: 200, width: '100%', backgroundColor: '#333'}}>
                            <TouchableWithoutFeedback
                                // style={styles.fullScreen}
                                onPress={() => setpausedstate(!pausedstate)}
                                >
                                    <View 
                                        style={styles.fullScreen}
                                    >
                                        <Video
                                            ref={ref => { player = ref }}
                                            controls={!true}
                                            paused={pausedstate}
                                            disableFocus={!true}
                                            resizeMode='contain'
                                            fullscreen
                                            fullscreenAutorotate
                                            fullscreenOrientation='landscape'
                                            pictureInPicture
                                            poster='https://pluspng.com/img-png/play-button-png-play-button-png-transparent-660.png'
                                            source={{ uri: `https://www.exituswebapps.com/ForceArchivesTest/tutorial/1_Video_de_Producto_Exitus.mp4` }}
                                            // source={{ uri: `${item.pulication}`}}
                                            onLoad={handleLoad}
                                            onProgress={handleProgress}
                                            onEnd={handleEnd}
                                            style={styles.fullScreen}
                                        />
                                    </View>
                            </TouchableWithoutFeedback>
                            <View>
                                
                                <TouchableWithoutFeedback
                                    // onPress={handleProgressPress}
                                >
                                    <View>
                                        <ProgressBar
                                            progress={progress}
                                            color='#D32345'
                                            unfilledColor='rgba(255,255,255,.0)'
                                            borderColor='transparent'
                                            width={300}
                                            height={2}
                                        />
                                    </View>
                                </TouchableWithoutFeedback>
                                <View style={styles.VideoControlButtons}>     
                                    {
                                        !pausedstate 
                                        ?   <Text style={{
                                                color: '#FFF',
                                                marginLeft: 15,
                                                fontSize: 10
                                                }}>
                                                {secondsToTime(Math.floor(progress * duration))}
                                            </Text>
                                        :   null
                                    }
                                    {/* <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 10}}>
                                        <TouchableOpacity
                                            style={{
                                                height: 30, width: 30,
                                                alignItems: 'center',
                                                zIndex: 1000,
                                            }}
                                            onPress={handleMainButtonTouch}
                                        >
                                            <Ionicons name='ios-rewind' size={17} color='#f9f9f9' />
                                        </TouchableOpacity>
                                        <TouchableOpacity
                                            style={{
                                                height: 30, width: 30,
                                                alignItems: 'center',
                                                zIndex: 1000,
                                            }}
                                            onPress={handleMainButtonTouch}
                                        >
                                            <Ionicons name={pausedstate ? 'ios-play' : 'ios-pause'} size={17} color='#f9f9f9' />
                                        </TouchableOpacity>
                                        <TouchableOpacity
                                            style={{
                                                height: 30, width: 30,
                                                alignItems: 'center',
                                                zIndex: 1000,
                                            }}
                                            onPress={handleMainButtonTouch}
                                        >
                                            <Ionicons name='ios-fastforward' size={17} color='#f9f9f9' />
                                        </TouchableOpacity>
                                    </View>
                                    <TouchableOpacity
                                        style={{
                                            height: 30, width: 30,
                                            alignItems: 'center',
                                            zIndex: 1000,
                                            marginTop: 10
                                        }}
                                        onPress={handleMainButtonTouch}
                                    >
                                        <Ionicons name='md-qr-scanner' size={17} color='#f9f9f9' />
                                    </TouchableOpacity> */}
                                </View>
                            </View>
                        </View>
                    </>
                )
                break;
            default: return null
        }
    }

    return (
        <>
            <View style={styles.card}>
                <View>
                    <View style={styles.publicationTitleContainer}>  
                        <Text style={styles.publicationTitle}>
                            {item.publication_title}
                        </Text>
                    </View>
                    <View style={{ alignItems: 'center', justifyContent: 'center' }}>
                        {
                            SwitchComponent(item.kind_publication)
                        }
                    </View>
                </View>
                <View style={styles.publicationTextContainer}>
                    <HTML html={item.publication_text} imagesMaxWidth={Dimensions.get('window').width} ignoredStyles={['var', 'transform']} ignoredTags={[...IGNORED_TAGS, '<yt-formatted-string>']}/>
                </View>
            </View>
        </>
    );
}

const styles = StyleSheet.create({
    card: {
        backgroundColor: '#484848',
        flexDirection: 'column',
        minHeight: 40,
        borderRadius: 10,
    },
    publicationTitleContainer: {
        backgroundColor: '#6d6d6d',
        color: 'white',
        height: 40,
        fontSize: 20,
        // borderRadius:10,
        borderTopLeftRadius: 10,
        borderTopRightRadius: 10,
        borderBottomLeftRadius: 0,
        borderBottomRightRadius: 0,
        flexDirection: 'column',
        justifyContent: 'center',
    },
    publicationTitle: {
        fontSize: 17,
        textAlign: 'left',
        color: 'white',
        fontWeight: '500',
        marginHorizontal: 15,
        marginTop: 5,
    },
    publicationTextContainer: {
        minHeight: 100, 
        paddingHorizontal: 15
    },
    LinkButton: { 
        width: '100%',
        height: 35,
        borderWidth: 0,
        borderColor: '#D32345',
        backgroundColor: '#D32345',
        alignItems: 'center',
        justifyContent:'center',
        marginTop: 2 
    },
    fullScreen: {
        flex: 1,
        // position: 'absolute',
        // top: 0,
        // left: 0,
        // bottom: 0,
        // right: 0,
    },
    VideoControlButtons: {
        flex: 1,
        flexDirection: 'row',
        maxHeight: 30,
        justifyContent:'space-between',
        alignItems: 'center',
        backgroundColor: '#484848',
        zIndex: 1000
    }
});

export default React.memo(NoticiasPost);