import React, {useContext, useEffect, useState, useRef} from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  TextInput,
  ScrollView,
  Alert,
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import {Context} from '../context/AuthContext';
import { Slider, Tooltip, Icon } from 'react-native-elements';
import Ionicons from 'react-native-vector-icons/Ionicons';
import Spinner from 'react-native-loading-spinner-overlay';
import ForceApi from '../api/force';
import {navigate} from '../navigationRef';

// let counterArray;
const CalculadoraForm = ({navigation, onSubmit}) => {
  const [A, setvA] = useState();
  const [B, setvB] = useState();
  const [C, setvC] = useState();
  const [D, setvD] = useState();
  const [newObject, setnewObject] = useState([]);
  const [lastObject, setlastObject] = useState();
  const [counterArray, setcounterArray] = useState();
  const [pterms, setpterms] = useState([]);
  const [Value, setValue] = useState();
  const valuesFromApi = [A, B, C, D];
  const [term, setvterm] = useState(Value);
  const [MaxV, setvMaxV] = useState(3000);
  const [vMount, setvMount] = useState(3000);
  const [vClientGet, setvClientGet] = useState();
  const [vPayment, setvPayment] = useState();

  const [verror, setverror] = useState();
  const [vPago, setvPago] = useState();
  const [lockB, setlockB] = useState(true)

  // const tooltipRef = useRef(null);

  // useEffect(() => {
    
  //   if (MaxV === 0) {
  //     tooltipRef.current.toggleTooltip();
  //   }
  // }, [MaxV, lastObject])

  CheckTextInput = () => {
    console.log('//////////////////////ultimo valor/////////////////////');
    onSubmit({vMount, lastObject, vPayment, vClientGet});
  };

  useEffect(() => {
    async function BCcontroller() {
      const vCreationUser = await AsyncStorage.getItem('id');
      const vSolicitudeId = await AsyncStorage.getItem('SolicitudeId');
      // const vSolicitudeId = 13628;
      const {data} = await ForceApi.post(`/ConsultBCController.php`, {
        vSolicitudeId,
        vCreationUser,
      });
      console.log('data', data);
      //if (data.error != true && data.payment != "0"){
      const values = data.terms;
      setpterms(values);
      const valuesSplit = values.split(',');
      setnewObject(valuesSplit);
      // setpterms(valuesSplit.prop)
      setcounterArray(valuesSplit.length - 1);
      console.log('count', counterArray);
      // setlastObject(newObject[newObject -1])
      setlastObject(newObject.pop());
      console.log('ultimo valor');

      console.log(lastObject);

      // valuesSplit.forEach(element => {

      // });

      // const [termA, termB, termC, termD] = values.split(',');
      // setvA(Number(termA));
      // setvB(Number(termB));
      // setvC(Number(termC));
      // setvD(typeof termD === 'string' ? Number(termD) : 0);
      // console.log(values);
      setverror(data.error);
      setvPago(data.payment);
    }
    BCcontroller();
  }, []);

  async function Payment({vMount, term}) {
    const vSolicitudeId = await AsyncStorage.getItem('SolicitudeId');
    // const vSolicitudeId = 13628;
    const vTerm = lastObject;
    console.log('Payment');
    console.log({vMount, vTerm});
    const {data} = await ForceApi.post(`/SimulatorPaymentController.php`, {
      vSolicitudeId,
      vTerm,
      vMount,
    });
    setvClientGet(data.clientGet);
    setvPayment(data.payment);
    // if (data.payment != 0) {
    //   setvPayment(data.payment);
    // } else {
    //   navigate('SolRechazada');
    // }
  }

  useEffect(() => {
    if (vMount && term > 0) {
      const vTerm = term;
      Payment({vMount, vTerm});
    }
  }, [vMount, term]);

  useEffect(() => {
    if ((vMount, lastObject)) {
      MaxOffer({vMount, lastObject});
    }
  }, [vMount, lastObject]);

  async function MaxOffer(vTerm) {
    const vSolicitudeId = await AsyncStorage.getItem('SolicitudeId');
    // const vSolicitudeId = 13628;
    const term = vTerm['lastObject'];
    console.log('term______________________________________');
    console.log(term);

    const {data} = await ForceApi.post(`/MaxOfferController.php`, {
      vSolicitudeId,
      term,
    });
    console.log('MaxOffer');
    console.log(data.maxMount);
    setvMaxV(Number(data.maxMount));
  }

  useEffect(() => {
    if (lastObject) {
      MaxOffer({lastObject});
    }
  }, [lastObject]);

  async function Enviar({vMount, term, vPayment, vClientGet}) {
    const vCreationUser = await AsyncStorage.getItem('id');
    const vSolicitudeId = await AsyncStorage.getItem('SolicitudeId');
    // const vSolicitudeId = 13628;
    const vTerm = term;
    const {data} = await ForceApi.post(`/InsertSimulatorInfoController.php`, {
      vSolicitudeId,
      vCreationUser,
      vMount,
      vClientGet,
      vTerm,
      vPayment,
    });
    console.log('insercion de datos');
    console.log(data);
  }

  //if (!A) return null;
  //if( Value > 0) setvterm(Value)
  useEffect(() => {
    if (verror != true && vPago != '0') {
      setverror(true);
    }
  }, []);

  const changeTerm = index => {
    console.log('changeTime index');
    console.log(index);

    console.log('changeTime selected');
    console.log(newObject[index]);
    setlastObject(newObject[index]);

    // setcounterArray(newObject[index])
  };

  if (Value > 0 && Value !== term) {
    setvterm(Value);
  }

  useEffect(() => {
    const lockButton = () => {
      if (MaxV > 0) {
        // console.warn('ewe fals');s
        setlockB(false)
      }
      else {
        // console.warn('ewe true');
        setlockB(true)
      }
    }
    lockButton()
  }, [MaxV, lastObject])

  if (verror != false && vPago === '0') {
    return (
      <Spinner
        size="large"
        visible={verror}
        color="#D32345"
        textContent={'Cargando Datos'}
        textStyle={styles.spinnerTextStyle}
      />
    );
  }
  return (
    <ScrollView>
      <View style={styles.centrado}>
        <View style={styles.tarjeta}>
          <Text style={styles.CreditText}>Simulador de crédito</Text>
          <View style={styles.top}>
            <Text style={styles.colorgris}>¿A qué plazo?:</Text>
            <Text style={styles.BtnText}> {lastObject} </Text>
          </View>
          <View style={{alignItems: 'stretch', justifyContent: 'center', marginHorizontal: 10}}>
            <Slider
              minimumTrackTintColor="#D32345"
              maximumValue={counterArray}
              minimumValue={0}
              step={1}
              thumbTintColor='#D32345'
              thumbTouchSize={{
                width: 70,
                height: 70
              }}
              value={counterArray ? counterArray : 50}
              onValueChange={index => changeTerm(index)}
              // onValueChange={index => setcounterArray(newObject[index])}
            />
            <View style={{              
              flexDirection: 'row',
              alignContent: 'center',
              justifyContent: 'space-around',
              flex: counterArray ? 1/counterArray : 1
            }}>
              {newObject.map((item, i) => {
                return (
                  <Text key={i} style={styles.plazo1}>
                    {item}
                  </Text>
                );
              })}
            </View>
            
            {/* <Tooltip
              width={250}
              height={100}
              ref={tooltipRef}
              backgroundColor='#333'
              highlightColor='#ff0000'
              pointerColor='#ff0000'
              withPointer={false}
              overlayColor='rgba(100, 100, 100, 0.5)'
              containerStyle={{
                marginRight: 20
              }}
              popover={<Text style={{ color: '#fff' }}>Este plazo no es válido para este usuario, seleccione uno mayor.</Text>}>
            </Tooltip> */}
            
          <View style={styles.top}>
            <Text style={styles.colorgris}>¿Cuánto dinero necesita?:</Text>
            <Text style={styles.BtnText}> ${vMount > 3000 ? vMount : 3000} MXN</Text>
          </View>
          <View style={{alignItems: 'stretch', justifyContent: 'center'}}>
            <Slider
              minimumTrackTintColor="#D32345"
              maximumValue={MaxV > 3000 ? MaxV : 3000}
              minimumValue={2500}
              step={500}
              thumbTintColor='#D32345'
              thumbTouchSize={{
                width: 70,
                height: 70
              }}
              value={vMount}
              onValueChange={newvMount => setvMount(newvMount === 2500 ? 3000 : newvMount)}
            />
            <View style={styles.plazos}>
              <Text style={styles.slideText}>$3000 MXN</Text>
              <Text style={[styles.slideText, styles.plz]}>${MaxV > 3000 ? MaxV : 3000} MXN</Text>
            </View>
          </View>
            {/* <Text style={styles.plazo1}>{A} </Text>
                    <Text style={styles.plazo2}>{B} </Text>
                    <Text style={styles.plazo3}>{C} </Text>
                    {D > 0 ? <Text style={styles.plazo3}>{D}  </Text>: null } */}
            {/* </View> */}
            <TouchableOpacity
              style={[styles.buttonContainer, styles.loginButton, styles.centr]}
              onPress={() => Payment({vMount, lastObject})}>
              <Text style={styles.BtnText}>Obtener pagos</Text>
            </TouchableOpacity>
            <Text style={styles.slideText}>Su crédito por: ${vMount > 3000 ? vMount : 3000} MXN</Text>
            <Text style={styles.slideText}>
              Usted recibe: ${vClientGet} MXN
            </Text>
            <Text style={styles.slideText}>A un plazo de: {MaxV > 0 ? lastObject : 'Este plazo no es válido para este usuario, seleccione uno mayor.'}</Text>

            <Text style={styles.PaymentText}>Su pago: ${vPayment} MXN</Text>
          </View>
        </View>
        <TouchableOpacity
          style={[styles.buttonContainer, MaxV > 0 ? styles.loginButton : styles.lockButton]}
          //onPress={() => Enviar({vMount,term, vPayment,vClientGe})}>
           //onPress={() => CheckTextInput()}>
           disabled={MaxV > 0 ? false : true}
           onPress={this.CheckTextInput}>
         
          <Text style={styles.BtnText}>ENVIAR</Text>
        </TouchableOpacity>
      </View>
    </ScrollView>
  );
};

CalculadoraForm.navigationOptions = {
  headerShown: false,
};

const styles = StyleSheet.create({
  plz: {
    marginLeft: '45%',
  },
  centr: {
    justifyContent: 'center',
    alignItems: 'center',
    alignContent: 'center',
    marginLeft: 25,
  },
  spinnerTextStyle: {
    color: '#D32345',
  },
  top: {
    flexDirection: 'row',
  },
  colorgris: {
    color: '#b3b4b5',
    marginLeft: 15,
  },

  plazos: {
    flexDirection: 'row',
    marginBottom: 20,
    justifyContent: 'center',
    alignItems: 'center',
    alignContent: 'center',
    display: 'flex',
  },
  plazo1: {
    color: '#b3b4b5',
    textAlign: 'center',
    // flex: counterArray ? 1/counterArray : 1
    // flex: 1 / 4,
  },
  plazo2: {
    color: '#b3b4b5',
    flex: 1 / 3,
  },
  plazo3: {
    color: '#b3b4b5',
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
    flex: 1 / 3,
  },
  container: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: '#333333',
  },
  logo: {
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    marginTop: 40,
    marginBottom: 20,
    marginLeft: 10,
  },
  backContainer: {
    flexDirection: 'column',
    marginBottom: 20,
  },
  head: {
    backgroundColor: '#000',
    borderRadius: 20,
  },
  back: {
    marginTop: 2,
    flexDirection: 'column',
    color: 'white',
  },
  textContainer: {
    flexDirection: 'row',
    marginTop: 30,
    marginBottom: 10,
    marginLeft: 10,
  },
  textIniciar: {
    color: 'white',
    width: 300,
    fontSize: 20,
    fontWeight: 'bold',
    textAlign: 'left',
    marginLeft: 25,
  },
  textInit: {
    color: '#b3b4b5',
    textAlign: 'left',
    marginLeft: 25,
  },

  centrado: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#333333',
  },
  Pin: {
    color: '#b3b4b5',
    marginLeft: 60,
    marginRight: 60,
  },
  inputContainer: {
    backgroundColor: '#5e5e5e',
    borderRadius: 30,
    width: 100,
    height: 45,
    marginTop: 30,
    marginBottom: 30,
    flexDirection: 'row',
    alignItems: 'center',
    marginLeft: 20,
  },
  inputs: {
    height: 45,
    marginLeft: 16,
    color: 'white',
    flex: 1,
  },
  buttonContainer: {
    height: 45,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 60,
    width: 300,
    borderRadius: 30,
    backgroundColor: 'transparent',
  },
  loginButton: {
    marginTop: 10,
    backgroundColor: '#D32345',
  },
  lockButton: {
    marginTop: 10,
    backgroundColor: '#911830',
  },
  BtnText: {
    color: 'white',
    fontWeight: 'bold',
  },

  tarjeta: {
    width: 350,
    shadowOpacity: 0.37,
    shadowRadius: 7.49,
    elevation: 12,
    marginVertical: 10,
    marginHorizontal: 20,
    flexBasis: '57%',
    flexDirection: 'column',
    backgroundColor: '#3d3d3d',
    borderRadius: 10,
  },
  CreditText: {
    marginTop: 10,
    marginBottom: 20,
    marginLeft: 15,
    color: 'white',
    fontWeight: 'bold',
  },
  slideText: {
    color: '#b3b4b5',
    marginBottom: 5,
    marginLeft: 15,
  },
  PaymentText: {
    color: 'white',
    fontWeight: 'bold',
    marginBottom: 20,
    marginTop: 10,
    marginLeft: 15,
  },
});

export default CalculadoraForm;
