//
//  MitekModule.m
//  MitekIntegration
//
//  Created by Juan Manuel Perez Santos on 3/4/20.
//  Copyright © 2020 Facebook. All rights reserved.
//

#import "MitekModule.h"
#import <UIKit/UIKit.h>
#import "CallbackHolder.h"
#import <MiSnapSDK/MiSnapSDK.h>

@implementation MitekModule

RCT_EXPORT_MODULE(MitekModule);

RCT_EXPORT_METHOD(launchMitek:(RCTResponseSenderBlock)successCallback error:(RCTResponseSenderBlock)failureCallback){

  [[CallbackHolder sharedInstance] setSuccessCallback:successCallback];
  [[CallbackHolder sharedInstance] setFailureCallback:failureCallback];

  [self performSelectorOnMainThread:@selector(presentMitekUI) withObject:nil waitUntilDone:NO];
  
}

-(void)presentMitekUI{

  UIViewController *topController = [UIApplication sharedApplication].keyWindow.rootViewController;

  NSMutableDictionary* parameters = [NSMutableDictionary
                                     dictionaryWithDictionary:[MiSnapSDKViewControllerUX2 defaultParametersForIdCardFront]];

  [parameters setObject:@"test" forKey:kMiSnapServerType];
  [parameters setObject:@"0.0" forKey:kMiSnapServerVersion];
  [parameters setObject:@"Mobile Deposit Check Front" forKey:kMiSnapShortDescription];

  MiSnapSDKViewControllerUX2* misnapVC = [MiSnapSDKViewControllerUX2 instantiateFromStoryboard];
  misnapVC.delegate = self;

  [misnapVC setupMiSnapWithParams:parameters];

  [topController presentViewController:misnapVC animated:true completion:nil];
}

//Método ejecutado al haber una captura exitosa
-(void)miSnapFinishedReturningEncodedImage:(NSString *)encodedImage
                             originalImage:(UIImage *)originalImage
                                andResults:(NSDictionary *)results {

  UIViewController *topController = [UIApplication sharedApplication].keyWindow.rootViewController;

  [topController dismissViewControllerAnimated:YES completion:^{

    [[CallbackHolder sharedInstance] sendSuccess:@[encodedImage]];

  }];

}

//Método ejecutado al ocurrir un error
-(void)miSnapCancelledWithResults:(NSDictionary *)results{

  [[CallbackHolder sharedInstance] sendFailure:@[@"Se canceló la captura de la imagen"]];

}

@end
