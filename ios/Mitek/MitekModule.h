//
//  MitekModule.h
//  MitekIntegration
//
//  Created by Juan Manuel Perez Santos on 3/4/20.
//  Copyright © 2020 Facebook. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <React/RCTBridgeModule.h>
#import <MiSnapSDK/MiSnapSDK.h>
#import "MiSnapSDKViewControllerUX2.h"

NS_ASSUME_NONNULL_BEGIN

@interface MitekModule : NSObject<RCTBridgeModule, MiSnapViewControllerDelegate>

@end

NS_ASSUME_NONNULL_END
