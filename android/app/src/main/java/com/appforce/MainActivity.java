package com.appforce;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.widget.Toast;

import com.facebook.react.ReactActivity;
import com.facebook.react.ReactActivityDelegate;
import com.facebook.react.ReactRootView;
//import com.miteksystems.misnap.analyzer.MiSnapAnalyzerResult;
//import com.miteksystems.misnap.params.BarcodeApi;
//import com.miteksystems.misnap.params.MiSnapApi;
import com.miteksystems.misnap.analyzer.MiSnapAnalyzerResult;
import com.miteksystems.misnap.params.MiSnapApi;
import com.swmansion.gesturehandler.react.RNGestureHandlerEnabledRootView;

import java.util.List;

public class MainActivity extends ReactActivity {

    private static final String TAG = "MainActivity";

    /**
     * Returns the name of the main component registered from JavaScript. This is used to schedule
     * rendering of the component.
     */
    @Override
    protected String getMainComponentName() {
        return "appforce";
    }

    @Override
    protected ReactActivityDelegate createReactActivityDelegate() {
        return new ReactActivityDelegate(this, getMainComponentName()) {
            @Override
            protected ReactRootView createRootView() {
                return new RNGestureHandlerEnabledRootView(MainActivity.this);
            }
        };
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data){
        super.onActivityResult(requestCode, resultCode, data);

        if (MiSnapApi.RESULT_PICTURE_CODE == requestCode) {
            if (RESULT_OK == resultCode) {
                if (data != null) {
                    Bundle extras = data.getExtras();
                    String miSnapResultCode = extras.getString(MiSnapApi.RESULT_CODE);
                    if (!miSnapResultCode.equals(MiSnapApi.RESULT_SUCCESS_CREDIT_CARD)) {
//                        Intent displayResults = new Intent(this, ViewResultActivity.class);
//                        displayResults.putExtras(data);
//                        displayResults.putExtra(MiSnapApi.RESULT_CODE, extras.getString(MiSnapApi.RESULT_CODE));
//                        startActivity(displayResults);
                    }

                    switch (miSnapResultCode) {

                        // MiSnap check capture
                        case MiSnapApi.RESULT_SUCCESS_VIDEO:
                        case MiSnapApi.RESULT_SUCCESS_STILL:
                            Log.i(TAG, "MIBI: " + extras.getString(MiSnapApi.RESULT_MIBI_DATA));

                            // Image returned successfully
                            byte[] sImage = data.getByteArrayExtra(MiSnapApi.RESULT_PICTURE_DATA);

                            // Now Base64-encode the byte array before sending it to the server.
                            byte[] sEncodedImage = Base64.encode(sImage, Base64.DEFAULT);

                            String base64String = new String(sEncodedImage);

                            CallbackHolder.getInstance().sendSuccess(base64String);

                            List<String> warnings = extras.getStringArrayList(MiSnapApi.RESULT_WARNINGS);
                            if (warnings != null && !warnings.isEmpty()) {
                                String message = "WARNINGS:";
                                if ((warnings.contains(MiSnapAnalyzerResult.FrameChecks.WRONG_DOCUMENT.name()))) {
                                    message += "\nWrong document detected";
                                }
                                Toast.makeText(this, message, Toast.LENGTH_LONG).show();
                            }


                            break;

                    }
                } else {
                    // Image canceled, stop
                    Toast.makeText(this, "MiSnap canceled", Toast.LENGTH_SHORT).show();
                }
            } else if (RESULT_CANCELED == resultCode) {

                CallbackHolder.getInstance().sendFailure("Operation Cancelled: Camera Not supported");

                // Camera not working or not available, stop
                Toast.makeText(this, "Operation canceled!!!", Toast.LENGTH_SHORT).show();
                if (data != null) {
                    Bundle extras = data.getExtras();
                    String miSnapResultCode = extras.getString(MiSnapApi.RESULT_CODE);
                    if (!miSnapResultCode.isEmpty()) {
                        Toast.makeText(this, "Shutdown reason: " + miSnapResultCode, Toast.LENGTH_SHORT).show();
                    }
                }
            }
        }
    }

}
