package com.appforce;

import android.content.Intent;
import android.widget.Toast;

import com.facebook.react.bridge.Callback;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.miteksystems.misnap.misnapworkflow.MiSnapWorkflowActivity;
import com.miteksystems.misnap.misnapworkflow_UX2.MiSnapWorkflowActivity_UX2;
import com.miteksystems.misnap.misnapworkflow_UX2.params.WorkflowApi;
import com.miteksystems.misnap.params.BarcodeApi;
import com.miteksystems.misnap.params.CameraApi;
import com.miteksystems.misnap.params.MiSnapApi;
import com.miteksystems.misnap.params.ScienceApi;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class MitekModule extends ReactContextBaseJavaModule {
    private static ReactApplicationContext reactContext;

    MitekModule(ReactApplicationContext context) {
        super(context);
        reactContext = context;
    }

    @Override
    public String getName() {
        return "MitekModule";
    }

    @Override
    public Map<String, Object> getConstants() {
        final Map<String, Object> constants = new HashMap<>();
        return constants;
    }

    @ReactMethod
    public void launchMitek(Callback successCallback, Callback failureCallback) {

        CallbackHolder.getInstance().setSuccessCallback(successCallback);
        CallbackHolder.getInstance().setFailureCallback(failureCallback);

        startMiSnapWorkflow(MiSnapApi.PARAMETER_DOCTYPE_ID_CARD_FRONT);

    }

    private void startMiSnapWorkflow(String docType) {

        JSONObject misnapParams = new JSONObject();
        try {
            misnapParams.put(MiSnapApi.MiSnapDocumentType, docType);
            if (docType.equals(MiSnapApi.PARAMETER_DOCTYPE_CHECK_FRONT)) {
                misnapParams.put(ScienceApi.MiSnapGeoRegion, ScienceApi.GEO_REGION_GLOBAL);
            }

            // Example of how to add additional barcode scanning options
            if (docType.equals(MiSnapApi.PARAMETER_DOCTYPE_BARCODES)) {
                // Set everything except Code 128 because it conflicts w/ the PDF417 barcode on the back of most drivers licenses.
                misnapParams.put(BarcodeApi.BarCodeTypes, BarcodeApi.BARCODE_ALL - BarcodeApi.BARCODE_CODE_128);
            }

            // Here you can override optional API parameter defaults
            misnapParams.put(CameraApi.MiSnapAllowScreenshots, 1);
            // e.g. misnapParams.put(MiSnapApi.AppVersion, "1.0");
            // Workflow parameters are now put into the same JSONObject as MiSnap parameters
            misnapParams.put(WorkflowApi.MiSnapTrackGlare, WorkflowApi.TRACK_GLARE_ENABLED);
            //misnapParams.put(CameraApi.MiSnapFocusMode, CameraApi.PARAMETER_FOCUS_MODE_HYBRID_NEW);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Intent intentMiSnap;
//        if (mUxWorkflow == 1) {
       // intentMiSnap = new Intent(reactContext, MiSnapWorkflowActivity.class);
//        } else {
            intentMiSnap = new Intent(reactContext, MiSnapWorkflowActivity_UX2.class);
//        }
        intentMiSnap.putExtra(MiSnapApi.JOB_SETTINGS, misnapParams.toString());
        reactContext.startActivityForResult(intentMiSnap, MiSnapApi.RESULT_PICTURE_CODE, null);
    }

}