package com.appforce;

import com.facebook.react.bridge.Callback;

public class CallbackHolder {

    private static final CallbackHolder ourInstance = new CallbackHolder();

    public static CallbackHolder getInstance() {
        return ourInstance;
    }

    private CallbackHolder() {
    }

    private Callback successCallback;

    private Callback failureCallback;

    public void setSuccessCallback(Callback successCallback) {
        this.successCallback = successCallback;
    }

    public void setFailureCallback(Callback failureCallback) {
        this.failureCallback = failureCallback;
    }

    public void sendSuccess(Object... args) {
        if (this.successCallback != null) {
            this.successCallback.invoke(args);
        }
    }

    public void sendFailure(Object... args) {
        if (this.failureCallback != null) {
            this.failureCallback.invoke(args);
        }
    }

}
